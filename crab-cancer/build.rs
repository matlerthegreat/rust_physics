fn main() {
    println!("cargo:rerun-if-changed=../cancer");

    let dst = cmake::build("../cancer");

    println!("cargo:rustc-link-search=native={}", dst.display());

    println!("cargo:rustc-link-lib=cancer");
    println!("cargo:rustc-link-lib=stdc++");
}
