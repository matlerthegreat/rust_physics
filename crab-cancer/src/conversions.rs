use crab_common::{
    math::{Vector, Quaterion},
    objects::Object,
    shapes::{Box, Shape, Sphere},
};

use crate::native::{self, SHAPE_BOX, SHAPE_SPHERE};

impl From<Vector> for native::Vector {
    fn from(v: Vector) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: v.z,
        }
    }
}

impl From<native::Vector> for Vector {
    fn from(v: native::Vector) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: v.z,
        }
    }
}

impl From<Sphere> for native::Sphere {
    fn from(sphere: Sphere) -> Self {
        Self {
            radius: sphere.radius,
        }
    }
}

impl From<native::Sphere> for Sphere {
    fn from(sphere: native::Sphere) -> Self {
        Self {
            radius: sphere.radius,
        }
    }
}

impl From<Box> for native::Box {
    fn from(b: Box) -> Self {
        Self {
            extents: b.extents.into(),
        }
    }
}

impl From<native::Box> for Box {
    fn from(b: native::Box) -> Self {
        Self {
            extents: b.extents.into(),
        }
    }
}

impl From<Shape> for native::Shape {
    fn from(shape: Shape) -> Self {
        match shape {
            Shape::Sphere(sphere) => Self {
                shape_type: native::SHAPE_SPHERE,
                shape_union: native::ShapeUnion {
                    shape_sphere: sphere.into(),
                },
            },
            Shape::Box(b) => Self {
                shape_type: native::SHAPE_BOX,
                shape_union: native::ShapeUnion {
                    shape_box: b.into(),
                },
            },
            Shape::Plane(_) => todo!(),
        }
    }
}

impl From<native::Shape> for Shape {
    fn from(shape: native::Shape) -> Self {
        unsafe {
            match shape.shape_type {
                SHAPE_SPHERE => Shape::Sphere(shape.shape_union.shape_sphere.into()),
                SHAPE_BOX => Shape::Box(shape.shape_union.shape_box.into()),
                _ => panic!(),
            }
        }
    }
}

impl From<Object> for native::Object {
    fn from(object: Object) -> Self {
        Self {
            shape: object.shape.into(),
            p: object.p.into(),
            prev_p: object.prev_p.into(),
            inv_m: object.inv_m,
        }
    }
}

impl From<native::Object> for Object {
    fn from(object: native::Object) -> Self {
        Self {
            shape: object.shape.into(),
            p: object.p.into(),
            prev_p: object.prev_p.into(),
            //r: todo!(),
            inv_m: object.inv_m
        }
    }
}
