use std::mem::MaybeUninit;

use crab_common::{
    math::{Scalar, Vector},
    objects::Object,
};

mod conversions;
mod native;

#[derive(Debug)]
#[repr(transparent)]
pub struct Cancer(*mut native::Engine);

impl From<Cancer> for *mut native::Engine {
    fn from(cancer: Cancer) -> Self {
        cancer.0
    }
}

impl From<*mut native::Engine> for Cancer {
    fn from(engine: *mut native::Engine) -> Self {
        Self(engine)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct WeakProxy(native::WeakProxy);

impl From<WeakProxy> for native::WeakProxy {
    fn from(proxy: WeakProxy) -> Self {
        proxy.0
    }
}

impl From<native::WeakProxy> for WeakProxy {
    fn from(proxy: native::WeakProxy) -> Self {
        Self(proxy)
    }
}

impl Cancer {
    pub fn naive() -> Cancer {
        unsafe { native::engine_construct_naive() }.into()
    }

    pub fn hash_grid() -> Cancer {
        unsafe { native::engine_construct_hash_grid() }.into()
    }

    pub fn sap() -> Cancer {
        unsafe { native::engine_construct_sap() }.into()
    }

    pub fn insert(&mut self, object: Object) -> WeakProxy {
        unsafe { native::engine_insert(self.0, &object.into()) }.into()
    }

    pub fn get_object_position(&self, proxy: &WeakProxy) -> Vector {
        unsafe { native::engine_get_position(self.0, proxy.0) }.into()
    }

    pub fn update(&mut self, time: Scalar) {
        unsafe { native::engine_update(self.0, time) }
    }

    pub fn into_iter(&self) -> impl Iterator<Item = (WeakProxy, Object)> + '_ {
        let succ = |it| unsafe {
            let mut proxy = MaybeUninit::zeroed();
            let mut object = MaybeUninit::zeroed();
            let iterator =
                native::engine_iterate(self.0, it, proxy.as_mut_ptr(), object.as_mut_ptr());
            if iterator.is_null() {
                None
            } else {
                Some((iterator, proxy.assume_init(), object.assume_init()))
            }
        };

        std::iter::successors(succ(std::ptr::null_mut()), move |(it, _, _)| succ(*it))
            .map(|(_, proxy, object)| (proxy.into(), object.into()))
    }
}

impl Drop for Cancer {
    fn drop(&mut self) {
        unsafe {
            native::engine_destructor(self.0);
        }
    }
}

// SAFETY: Engine is allocated and dealocated by C++
// as long as it's just new() it's safe
unsafe impl Send for Cancer {}

// SAFETY: all mutating methods take &mut self
// thus utilizing the borrow checker for thred-safety
unsafe impl Sync for Cancer {}

#[cfg(test)]
mod tests {
    use std::iter::repeat_with;

    use crab_common::{
        math::Vector,
        random::gen_random_object,
        shapes::{Box, Shape, Sphere, AABB},
    };
    use rand::prelude::*;
    use rand::rngs::SmallRng;

    use super::*;

    #[test]
    fn insert_object() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(100., 100., 100.));
        let object = gen_random_object(&mut rng, &bounds);
        let pos = object.p;

        let mut engine = Cancer::naive();
        let proxy = engine.insert(object);

        assert_eq!(proxy.0.gen, 0);
        assert_eq!(proxy.0.offset, 0);

        assert_eq!(engine.get_object_position(&proxy), pos);
    }

    #[test]
    fn insert_objects() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(100., 100., 100.));

        let mut engine = Cancer::naive();

        for object in repeat_with(|| gen_random_object(&mut rng, &bounds)).take(100) {
            let pos = object.p;
            let proxy = engine.insert(object);

            assert_eq!(engine.get_object_position(&proxy), pos);
        }
    }

    #[test]
    fn sphere_vs_sphere() {
        let a = Object::new(
            Shape::Sphere(Sphere::new(1.0)),
            Vector::new(-0.5, -9.0, 0.0),
        );
        let b = Object::new(Shape::Sphere(Sphere::new(1.0)), Vector::new(0.5, -9.0, 0.0));

        let mut engine = Cancer::naive();
        let a = engine.insert(a);
        let b = engine.insert(b);

        engine.update(0.1333);

        assert_eq!(engine.get_object_position(&a), Vector::new(-1.0, -9.0, 0.0));
        assert_eq!(engine.get_object_position(&b), Vector::new(1.0, -9.0, 0.0));
    }

    #[test]
    fn box_vs_sphere() {
        let a = Object::new(
            Shape::Box(Box::new(Vector::lift(1.0))),
            Vector::new(-0.5, -9.0, 0.0),
        );
        let b = Object::new(Shape::Sphere(Sphere::new(1.0)), Vector::new(0.5, -9.0, 0.0));

        let mut engine = Cancer::naive();
        let a = engine.insert(a);
        let b = engine.insert(b);

        engine.update(0.1333);

        assert_eq!(engine.get_object_position(&a), Vector::new(-1.0, -9.0, 0.0));
        assert_eq!(engine.get_object_position(&b), Vector::new(1.0, -9.0, 0.0));
    }

    #[test]
    fn box_vs_box() {
        let a = Object::new(
            Shape::Box(Box::new(Vector::lift(1.0))),
            Vector::new(-0.5, -9.0, 0.0),
        );
        let b = Object::new(
            Shape::Box(Box::new(Vector::lift(1.0))),
            Vector::new(0.5, -9.0, 0.0),
        );

        let mut engine = Cancer::naive();
        let a = engine.insert(a);
        let b = engine.insert(b);

        engine.update(0.1333);

        assert_eq!(engine.get_object_position(&a), Vector::new(-1.0, -9.0, 0.0));
        assert_eq!(engine.get_object_position(&b), Vector::new(1.0, -9.0, 0.0));
    }
}
