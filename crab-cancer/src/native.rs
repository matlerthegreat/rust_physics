#[repr(C)]
pub struct Engine {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

#[repr(C)]
pub struct EngineIterator {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type Scalar = f32;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Vector {
    pub x: Scalar,
    pub y: Scalar,
    pub z: Scalar,
}

pub type Gen = ::std::os::raw::c_ushort;
pub type Offset = ::std::os::raw::c_ushort;

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct WeakProxy {
    pub gen: Gen,
    pub offset: Offset,
}

pub type ShapeType = ::std::os::raw::c_uint;
pub const SHAPE_SPHERE: ShapeType = 0;
pub const SHAPE_BOX: ShapeType = 1;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Sphere {
    pub radius: Scalar,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Box {
    pub extents: Vector,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Shape {
    pub shape_type: ShapeType,
    pub shape_union: ShapeUnion,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub union ShapeUnion {
    pub shape_sphere: Sphere,
    pub shape_box: Box,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Object {
    pub shape: Shape,
    pub p: Vector,
    pub prev_p: Vector,
    pub inv_m: f32,
}

extern "C" {
    pub fn engine_construct_naive() -> *mut Engine;
    pub fn engine_construct_hash_grid() -> *mut Engine;
    pub fn engine_construct_sap() -> *mut Engine;

    pub fn engine_destructor(engine: *mut Engine);

    pub fn engine_insert(engine: *mut Engine, object: &Object) -> WeakProxy;
    pub fn engine_get_position(engine: *const Engine, proxy: WeakProxy) -> Vector;
    pub fn engine_update(engine: *mut Engine, time: Scalar);

    pub fn engine_iterate(
        engine: *const Engine,
        iterator: *mut EngineIterator,
        proxy: *mut WeakProxy,
        object: *mut Object,
    ) -> *mut EngineIterator;
}
