#include "shape.hpp"

bool AABB::intersects(const AABB& other) const {
	return
		(minimum.x <= other.maximum.x && maximum.x >= other.minimum.x) &&
		(minimum.y <= other.maximum.y && maximum.y >= other.minimum.y) &&
		(minimum.z <= other.maximum.z && maximum.z >= other.minimum.z);
}

/*bool are_intersecting(const Point& a, const Point& b) {
	return distance_squared(a, b) <= 0.0f;
}

bool are_intersecting(const Point& a, const Sphere& b) {
	return distance_squared(b.center, a) <= powf(b.radius, 2);
}

bool are_intersecting(const Point& a, const AABB& b) {
	return
		(b.minimum.x <= a.x && b.maximum.x >= a.x) &&
		(b.minimum.y <= a.y && b.maximum.y >= a.y) &&
		(b.minimum.z <= a.z && b.maximum.z >= a.z);
}

bool are_intersecting(const Sphere& a, const AABB& b) {
	Scalar d = 0.0;

	if (a.center.x < b.minimum.x) {
		d += pow(a.center.x - b.minimum.x, 2);
	} else if (a.center.x > b.maximum.x) {
		d += pow(a.center.x - b.maximum.x, 2);
	}
	
	if (a.center.y < b.minimum.y) {
		d += pow(a.center.y - b.minimum.y, 2);
	} else if (a.center.y > b.maximum.y) {
		d += pow(a.center.y - b.maximum.y, 2);
	}
	
	if (a.center.z < b.minimum.z) {
		d += pow(a.center.z - b.minimum.z, 2);
	} else if (a.center.z > b.maximum.z) {
		d += pow(a.center.z - b.maximum.z, 2);
	}

	return d <= pow(a.radius, 2);
}

bool are_intersecting(const AABB& a, const AABB& b) {
	return
		(a.minimum.x <= b.maximum.x && a.maximum.x >= b.minimum.x) &&
		(a.minimum.y <= b.maximum.y && a.maximum.y >= b.minimum.y) &&
		(a.minimum.z <= b.maximum.z && a.maximum.z >= b.minimum.z);
}

bool are_intersecting(const Sphere& a, const Sphere& b) {
	auto max_distance = a.radius + b.radius;
	return distance_squared(a.center, b.center) <= powf(max_distance, 2);
}

	if (auto point_a = std::get_if<Point>(&a)) {
		if (auto point_b = std::get_if<Point>(&b)) {
			return are_intersecting(*point_a, *point_b);
		}
		if (auto sphere_b = std::get_if<Sphere>(&b)) {
			return are_intersecting(*point_a, *sphere_b);
		}
		if (auto aabb_b = std::get_if<AABB>(&b)) {
			return are_intersecting(*point_a, *aabb_b);
		}
	}
	if (auto sphere_a = std::get_if<Sphere>(&a)) {
		if (auto point_b = std::get_if<Point>(&b)) {
			return are_intersecting(*point_b, *sphere_a);
		}
		if (auto sphere_b = std::get_if<Sphere>(&b)) {
			return are_intersecting(*sphere_a, *sphere_b);
		}
		if (auto aabb_b = std::get_if<AABB>(&b)) {
			return are_intersecting(*sphere_a, *aabb_b);
		}
	}
	if (auto aabb_a = std::get_if<AABB>(&a)) {
		if (auto point_b = std::get_if<Point>(&b)) {
			return are_intersecting(*point_b, *aabb_a);
		}
		if (auto sphere_b = std::get_if<Sphere>(&b)) {
			return are_intersecting(*sphere_b, *aabb_a);
		}
		if (auto aabb_b = std::get_if<AABB>(&b)) {
			return are_intersecting(*aabb_a, *aabb_b);
		}
	}

	throw "are_intresecting run out of variants!";
}*/