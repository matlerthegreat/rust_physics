#include "object.hpp"
#include "scalars.hpp"
#include "shape.hpp"
#include <cstddef>
#include <limits>

Intersection sphereVsSphere(const Object& a, const Object& b) {
	Scalar penetration = (a.shape.sphere.radius + b.shape.sphere.radius) - distance(a.p, b.p);
	Vector normal = normalize(a.p - b.p);
	return Intersection{penetration, normal};
}

Intersection boxVsBox(const Object& a, const Object& b) {
	Scalar penetration = std::numeric_limits<Scalar>::infinity();
	Vector normal;

	for (int axis = 0; axis < 3; ++axis) {
		Scalar t = a.p.get(axis) + a.shape.box.extents.get(axis) - b.p.get(axis) + b.shape.box.extents.get(axis);
		if (t < penetration) {
			penetration = t;
			normal = -PrimaryAxes[axis];
		}
		t = b.p.get(axis) + b.shape.box.extents.get(axis) - a.p.get(axis) + a.shape.box.extents.get(axis);
		if (t < penetration) {
			penetration = t;
			normal = PrimaryAxes[axis];
		}
	}

	return Intersection{penetration, normal};
}

Intersection sphereVsBox(const Object& a, const Object& b) {
	Scalar penetration = std::numeric_limits<Scalar>::infinity();
	Vector normal;

	for (std::size_t axis = 0; axis < 3; ++axis) {
		Scalar t = a.p.get(axis) + a.shape.sphere.radius - b.p.get(axis) + b.shape.box.extents.get(axis);
		if (t < penetration) {
			penetration = t;
			normal = -PrimaryAxes[axis];
		}
		t = b.p.get(axis) + b.shape.box.extents.get(axis) - a.p.get(axis) + a.shape.sphere.radius;
		if (t < penetration) {
			penetration = t;
			normal = PrimaryAxes[axis];
		}
	}

	return Intersection{penetration, normal};
}

Intersection Object::intersects(const Object& other) const {
	if (shape.shapeType == SHAPE_SPHERE) {
		if (other.shape.shapeType == SHAPE_SPHERE) {
            return sphereVsSphere(*this, other);
		} else if (other.shape.shapeType == SHAPE_BOX) {
			return sphereVsBox(*this, other);
		}
	} else if (shape.shapeType == SHAPE_BOX) {
		if (other.shape.shapeType == SHAPE_SPHERE) {
			Intersection i = sphereVsBox(other, *this);
			i.normal = -i.normal;
			return i;
		} else if (other.shape.shapeType == SHAPE_BOX) {
			return boxVsBox(*this, other);
		}
	}
	return Intersection{0, Vector::lift(0)};
}

AABB Object::getBoundingAABB() const
{
	switch (shape.shapeType) {
		case SHAPE_SPHERE: {
			auto minimum = p - Vector::lift(shape.sphere.radius);
			auto maximum = p + Vector::lift(shape.sphere.radius);

			return AABB{minimum, maximum};
		}
		case SHAPE_BOX: {
			auto minimum = p - shape.box.extents;
			auto maximum = p + shape.box.extents;

			return AABB{minimum, maximum};
		}
	}
}

AABB Object::getPrevBoundingAABB() const
{
	switch (shape.shapeType) {
		case SHAPE_SPHERE: {
			auto minimum = prev_p - Vector::lift(shape.sphere.radius);
			auto maximum = prev_p + Vector::lift(shape.sphere.radius);

			return AABB{minimum, maximum};
		}
		case SHAPE_BOX: {
			auto minimum = prev_p - shape.box.extents;
			auto maximum = prev_p + shape.box.extents;

			return AABB{minimum, maximum};
		}
	}
}
