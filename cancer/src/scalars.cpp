#include "scalars.hpp"

Scalar distance(const Point& v1, const Point& v2) {
	return (v1 - v2).length();
}

Scalar distance_squared(const Point& v1, const Point& v2) {
	return (v1 - v2).length_squared();
}

Vector normalize(Vector v) {
	return v * (1.0 / v.length());
}