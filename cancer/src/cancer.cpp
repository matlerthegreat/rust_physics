#include "cancer.hpp"
#include "broad_phase.hpp"
#include "engine.hpp"
#include "scalars.hpp"

#include <iostream>
#include <memory>

Engine* engine_construct_naive() {
    return new Engine(std::make_unique<BruteForce>());
}

Engine* engine_construct_hash_grid() {
    return new Engine(std::make_unique<SpatialHashGrid>());
}

Engine* engine_construct_sap() {
    return new Engine(std::make_unique<PersistentSAP>());
}

void engine_destructor(Engine* engine) {
    delete engine;
}

WeakProxy engine_insert(Engine* engine, Object& object) {
    return engine->insert(object);
}

Vector engine_get_position(const Engine *engine, WeakProxy proxy) {
    return engine->getObjectPosition(proxy);
}

void engine_update(Engine* engine, Scalar time) {
    engine->update(time);
}

Engine::ConstIterator* engine_iterate(const Engine* engine, Engine::ConstIterator* iterator, WeakProxy* out_proxy, Object* out_object) {
    if (!iterator) {
        iterator = new Engine::ConstIterator(engine->begin());
    } else {
        ++(*iterator);
    }
    
    if (*iterator == engine->end()) {
        delete iterator;
        return nullptr;
    }

    *out_proxy = iterator->weak();
    *out_object = &iterator->strong();

    return iterator;
}