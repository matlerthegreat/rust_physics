// Cancer.cpp : Defines the functions for the static library.
//

#include <vector>
#include <iterator>
#include <algorithm>
#include <iostream>

#include "engine.hpp"
#include "shape.hpp"
#include "object.hpp"
#include "broad_phase.hpp"

Engine::Engine(std::unique_ptr<BroadPhase> accel)
	: m_accel(std::move(accel)) {
}

Engine::~Engine() {
}

WeakProxy Engine::insert(Object object) {
	WeakProxy proxy = m_objects.insert(object);
	m_accel->registerObject(proxy.upgrade_const(m_objects), m_objects);
	return proxy;
}

const Vector Engine::getObjectPosition(WeakProxy proxy) const {
	return m_objects.get(proxy).p;
}

void Engine::update(Scalar t) {
	for (auto& object: m_objects) {
		Vector dx = object.p - object.prev_p;
		object.prev_p = object.p;

		Scalar g = 100.0;
		Vector v = dx / t * 0.95;
		Vector a = Vector{0, 1, 0} * -g;
		object.p = object.p + v * t + a * 0.5 * t * t;

		AABB aabb = object.getBoundingAABB();

		Scalar size = 10.0;

		if (aabb.minimum.x < -size) {
			object.p.x += -size - aabb.minimum.x;
		} else if (aabb.maximum.x > size) {
			object.p.x += size - aabb.maximum.x;
		}

		if (aabb.minimum.y < -size) {
			object.p.y += -size - aabb.minimum.y;
		} else if (aabb.maximum.y > size) {
			object.p.y += size - aabb.maximum.y;
		}

		if (aabb.minimum.z < -size) {
			object.p.z += -size - aabb.minimum.z;
		} else if (aabb.maximum.z > size) {
			object.p.z += size - aabb.maximum.z;
		}
	}

	m_accel->update(m_objects);
	m_accel->calcCollisions(m_objects);

	for (const auto collision: m_accel->getCollisions()) {
		if (collision.get(0) == collision.get(1)) {
			continue;
		}

		auto a = collision.get(0).upgrade(m_objects);
		auto b = collision.get(1).upgrade(m_objects);

		Intersection intersection = a->intersects(&b);

		if (intersection.penetration > 0.0) {
			Vector diff = intersection.normal * intersection.penetration * 0.5;
			a->p += diff;
			b->p -= diff;
		}
	}
}

Engine::ConstIterator Engine::begin() const {
	return m_objects.begin();
}

Engine::ConstIterator Engine::end() const {
	return m_objects.end();
}