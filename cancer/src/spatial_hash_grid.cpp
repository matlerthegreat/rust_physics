#include "broad_phase.hpp"
#include "scalars.hpp"
#include "shape.hpp"

#include <cstddef>
#include <cstdio>
#include <ranges>
#include <vector>
#include <bitset>
#include <set>

void SpatialHashGrid::registerObject(const StrongProxy<const Object>& proxy, const Objects& objects) {
	std::bitset<SIZE> used;
	AABB aabb = proxy->getBoundingAABB();
	for (int x = floor(aabb.minimum.x); x < ceil(aabb.maximum.x); ++x) {
		for (int y = floor(aabb.minimum.y); y < ceil(aabb.maximum.y); ++y) {
			for (int z = floor(aabb.minimum.z); z < ceil(aabb.maximum.z); ++z) {
				auto hash = spatialHash(x, y, z);
				if (!used[hash]) {
					m_proxyLists.insert(hash, proxy.downgrade());
				}
				used.set(hash);
			}
		}
	}
}

void SpatialHashGrid::update(const Objects& objects) {
	for (auto proxy = objects.begin(); proxy != objects.end(); ++proxy) {
		const AABB curr = proxy->getBoundingAABB();
		const AABB prev = proxy->getPrevBoundingAABB();

		for (int x = floor(prev.minimum.x); x < ceil(prev.maximum.x); ++x) {
			for (int y = floor(prev.minimum.y); y < ceil(prev.maximum.y); ++y) {
				for (int z = floor(prev.minimum.z); z < ceil(prev.maximum.z); ++z) {
					if (x < floor(curr.minimum.x) || ceil(curr.maximum.x) <= x ||
						y < floor(curr.minimum.y) || ceil(curr.maximum.y) <= y ||
						z < floor(curr.minimum.z) || ceil(curr.maximum.z) <= z) {
						auto hash = spatialHash(x, y, z);
						m_proxyLists.remove(hash, proxy.weak());
					}
				}
			}
		}

		std::bitset<SIZE> used;
		for (int x = floor(curr.minimum.x); x < ceil(curr.maximum.x); ++x) {
			for (int y = floor(curr.minimum.y); y < ceil(curr.maximum.y); ++y) {
				for (int z = floor(curr.minimum.z); z < ceil(curr.maximum.z); ++z) {
					if (x < floor(prev.minimum.x) || ceil(prev.maximum.x) <= x ||
						y < floor(prev.minimum.y) || ceil(prev.maximum.y) <= y ||
						z < floor(prev.minimum.z) || ceil(prev.maximum.z) <= z)
					{
						auto hash = spatialHash(x, y, z);
						if (!used[hash]) {
							m_proxyLists.insert(hash, proxy.weak());
						}
						used.set(hash);
					}
				}
			}
		}
	}
}

std::size_t SpatialHashGrid::calcCollisions(const Objects& objects) {
	m_collisions.clear();

	for (std::size_t i = 0; i < SIZE; ++i) {
		auto curr = m_proxyLists.getHead(i);
		while (curr != m_proxyLists.INV_INDEX) {
			auto currItem = m_proxyLists.get(curr);
			auto currAABB = currItem.data.upgrade_const(objects)->getBoundingAABB();

			auto other = currItem.next;
			while (other != m_proxyLists.INV_INDEX) {
				auto otherItem = m_proxyLists.get(other);
				if (currItem.data == otherItem.data) {
					other = otherItem.next;
					continue;
				}

				auto otherAABB = otherItem.data.upgrade_const(objects)->getBoundingAABB();
				if (currAABB.intersects(otherAABB)) {
					m_collisions.insert(CollisionPair(currItem.data, otherItem.data));
				}
				
				other = otherItem.next;
			}

			curr = currItem.next;
		}
	}

	return m_collisions.size();
}

std::size_t SpatialHashGrid::spatialHash(int x, int y, int z) {
	std::size_t h1 = x * 73856093;
	std::size_t h2 = y * 19349663;
	std::size_t h3 = z * 83492791;
	return (h1 ^ h2 ^ h3) % SIZE;
}