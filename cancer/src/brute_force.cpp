#include "broad_phase.hpp"

#include <algorithm>
#include <cstdio>
#include <iterator>
#include <iostream>
#include <unordered_set>

std::size_t BruteForce::calcCollisions(const Objects& objects) {
	m_collisions.clear();

	for (auto a = objects.begin(); a != objects.end(); ++a) {
		for (auto b = std::next(a); b != objects.end(); ++b) {
			auto intersection = a->intersects(*b);
			if (intersection.penetration > 0.0) {
				m_collisions.emplace(a.weak(), b.weak());
			}
		}
	}

	return m_collisions.size();
}