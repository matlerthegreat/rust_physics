#include "broad_phase.hpp"
#include <cstddef>

void PersistentSAP::boubleDown(std::size_t axis, std::size_t i, const Objects& objects) {
    if (i < 1) {
        return;
    }

    EndPoint& left = m_axes[axis][i-1];
    EndPoint& curr = m_axes[axis][i];

    if (!left.start && curr.start && left.point > curr.point) {
        if (left.proxy.upgrade_const(objects)->getBoundingAABB().intersects(curr.proxy.upgrade_const(objects)->getBoundingAABB())) {
            m_collisions.insert(CollisionPair(left.proxy, curr.proxy));
        }
        std::swap(left, curr);
        boubleDown(axis, i-1, objects);
    } else if (left.start && !curr.start && left.point > curr.point) {
            if (!left.proxy.upgrade_const(objects)->getBoundingAABB().intersects(curr.proxy.upgrade_const(objects)->getBoundingAABB())) {
            m_collisions.erase(CollisionPair(left.proxy, curr.proxy));
        }
        std::swap(left, curr);
        boubleDown(axis, i-1, objects);
    } else if (left.point > curr.point) {
        std::swap(left, curr);
        boubleDown(axis, i-1, objects);
    }
}

void PersistentSAP::registerObject(const StrongProxy<const Object>& proxy, const Objects& objects) {
    AABB aabb = proxy->getBoundingAABB();

    for (std::size_t axis = 0; axis < 3; ++axis) {
        std::size_t i = m_axes[axis].size();
        m_axes[axis].emplace_back(true, aabb.minimum.get(axis), proxy.downgrade());
        boubleDown(axis, i, objects);
        m_axes[axis].emplace_back(false, aabb.maximum.get(axis), proxy.downgrade());
        boubleDown(axis, i+1, objects);
    }
}

void PersistentSAP::update(const Objects &objects) {
    for (std::size_t axis = 0; axis < 3; ++axis) {
        for (std::size_t i = 0; i < m_axes[axis].size(); ++i) {
            AABB aabb = m_axes[axis][i].proxy.upgrade_const(objects)->getBoundingAABB();
            if (m_axes[axis][i].start) {
                m_axes[axis][i].point = aabb.minimum.get(axis);
            } else {
                m_axes[axis][i].point = aabb.maximum.get(axis);
            }
            boubleDown(axis, i, objects);
        }
    }
}