#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <iostream>
#include <functional>

template<typename T, std::size_t N>
class MultiheadLinkedList {
public:
	using Index = unsigned long;
	constexpr static Index INV_INDEX = -1;
	struct Node {
		T data;
		Index next;
	};

	MultiheadLinkedList() {
		std::fill(heads.begin(), heads.end(), INV_INDEX);
	}

	void insert(std::size_t key, T data) {
		if (freeHead != INV_INDEX) {
			Index index = freeHead;
			freeHead = memoryPool[index].next;

			memoryPool[index].data = data;
			memoryPool[index].next = heads[key];
			heads[key] = index;
		} else {
			memoryPool.emplace_back(data, heads[key]);
			heads[key] = memoryPool.size() - 1;
		}
	}

	void clear() {
		for (std::size_t head = 0; head < N; ++head) {
			freeHead = clear_internal(heads[head]);
			heads[head] = INV_INDEX;
		}
	}

	Index clear_internal(Index index) {
		if (index == INV_INDEX) {
			return freeHead;
		}

		memoryPool[index].next = clear_internal(memoryPool[index].next);
		return index;
	}

	void remove(std::size_t key, const T& value) {
		heads[key] = remove_internal(heads[key], value);
	}

	Index getHead(std::size_t key) {
		return heads[key];
	}

	auto begin() const {
		return heads.begin();
	}

	auto end() const {
		return heads.end();
	}

	const Node& get(Index i) const {
		return memoryPool[i];
	}

	Node& get(Index i) {
		return memoryPool[i];
	}

protected:
	Index remove_internal(Index index, const T& value) {
		if (index == INV_INDEX) {
			return INV_INDEX;
		}

		if (memoryPool[index].data == value) {
			Index next = memoryPool[index].next;
			memoryPool[index].next = freeHead;
			freeHead = index;

			return next;
		}
		
		memoryPool[index].next = remove_internal(memoryPool[index].next, value);

		return index;
	}

	std::array<Index, N> heads;
	std::vector<Node> memoryPool;
	Index freeHead = INV_INDEX;
};