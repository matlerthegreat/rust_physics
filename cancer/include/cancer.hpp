#include "engine.hpp"
#include "object.hpp"
#include "scalars.hpp"

#ifdef __cplusplus
extern "C" {
#endif
 
Engine* engine_construct_naive();
Engine* engine_construct_hash_grid();
Engine* engine_construct_sap();

void engine_destructor(Engine* engine);

WeakProxy engine_insert(Engine* engine, Object& object);
Vector engine_get_position(const Engine* engine, WeakProxy proxy);
void engine_update(Engine* engine, Scalar time);

Engine::ConstIterator* engine_iterate(const Engine* engine, Engine::ConstIterator* iterator, WeakProxy* out_proxy, Object* out_object);

#ifdef __cplusplus
}
#endif