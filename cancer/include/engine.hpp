#pragma once

#include "object.hpp"
#include "scalars.hpp"
#include "broad_phase.hpp"

#include <memory>
#include <vector>

class Engine {
public:
    typedef Arena<Object>::ConstIterator ConstIterator;
    Engine(std::unique_ptr<BroadPhase> accel);
    ~Engine();

	WeakProxy insert(Object object);
    const Vector getObjectPosition(WeakProxy proxy) const;

    void update(Scalar time);
    void regenerate();

    ConstIterator begin() const;
    ConstIterator end() const;

private:
	Objects m_objects;
    std::unique_ptr<BroadPhase> m_accel;
};
