#include <cstddef>
#include <vector>
#include <iostream>

using Gen = unsigned short;
using Offset = unsigned short;

const Gen InvalidGen = -1;
const Offset InvalidOffset = -1;

template<typename T>
struct Entry {
    Entry(T in_value) : value(in_value)
    {}

    union {
        Offset next;
        T value;
    };
    Gen gen = 0;
};

struct WeakProxy;

template<typename T>
struct StrongProxy;

template<typename T>
class Arena;

struct WeakProxy {
    Gen gen;
    Offset offset;

    std::strong_ordering operator<=>(const WeakProxy &rhs) const {
        if (offset == rhs.offset) {
            return gen <=> rhs.gen;
        } else {
            return offset <=> rhs.offset;
        }
    }

    constexpr bool operator==(WeakProxy const&) const = default;

    template<typename T>
    StrongProxy<T> upgrade(Arena<T>& arena);

    template<typename T>
    StrongProxy<const T> upgrade_const(const Arena<T>& arena);
};

template<typename T>
struct StrongProxy {
    Gen gen;
    Offset offset;
    T& value;

    WeakProxy downgrade() const;
    T* operator->() { return &value; }
    const T* operator->() const { return &value; }
    T& operator&() {return value; }
};


template<typename T>
class Arena {
public:
    WeakProxy insert(T value) {
        if (m_free == InvalidOffset) {
            m_entries.emplace_back(value);

            return WeakProxy{m_entries.back().gen, (Offset)(m_entries.size()) - 1u};
        } else {
            Offset offset = m_free;
            Entry<T>& entry = m_entries[offset];
            m_free = entry.next;
            entry.value = value;

            return WeakProxy{entry.gen, offset};
        }
    }

    const T& get(WeakProxy proxy) const {
        const Entry<T>& entry = m_entries[proxy.offset];
        if (entry.gen == proxy.gen) {
            return entry.value;
        }
    }

    T& get(WeakProxy proxy) {
        Entry<T>& entry = m_entries[proxy.offset];
        if (entry.gen == proxy.gen) {
            return entry.value;
        }
    }

    std::size_t size() const {
        std::size_t entriesCount = m_entries.size();
        std::size_t emptyEntriesCount = 0;

        Offset node = m_free;
        while (node != InvalidOffset) {
            ++emptyEntriesCount;
            node = m_entries[node].next;
        }

        return entriesCount - emptyEntriesCount;
    }

    struct Iterator {
        Iterator(Entry<T>* data, Offset offset, Offset maxOffset) : m_data(data), m_offset(offset), m_maxOffset(maxOffset)
        {}

        using difference_type   = std::ptrdiff_t;
        using value_type        = T;
        using pointer           = value_type*;
        using reference         = value_type&;

        reference operator*() const { return (m_data + m_offset)->value; }
        pointer operator->() { return &(m_data + m_offset)->value; }
        Iterator& operator++() { do {m_offset++;} while (m_offset <= m_maxOffset && (m_data+m_offset)->gen == InvalidGen); return *this; }
        Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }
        friend bool operator == (const Iterator& a, const Iterator& b) { return a.m_offset == b.m_offset; };
        friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_offset != b.m_offset; };  

        WeakProxy weak() { return WeakProxy{m_data[m_offset].gen, m_offset}; }
        StrongProxy<T> strong() { return StrongProxy{m_data[m_offset].gen, m_offset, m_data[m_offset].value}; }
        StrongProxy<const T> strong_const() const { return StrongProxy<const T>{m_data[m_offset].gen, m_offset, m_data[m_offset].value}; }

    private:
        Entry<T>* m_data;
        Offset m_offset;
        Offset m_maxOffset;
    };

    struct ConstIterator {
        ConstIterator(const Entry<T>* data, Offset offset, Offset maxOffset) : m_data(data), m_offset(offset), m_maxOffset(maxOffset)
        {}

        using difference_type   = std::ptrdiff_t;
        using value_type        = const T;
        using pointer           = value_type*;
        using reference         = value_type&;

        reference operator*() const { return (m_data + m_offset)->value; }
        pointer operator->() { return &(m_data + m_offset)->value; }
        ConstIterator& operator++() { do {m_offset++;} while (m_offset <= m_maxOffset && (m_data+m_offset)->gen == InvalidGen); return *this; }
        ConstIterator operator++(int) { ConstIterator tmp = *this; ++(*this); return tmp; }
        friend bool operator == (const ConstIterator& a, const ConstIterator& b) { return a.m_offset == b.m_offset; };
        friend bool operator!= (const ConstIterator& a, const ConstIterator& b) { return a.m_offset != b.m_offset; };  

        WeakProxy weak() { return WeakProxy{m_data[m_offset].gen, m_offset}; }
        StrongProxy<const T> strong() { return StrongProxy<const T>{m_data[m_offset].gen, m_offset, m_data[m_offset].value}; }

    private:
        const Entry<T>* m_data;
        Offset m_offset;
        Offset m_maxOffset;
    };


    //static_assert(std::forward_iterator<Iterator>);

    Iterator begin() {
        if (m_entries.empty()) {
            return end();
        }
        Offset offset = 0;
        Offset maxOffset = m_entries.size() - 1;
        while (m_entries.size() <= maxOffset && m_entries[offset].gen == InvalidGen) {
            ++offset;
        }
        return Iterator(m_entries.data(), offset, maxOffset);
    }

    Iterator end() {
        Offset maxOffset = m_entries.size() - 1;
        return Iterator(m_entries.data(), maxOffset + 1, maxOffset);
    }

    ConstIterator begin() const {
        if (m_entries.empty()) {
            return end();
        }
        Offset offset = 0;
        Offset maxOffset = m_entries.size() - 1;
        while (m_entries.size() <= maxOffset && m_entries[offset].gen == InvalidGen) {
            ++offset;
        }
        return ConstIterator(m_entries.data(), offset, maxOffset);
    }

    ConstIterator end() const {
        Offset maxOffset = m_entries.size() - 1;
        return ConstIterator(m_entries.data(), maxOffset + 1, maxOffset);
    }

private:
    std::vector<Entry<T>> m_entries;
    Offset m_free = InvalidOffset;
};

template<typename T>
StrongProxy<T> WeakProxy::upgrade(Arena<T> &arena){
    return StrongProxy<T>{gen, offset, arena.get(*this)};
}

template<typename T>
StrongProxy<const T> WeakProxy::upgrade_const(const Arena<T> &arena) {
    return StrongProxy<const T>{gen, offset, arena.get(*this)};
}

template<typename T>
WeakProxy StrongProxy<T>::downgrade() const {
    return WeakProxy{gen, offset};
}