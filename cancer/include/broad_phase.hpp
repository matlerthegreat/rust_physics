#pragma once

#include <unordered_set>
#include <cstddef>
#include <utility>
#include <variant>
#include <vector>
#include <array>
#include <map>
#include <set>

#include "object.hpp"
#include "multihead_linked_list.hpp"
#include "shape.hpp"

class CollisionPair {
public:
	CollisionPair(WeakProxy first, WeakProxy second)
		: m_first(first), m_second(second)
	{
		if (m_first > m_second) {
			std::swap(m_first, m_second);
		}
		assert(m_first < m_second);
	}

	WeakProxy get(bool second) const {
		if (second) return m_second;
		else return m_first;
	}

	/*std::strong_ordering operator<=>(const CollisionPair &rhs) const {
		assert(m_first < m_second);
		assert(rhs.m_first < rhs.m_second);
		if ((m_first <=> rhs.m_first) == std::strong_ordering::equal) {
			return m_second <=> rhs.m_second;
		} else {
			return m_first <=> rhs.m_first;
		}
	}*/
	friend auto operator<=>(const CollisionPair&, const CollisionPair&) = default;

private:
	WeakProxy m_first;
	WeakProxy m_second;
};

namespace std
{
    template <>
    struct hash<WeakProxy>
    {
        size_t operator()(WeakProxy const & v) const noexcept
        {
            return (
                std::hash<Gen>()(v.gen) ^ std::hash<Offset>()(v.offset)
            );
        }
    };

    template <>
    struct hash<CollisionPair>
    {
        size_t operator()(CollisionPair const & v) const noexcept
        {
            return (
                std::hash<WeakProxy>()(v.get(false)) ^ std::hash<WeakProxy>()(v.get(true))
            );
        }
    };
}

class BroadPhase
{
public:
	virtual ~BroadPhase() {};
	virtual void registerObject(const StrongProxy<const Object>& proxy, const Objects& objects) {};
	virtual void update(const Objects& objects) {};

	virtual std::size_t calcCollisions(const Objects& objects) {
		return m_collisions.size();
	};

	const std::unordered_set<CollisionPair>& getCollisions() const {
		return m_collisions;
	};

protected:
	std::unordered_set<CollisionPair> m_collisions;
};

class BruteForce: public BroadPhase {
public:
	virtual std::size_t calcCollisions(const Objects& objects) override;
};

class SpatialHashGrid: public BroadPhase {
public:
	constexpr static std::size_t SIZE = 1024;
	virtual void registerObject(const StrongProxy<const Object>& proxy, const Objects& objects) override;
	virtual void update(const Objects& objects) override;

	virtual std::size_t calcCollisions(const Objects& objects) override;

private:
	std::size_t spatialHash(int x, int y, int z);

	MultiheadLinkedList<WeakProxy, SIZE> m_proxyLists;
};

class PersistentSAP: public BroadPhase {
public:
	virtual void registerObject(const StrongProxy<const Object>& proxy, const Objects& objects) override;
	virtual void update(const Objects& objects) override;

private:
	struct EndPoint {
		bool start;
		Scalar point;
		WeakProxy proxy;
	};
	typedef std::vector<EndPoint> Axis;

	void boubleDown(std::size_t axis, std::size_t i, const Objects& objects);

	std::array<Axis, 3> m_axes;
};