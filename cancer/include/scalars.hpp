#pragma once
#include <cmath>
#include <cstddef>
#include <functional>
#include <type_traits>

using Scalar = float;
using Integer = int;

template<typename T>
struct TVector {
	T x, y, z;

	static TVector<T> lift(T s) {
		return TVector<T>{s, s, s};
	}
	
	template<typename U, typename F>
	TVector<U> map(F f) const {
		return TVector<U>{f(x), f(y), f(z)};
	}

	TVector<T> operator+(const TVector<T>& lhs) const {
		return TVector<T>{ x + lhs.x, y + lhs.y, z + lhs.z };
	}

	TVector<T>& operator+=(const TVector<T>& lhs) {
		x += lhs.x;
		y += lhs.y;
		z += lhs.z;
		return *this;
	}

	TVector<T> operator+(T lhs) const {
		return TVector<T>{ x + lhs, y + lhs, z + lhs };
	}

	TVector<T> operator-(const TVector<T>& lhs) const {
		return TVector<T>{ x - lhs.x, y - lhs.y, z - lhs.z };
	}

	TVector<T>& operator-=(const TVector<T>& lhs) {
		x -= lhs.x;
		y -= lhs.y;
		z -= lhs.z;
		return *this;
	}

	TVector<T> operator-(const T lhs) const {
		return TVector<T>{ x - lhs, y - lhs, z - lhs };
	}

	TVector<T> operator*(const T lhs) const {
		return TVector<T>{ x * lhs, y * lhs, z * lhs };
	}

	TVector<T> operator/(const T lhs) const {
		return TVector<T>{ x / lhs, y / lhs, z / lhs };
	}

	TVector<T> operator-() const {
		return TVector<T>{ -x, -y, -z };
	}

	//template<std::enable_if_t<std::is_floating_point<T>::value, bool> = true>
	T dot(const TVector<T>& other) const {
		return x * other.x + y * other.y + z * other.z;
	}

	//template<std::enable_if_t<std::is_floating_point<T>::value, bool> = true>
	T length_squared() const {
		return dot(*this);
	}

	//template<std::enable_if_t<std::is_floating_point<T>::value, bool> = true>
	T length() const {
		return sqrt(length_squared());
	}

	T get(std::size_t axis) const {
		switch (axis) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
		}
	}

	//template<std::enable_if_t<std::is_floating_point<T>::value, bool> = true>
	TVector<Integer> floor() const {
		return TVector<Integer>{(Integer)floorf(x), (Integer)floorf(y), (Integer)floorf(z)};
	}

	//template<std::enable_if_t<std::is_floating_point<T>::value, bool> = true>
	TVector<Integer> ceil() const {
		return TVector<Integer>{(Integer)ceilf(x), (Integer)ceilf(y), (Integer)ceilf(z)};
	}
};

using Vector = TVector<Scalar>;
using IVector = TVector<Integer>;
using Point = Vector;

const Vector PrimaryAxes[3] = {
	Vector{1, 0, 0},
	Vector{0, 1, 0},
	Vector{0, 0, 1}
};

Scalar distance(const Point& v1, const Point& v2);
Scalar distance_squared(const Point& v1, const Point& v2);

Vector normalize(Vector v);
