#include "scalars.hpp"
#include "object.hpp"
#include "engine.hpp"

/// <div rustbindgen replaces="Vector"></div>
struct OpaqueVector {
	Scalar x;
	Scalar y;
	Scalar z;
};

/// <div rustbindgen replaces="Sphere"></div>
struct OpaqueSphere {
	Scalar radius;
};

/// <div rustbindgen replaces="Object"></div>
struct OpaqueObject {
	Shape shape;
	Vector p;
	Vector prev_p;
};

/**
 * <div rustbindgen opaque></div>
 * <div rustbindgen nocopy></div>
 */
class Engine;
