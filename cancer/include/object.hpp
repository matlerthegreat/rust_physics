#pragma once

#include "shape.hpp"
#include "arena.hpp"

struct Intersection {
	Scalar penetration;
	Vector normal;
};

struct Object {
	Shape shape;
	Vector p;
	Vector prev_p;
	float inv_m;

	Intersection intersects(const Object& other) const;
	AABB getBoundingAABB() const;
	AABB getPrevBoundingAABB() const;
};

using Objects = Arena<Object>;