#pragma once

#include "scalars.hpp"
#include <variant>
#include <vector>


struct AABB {
	Point minimum;
	Point maximum;

	bool intersects(const AABB& other) const;
};

struct Sphere {
	Scalar radius;
};

struct Box {
	Vector extents;
};

enum ShapeType {
	SHAPE_SPHERE,
	SHAPE_BOX
};

struct Shape {
	ShapeType shapeType;
	union {
		Sphere sphere;
		Box box;
	};
};
