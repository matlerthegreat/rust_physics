use std::collections::HashSet;

pub use crab_common::arena::WeakProxy;

use crab_broad_phase::{
    brute_force,
    spatial_hash_grid::{NaiveHashGrid, PooledHashGrid, StaticHashGrid},
    sweep_and_prune::PersistentSAP,
};
use crab_common::{
    arena::{Arena, StrongProxy},
    math::{Scalar, Vector, Quaterion},
    objects::Object, iterator::CombinationsExt, shapes::Shape,
};

#[allow(clippy::large_enum_variant)]
#[derive(Debug)]
pub enum BroadPhaseAccel {
    None,
    NaiveHashGrid(NaiveHashGrid),
    StaticHashGrid(StaticHashGrid<1024>),
    PooledHashGrid(PooledHashGrid<1024>),
    SAP(PersistentSAP),
}

impl BroadPhaseAccel {
    fn insert(&mut self, proxy: WeakProxy, objects: &Arena<Object>) {
        match self {
            BroadPhaseAccel::None => (),
            BroadPhaseAccel::NaiveHashGrid(accel) => accel.insert(objects.upgrade(&proxy).unwrap()),
            BroadPhaseAccel::StaticHashGrid(accel) => {
                accel.insert(objects.upgrade(&proxy).unwrap())
            }
            BroadPhaseAccel::PooledHashGrid(accel) => {
                accel.insert(objects.upgrade(&proxy).unwrap())
            }
            BroadPhaseAccel::SAP(accel) => accel.insert(proxy, objects),
        }
    }

    fn update(&mut self, objects: &Arena<Object>) {
        match self {
            BroadPhaseAccel::None => (),
            BroadPhaseAccel::NaiveHashGrid(accel) => accel.update(objects),
            BroadPhaseAccel::StaticHashGrid(accel) => accel.update(objects),
            BroadPhaseAccel::PooledHashGrid(accel) => accel.update(objects),
            BroadPhaseAccel::SAP(accel) => accel.update(objects),
        }
    }

    fn get_intersections(&self, objects: &Arena<Object>) -> HashSet<(WeakProxy, WeakProxy)> {
        match self {
            BroadPhaseAccel::None => brute_force::naive(objects.iter()).collect(),
            BroadPhaseAccel::NaiveHashGrid(accel) => accel.get_intersections(objects).collect(),
            BroadPhaseAccel::StaticHashGrid(accel) => accel.get_intersections(objects).collect(),
            BroadPhaseAccel::PooledHashGrid(accel) => accel.get_intersections(objects).collect(),
            BroadPhaseAccel::SAP(accel) => accel.get_intersections().collect(),
        }
    }

    fn remove(&mut self, proxy: &WeakProxy, objects: &Arena<Object>) {
        match self {
            BroadPhaseAccel::None => (),
            BroadPhaseAccel::NaiveHashGrid(accel) => accel.remove(proxy, objects),
            BroadPhaseAccel::StaticHashGrid(accel) => accel.remove(proxy, objects),
            BroadPhaseAccel::PooledHashGrid(accel) => accel.remove(proxy, objects),
            BroadPhaseAccel::SAP(accel) => accel.remove(proxy),
        }
    }
}

#[derive(Debug)]
pub struct Crab {
    pub objects: Arena<Object>,
    broad_phase_accel: BroadPhaseAccel,
}

impl Crab {
    pub fn new_naive() -> Self {
        Self {
            objects: Arena::default(),
            broad_phase_accel: BroadPhaseAccel::None,
        }
    }

    pub fn new_simple() -> Self {
        Self {
            objects: Arena::default(),
            broad_phase_accel: BroadPhaseAccel::NaiveHashGrid(Default::default()),
        }
    }

    pub fn new_static() -> Self {
        Self {
            objects: Arena::default(),
            broad_phase_accel: BroadPhaseAccel::StaticHashGrid(Default::default()),
        }
    }

    pub fn new_pooled() -> Self {
        Self {
            objects: Arena::default(),
            broad_phase_accel: BroadPhaseAccel::PooledHashGrid(Default::default()),
        }
    }

    pub fn new_sap() -> Self {
        Self {
            objects: Arena::default(),
            broad_phase_accel: BroadPhaseAccel::SAP(PersistentSAP::new()),
        }
    }

    pub fn insert(&mut self, object: Object) -> WeakProxy {
        let proxy = self.objects.insert(object);

        self.broad_phase_accel.insert(proxy, &self.objects);

        proxy
    }

    pub fn remove(&mut self, proxy: &WeakProxy) {
        self.broad_phase_accel.remove(proxy, &self.objects);
        self.objects.remove(proxy).unwrap();
    }

    pub fn get_object_position(&self, proxy: &WeakProxy) -> Vector {
        let object = self.objects.upgrade(proxy).unwrap();
        object.p
    }

    /*pub fn get_object_rotation(&self, proxy: &WeakProxy) -> Quaterion {
        let object = self.objects.upgrade(proxy).unwrap();
        object.r
    }*/

    pub fn update(&mut self, t: Scalar) {
        self.objects.values_mut().for_each(|object| {
            let dx = object.p - object.prev_p;
            object.prev_p = object.p;

            if object.inv_m > 0.01 {
                let g = 100.0;
                let v = dx / t * 0.95;
                let a = Vector::OY * -g;
                object.p += v * t + a * 0.5 * t * t;
            };

            let aabb = object.get_bounding_aabb();

            let size = 10.0;

            if aabb.minimum.x < -size {
                object.p.x += -size - aabb.minimum.x;
            } else if aabb.maximum.x > size {
                object.p.x += size - aabb.maximum.x;
            }

            if aabb.minimum.y < -size {
                object.p.y += -size - aabb.minimum.y;
            } else if aabb.maximum.y > size {
                object.p.y += size - aabb.maximum.y;
            }

            if aabb.minimum.z < -size {
                object.p.z += -size - aabb.minimum.z;
            } else if aabb.maximum.z > size {
                object.p.z += size - aabb.maximum.z;
            }
        });

        //self.broad_phase_accel.update(&self.objects);

        //let candidates = self.broad_phase_accel.get_intersections(&self.objects);
        let candidates: Vec<_> = self.objects.iter().map(|p| StrongProxy::downgrade(&p)).combinations().collect();

        for (a, b) in candidates {
            if a == b {
                continue;
            }

            let at = self.objects.upgrade(&a).unwrap();
            let bt = self.objects.upgrade(&b).unwrap();

            let m = at.inv_m + bt.inv_m;

            if let Some((penetration, normal)) = at.intersects(&bt) {
                let diff = normal * penetration;
                let mut at = self.objects.upgrade_mut(&a).unwrap();
                let i = at.inv_m / m;
                at.p += diff * i;

                let mut bt = self.objects.upgrade_mut(&b).unwrap();
                let i = bt.inv_m / m;
                bt.p -= diff * i;
            }
        }

        /*let rot = Quaterion::from_rotation(0.1, Vector::OZ);
        for mut object in self.objects.iter_mut() {
            object.r *= rot;
        }*/
    }

    pub fn extend<'a>(
        &'a mut self,
        objects: impl IntoIterator<Item = Object> + 'a,
    ) -> impl Iterator<Item = WeakProxy> + 'a {
        self.objects.extend(objects)
    }
}

impl IntoIterator for Crab {
    type Item = (WeakProxy, Object);

    type IntoIter = <Arena<Object> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.objects.into_iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crab_common::{
        objects::Object,
        shapes::{Box, Shape, Sphere},
    };

    #[test]
    fn sphere_vs_sphere() {
        let a = Object::new(
            Shape::Sphere(Sphere::new(1.0)),
            Vector::new(-0.5, -9.0, 0.0),
        );
        let b = Object::new(Shape::Sphere(Sphere::new(1.0)), Vector::new(0.5, -9.0, 0.0));

        let mut engine = Crab::new_naive();
        let a = engine.insert(a);
        let b = engine.insert(b);

        engine.update(0.1333);

        assert_eq!(engine.get_object_position(&a), Vector::new(-1.0, -9.0, 0.0));
        assert_eq!(engine.get_object_position(&b), Vector::new(1.0, -9.0, 0.0));
    }

    #[test]
    fn box_vs_sphere() {
        let a = Object::new(
            Shape::Box(Box::new(Vector::lift(1.0))),
            Vector::new(-0.5, -9.0, 0.0),
        );
        let b = Object::new(Shape::Sphere(Sphere::new(1.0)), Vector::new(0.5, -9.0, 0.0));

        let mut engine = Crab::new_naive();
        let a = engine.insert(a);
        let b = engine.insert(b);

        engine.update(0.1333);

        assert_eq!(engine.get_object_position(&a), Vector::new(-1.0, -9.0, 0.0));
        assert_eq!(engine.get_object_position(&b), Vector::new(1.0, -9.0, 0.0));
    }

    #[test]
    fn box_vs_box() {
        let a = Object::new(
            Shape::Box(Box::new(Vector::lift(1.0))),
            Vector::new(-0.5, -9.0, 0.0),
        );
        let b = Object::new(
            Shape::Box(Box::new(Vector::lift(1.0))),
            Vector::new(0.5, -9.0, 0.0),
        );

        let mut engine = Crab::new_naive();
        let a = engine.insert(a);
        let b = engine.insert(b);

        engine.update(0.1333);

        assert_eq!(engine.get_object_position(&a), Vector::new(-1.0, -9.0, 0.0));
        assert_eq!(engine.get_object_position(&b), Vector::new(1.0, -9.0, 0.0));
    }
}
