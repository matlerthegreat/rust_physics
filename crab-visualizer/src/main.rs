use std::time::Instant;

mod engine;

use bevy::{prelude::*, utils::HashMap, math::vec3, window::{CursorGrabMode, Window, PresentMode, Cursor}, input::mouse::MouseMotion};
use crab::{self, Crab};
use crab_cancer::Cancer;
use crab_common::{
    math::Vector,
    random::{gen_random_object, gen_random_vector},
    shapes::{Shape, AABB, Plane}, objects::Object,
};
use engine::{Engine, ObjectProxy};
use rand::{rngs::SmallRng, SeedableRng};

#[derive(Resource, Default)]
struct PerfStats {
    objects_count: usize,
    physics_time: f32,
}

#[derive(Component)]
struct PerfStatsComponent;

#[derive(Resource)]
struct Physics {
    engine: Engine,
}

#[derive(Resource)]
struct Rng(SmallRng);

#[derive(Default)]
struct SpawnObjectEvent;

#[derive(Default)]
struct ChangeEngineTypeEvent;

enum ControllerEvent{
    Movement(Vector),
    Rotation(Vec2)
}

pub fn main() {
    let mut cursor = Cursor::default();
    cursor.grab_mode = CursorGrabMode::Locked;
    cursor.visible = false;

    App::new()
        .init_resource::<PerfStats>()
        .insert_resource(Rng(SmallRng::seed_from_u64(6844846589)))
        .insert_resource(Physics {
            engine: Engine::CrabNaive(Crab::new_naive()),
        })
        .add_event::<SpawnObjectEvent>()
        .add_event::<ChangeEngineTypeEvent>()
        .add_event::<ControllerEvent>()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "I am a window!".into(),
                resolution: (500., 300.).into(),
                present_mode: PresentMode::AutoVsync,
                // Tells wasm to resize the window according to the available canvas
                fit_canvas_to_parent: true,
                // Tells wasm not to override default event handling, like F5, Ctrl+R etc.
                prevent_default_event_handling: false,
                cursor,
                ..default()
            }),
            ..default()
        }))
        .add_startup_system(setup)
        .add_system(keyboard_input)
        .add_system(mouse_motion)
        .add_system(object_spawner)
        .add_system(player_controller)
        .add_system(engine_type_changer)
        .add_system(update_perf_stats_display)
        .add_system(update_camera)
        .add_systems(
            (physics_solve, physics_apply.after(physics_solve), object_killer.after(physics_apply))
                .in_schedule(CoreSchedule::FixedUpdate),
        )
        .add_system(bevy::window::close_on_esc)
        .run();
}

#[derive(Component)]
struct Dynamic {
    proxy: ObjectProxy,
    life: usize,
}

#[derive(Component)]
struct Player {
    rot: Vec2
}

impl Player {
    fn get_rotation(&self) -> Quat {
        Quat::from_rotation_y(-self.rot.x / 100.0) * Quat::from_rotation_x(self.rot.y / 500.0)
    }
}

fn keyboard_input(
    keys: Res<Input<KeyCode>>,
    mut ev_spawn_object: EventWriter<SpawnObjectEvent>,
    mut ev_change_engine_type: EventWriter<ChangeEngineTypeEvent>,
    mut ev_movement: EventWriter<ControllerEvent>,
) {
    if keys.pressed(KeyCode::Space) {
        ev_spawn_object.send_default();
    }
    if keys.just_pressed(KeyCode::Return) {
        ev_change_engine_type.send_default();
    }
    if keys.pressed(KeyCode::F) {
        ev_movement.send(ControllerEvent::Movement(Vector::OZ));
    }
}

fn mouse_motion(
    mut motion_evr: EventReader<MouseMotion>,
    mut ev_movement: EventWriter<ControllerEvent>,
) {
    for ev in motion_evr.iter() {
        ev_movement.send(ControllerEvent::Rotation(ev.delta));
    }
}

fn object_spawner(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut physics: ResMut<Physics>,
    mut rng: ResMut<Rng>,
    mut perf_stats: ResMut<PerfStats>,
    mut ev_spawn_object: EventReader<SpawnObjectEvent>,
    player: Query<(&Player, &Dynamic)>,
) {
    let (player, dynamic) = player.get_single().unwrap();
    let vel = from_bevy(player.get_rotation().mul_vec3(vec3(0.0, 0.1, 0.3)));
    let pos = from_bevy(physics.engine.get_object_position(&dynamic.proxy)) + vel.clone().normalize();

    for _ in ev_spawn_object.iter() {
        let bounds = AABB::new(Vector::lift(-10.0), Vector::lift(10.0));
        let mut object = gen_random_object(&mut rng.0, &bounds);
        object.p = pos + vel;
        object.prev_p = pos;

        let mut rotation = Quat::IDENTITY;

        let mesh = match &object.shape {
            Shape::Sphere(sphere) => Mesh::from(shape::UVSphere {
                radius: sphere.radius,
                ..default()
            }),
            Shape::Box(b) => Mesh::from(shape::Box {
                min_x: -b.extents.x,
                max_x: b.extents.x,
                min_y: -b.extents.y,
                max_y: b.extents.y,
                min_z: -b.extents.z,
                max_z: b.extents.z,
            }),
            Shape::Plane(_) => {
                rotation = Quat::from_rotation_arc(vec3(0.0, -1.0, 0.0), Vec3::from_array(object.p.into()));
                Mesh::from(shape::Plane {
                    size: 100.0,
                    ..default()
                })
            },
        };

        let mut entity = commands.spawn(PbrBundle {
            mesh: meshes.add(mesh),
            material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
            transform: Transform::from_xyz(object.p.x, object.p.y, object.p.z) * Transform::from_rotation(rotation),
            ..default()
        });

        let proxy = physics.engine.insert(object);
        entity.insert(Dynamic { proxy, life: rand::Rng::gen_range(&mut rng.0, 100..300) });
        perf_stats.objects_count += 1;
    }
}



fn object_killer(
    mut commands: Commands,
    mut physics: ResMut<Physics>,
    mut perf_stats: ResMut<PerfStats>,
    mut objects: Query<(Entity, &mut Dynamic), Without<Player>>,
) {
    for (entity, mut object) in objects.iter_mut() {
        if object.life > 0 {
            object.life -= 1;
        } else {
            physics.engine.remove(object.proxy);
            perf_stats.objects_count -= 1;
            commands.entity(entity).despawn();
        }
    }
}

fn to_bevy(v: Vector) -> Vec3 {
    vec3(v.x, v.y, v.z)
}

fn from_bevy(v: Vec3) -> Vector {
    Vector::new(v.x, v.y, v.z)
}

fn player_controller(
    mut physics: ResMut<Physics>,
    mut query: Query<(&mut Player, &Dynamic)>,
    mut ev_controller: EventReader<ControllerEvent>,
) {
    for controller in ev_controller.into_iter() {
    let (mut player, dynamic) = query.get_single_mut().unwrap();
        match controller {
            ControllerEvent::Movement(movement) => {
                let rotation = player.get_rotation();
                let d = from_bevy(rotation.mul_vec3(to_bevy(movement.normalize() / 100.0)));
                physics.engine.move_object(dynamic.proxy, d);
            },
            ControllerEvent::Rotation(r) => player.rot += *r,
        }
    }
}

fn engine_type_changer(
    mut physics: ResMut<Physics>,
    mut query: Query<&mut Dynamic>,
    mut ev_change_engine_type: EventReader<ChangeEngineTypeEvent>,
) {
    for _ in ev_change_engine_type.iter() {
        let new_engine = match &physics.engine {
            Engine::CrabNaive(_) => Engine::CrabHashGrid(Crab::new_pooled()),
            Engine::CrabHashGrid(_) => Engine::CrabSAP(Crab::new_sap()),
            Engine::CrabSAP(_) => Engine::CancerNaive(Cancer::naive()),
            Engine::CancerNaive(_) => Engine::CancerHashGrid(Cancer::hash_grid()),
            Engine::CancerHashGrid(_) => Engine::CancerSAP(Cancer::sap()),
            Engine::CancerSAP(_) => Engine::CrabNaive(Crab::new_naive()),
        };

        let old_engine = std::mem::replace(&mut physics.engine, new_engine);

        let mut proxy_map: HashMap<ObjectProxy, ObjectProxy> = HashMap::new();
        match old_engine {
            Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine) => {
                for (old_proxy, object) in engine.into_iter() {
                    let new_proxy = physics.engine.insert(object);
                    proxy_map.insert(old_proxy.into(), new_proxy);
                }
            }
            Engine::CancerNaive(engine)
            | Engine::CancerHashGrid(engine)
            | Engine::CancerSAP(engine) => {
                for (old_proxy, object) in engine.into_iter() {
                    let new_proxy = physics.engine.insert(object);
                    proxy_map.insert(old_proxy.into(), new_proxy);
                }
            }
        };

        query.for_each_mut(|mut dynamic| dynamic.proxy = proxy_map[&dynamic.proxy]);
    }
}

fn physics_solve(mut physics: ResMut<Physics>, mut perf_stats: ResMut<PerfStats>) {
    let start = Instant::now();

    let time = 0.0166;
    physics.engine.update(time);

    let time_ms = start.elapsed().as_secs_f32() * 1000.0;
    perf_stats.physics_time = perf_stats.physics_time * 0.9 + time_ms * 0.1;
}

fn physics_apply(mut query: Query<(&Dynamic, &mut Transform)>, physics: Res<Physics>) {
    for (dynamic, mut transform) in query.iter_mut() {
        *transform = physics.engine.get_object_transform(&dynamic.proxy);
    }
}

fn update_perf_stats_display(
    perf_stats: Res<PerfStats>,
    physics: Res<Physics>,
    mut query: Query<&mut Text, With<PerfStatsComponent>>,
) {
    for mut text in &mut query {
        let engine_type = &physics.engine;
        text.sections[1].value = format!("{engine_type}");
        let objects_count = perf_stats.objects_count;
        text.sections[3].value = format!("{objects_count}");
        let physics_time = perf_stats.physics_time;
        text.sections[5].value = format!("{physics_time}");
    }
}

fn update_camera(
    mut camera: Query<&mut Transform, With<Camera>>,
    physics: Res<Physics>,
    player: Query<(&Player, &Dynamic)>,
) {
    let mut camera = camera.get_single_mut().unwrap();
    let (player, dynamic) = player.get_single().unwrap();

    let pos = physics.engine.get_object_transform(&dynamic.proxy);
    let rot = Transform::from_rotation(player.get_rotation());

    //let transform = pos * rot * Transform;
    let transform =  (pos * rot * Transform::from_xyz(0.0, 0.0, -5.0))
            .looking_at(pos.translation, Vec3::Y);

    *camera = transform;
}

/// set up a simple 3D scene
fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut physics: ResMut<Physics>,
    asset_server: Res<AssetServer>,
) {
    // plane
    commands.spawn(PbrBundle {
        mesh: meshes.add(shape::Plane::from_size(20.0).into()),
        material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
        transform: Transform::from_xyz(0.0, -10.0, 0.0),
        ..default()
    });
    // light
    commands.spawn(PointLightBundle {
        point_light: PointLight {
            intensity: 15000.0,
            shadows_enabled: true,
            range: 1000.0,
            ..default()
        },
        transform: Transform::from_xyz(40.0, 40.0, 40.0),
        ..default()
    });
    // camera
    commands.spawn(Camera3dBundle {
        transform: Transform::from_xyz(-20.0, 20.0, 30.0)
            .looking_at(Vec3::new(0.0, -2.0, 0.0), Vec3::Y),
        ..default()
    });
    // player
    let object = Object::new(Shape::sphere(0.5), Vector::lift(0.0));
    let mesh = Mesh::from(shape::UVSphere {
        radius: 0.5,
        ..default()
    });
    let mut entity = commands.spawn(PbrBundle {
        mesh: meshes.add(mesh),
        material: materials.add(Color::rgb(1.0, 0.0, 0.0).into()),
        ..default()
    });

    let proxy = physics.engine.insert(object);
    entity.insert(Dynamic { proxy, life: 0 });
    entity.insert(Player { rot: Vec2::ZERO });

    let font_size = 17.0;

    commands.spawn((
        // Create a TextBundle that has a Text with a list of sections.
        TextBundle::from_sections([
            TextSection::new(
                "Physics engine ",
                TextStyle {
                    font_size,
                    color: Color::BLACK,
                    font: asset_server.load("/usr/share/fonts/gnu-free/FreeMono.otf"),
                },
            ),
            TextSection::from_style(TextStyle {
                font_size,
                color: Color::GOLD,
                font: asset_server.load("/usr/share/fonts/gnu-free/FreeMono.otf"),
            }),
            TextSection::new(
                "\nObjects count: ",
                TextStyle {
                    font_size,
                    color: Color::BLACK,
                    font: asset_server.load("/usr/share/fonts/gnu-free/FreeMono.otf"),
                },
            ),
            TextSection::from_style(TextStyle {
                font_size,
                color: Color::GOLD,
                font: asset_server.load("/usr/share/fonts/gnu-free/FreeMono.otf"),
            }),
            TextSection::new(
                "\nPhysics time (ms): ",
                TextStyle {
                    font_size,
                    color: Color::BLACK,
                    font: asset_server.load("/usr/share/fonts/gnu-free/FreeMono.otf"),
                },
            ),
            TextSection::from_style(TextStyle {
                font_size,
                color: Color::GOLD,
                font: asset_server.load("/usr/share/fonts/gnu-free/FreeMono.otf"),
            }),
        ]),
        PerfStatsComponent,
    ));

    /*let mut floor = Object::new(Shape::Plane(Plane::new(-10.0)), Vector::new(0.0, 1.0, 0.0));
    floor.inv_m = 0.0;
    physics.engine.insert(floor);*/
}
