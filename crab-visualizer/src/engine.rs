use bevy::{prelude::{Transform, Quat, Vec3}, math::vec3};
use crab::{self, Crab};
use crab_cancer::{self as cancer, Cancer};
use crab_common::{objects::Object, math::Vector};

#[derive(Debug)]
pub enum Engine {
    CrabNaive(Crab),
    CrabHashGrid(Crab),
    CrabSAP(Crab),
    CancerNaive(Cancer),
    CancerHashGrid(Cancer),
    CancerSAP(Cancer),
}

impl Engine {
    pub fn insert(&mut self, object: Object) -> ObjectProxy {
        match self {
            Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine) => {
                engine.insert(object).into()
            }
            Engine::CancerNaive(engine)
            | Engine::CancerHashGrid(engine)
            | Engine::CancerSAP(engine) => engine.insert(object).into(),
        }
    }

    pub fn update(&mut self, time: f32) {
        match self {
            Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine) => {
                engine.update(time)
            }
            Engine::CancerNaive(engine)
            | Engine::CancerHashGrid(engine)
            | Engine::CancerSAP(engine) => engine.update(time),
        }
    }

    pub fn get_object_transform(&self, proxy: &ObjectProxy) -> Transform {
        let translation = self.get_object_position(proxy);
        //let rotation = self.get_object_rotation(proxy);
        Transform { translation, rotation: Quat::IDENTITY, scale: Vec3::ONE }
    }

    pub fn get_object_position(&self, proxy: &ObjectProxy) -> Vec3 {
        let v = match (self, proxy) {
            (
                Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine),
                ObjectProxy::Crab(proxy),
            ) => engine.get_object_position(proxy),
            (
                Engine::CancerNaive(engine)
                | Engine::CancerHashGrid(engine)
                | Engine::CancerSAP(engine),
                ObjectProxy::Cancer(proxy),
            ) => engine.get_object_position(proxy),
            _ => panic!("Engine - proxy missmatch"),
        };

        vec3(v.x, v.y, v.z)
    }

    /*pub fn get_object_rotation(&self, proxy: &ObjectProxy) -> Quat {
        match (self, proxy) {
            (
                Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine),
                ObjectProxy::Crab(proxy),
            ) => {
                let q = engine.get_object_rotation(proxy);
                let (s, v) = q.into_parts();
                Quat::from_xyzw(v.x, v.y, v.z, s)
            },
            (
                Engine::CancerNaive(engine)
                | Engine::CancerHashGrid(engine)
                | Engine::CancerSAP(engine),
                ObjectProxy::Cancer(proxy),
            ) => todo!(),
            _ => panic!("Engine - proxy missmatch"),
        }
    }*/

    pub fn move_object(&mut self, proxy: ObjectProxy, d: Vector) {
        match (self, proxy) {
            (
                Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine),
                ObjectProxy::Crab(proxy),
            ) => {
                let object = engine.objects.get_mut(&proxy).unwrap();
                object.p += d;
            },
            (
                Engine::CancerNaive(engine)
                | Engine::CancerHashGrid(engine)
                | Engine::CancerSAP(engine),
                ObjectProxy::Cancer(proxy),
            ) => (),
            _ => panic!("Engine - proxy missmatch"),
        }
    }

    pub fn remove(&mut self, proxy: ObjectProxy) {
        match (self, proxy) {
            (
                Engine::CrabNaive(engine) | Engine::CrabHashGrid(engine) | Engine::CrabSAP(engine),
                ObjectProxy::Crab(proxy),
            ) => {
                engine.remove(&proxy);
            },
            (
                Engine::CancerNaive(engine)
                | Engine::CancerHashGrid(engine)
                | Engine::CancerSAP(engine),
                ObjectProxy::Cancer(proxy),
            ) => (),
            _ => panic!("Engine - proxy missmatch"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ObjectProxy {
    Crab(crab::WeakProxy),
    Cancer(cancer::WeakProxy),
}

impl From<crab::WeakProxy> for ObjectProxy {
    fn from(inner: crab::WeakProxy) -> Self {
        ObjectProxy::Crab(inner)
    }
}

impl From<cancer::WeakProxy> for ObjectProxy {
    fn from(inner: cancer::WeakProxy) -> Self {
        ObjectProxy::Cancer(inner)
    }
}

impl std::fmt::Display for Engine {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Engine::CrabNaive(_) => write!(f, "Rust Naive"),
            Engine::CrabHashGrid(_) => write!(f, "Rust HashGrid"),
            Engine::CrabSAP(_) => write!(f, "Rust SAP"),
            Engine::CancerNaive(_) => write!(f, "C++ Naive"),
            Engine::CancerHashGrid(_) => write!(f, "C++ HashGrid"),
            Engine::CancerSAP(_) => write!(f, "C++ SAP"),
        }
    }
}
