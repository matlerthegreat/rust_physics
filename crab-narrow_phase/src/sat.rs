use std::cmp::min;

use crab_common::{math::{Vector, Scalar}, shapes::{Box, Sphere}, objects::Object, arena::{WeakProxy, StrongProxy}, iterator::MinMaxExt};

#[derive(Debug)]
pub struct Collision {
    //pub penetration: Scalar,
    //pub normal: Vector,
    pub penetration: Vector,
    pub point: Vector, // point inside the intersecting region
}

impl PartialEq for Collision {
    fn eq(&self, other: &Self) -> bool {
        self.penetration.eq(&other.penetration)
    }
}

impl PartialOrd for Collision {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.penetration.length_squared().partial_cmp(&other.penetration.length_squared())
    }
}

impl Collision {
    fn min(self, rhs: Collision) -> Collision {
        if self < rhs {
            self
        } else {
            rhs
        }
    }
}

// Project a shape onto a vector, returning the bounds (min, max) on the vector
fn project(object: &Object, n: Vector) -> (Scalar, Scalar) {
    match object.shape {
        crab_common::shapes::Shape::Sphere(s) => {
            let p = object.p.dot(n);
            (p - s.radius, p + s.radius)
        },
        crab_common::shapes::Shape::Box(b) => {
            let vertices = 
                [
                    Vector::new( -b.extents.x, -b.extents.y, -b.extents.z),
                    Vector::new( -b.extents.x, -b.extents.y,  b.extents.z),
                    Vector::new( -b.extents.x,  b.extents.y, -b.extents.z),
                    Vector::new( -b.extents.x,  b.extents.y,  b.extents.z),
                    Vector::new(  b.extents.x, -b.extents.y, -b.extents.z),
                    Vector::new(  b.extents.x, -b.extents.y,  b.extents.z),
                    Vector::new(  b.extents.x,  b.extents.y, -b.extents.z),
                    Vector::new(  b.extents.x,  b.extents.y,  b.extents.z),
                ].into_iter()
                .map(|v| object.p + object.r.rotate(v));

            vertices
            .map(|v| v.dot(n))
            .min_max()
            .unwrap()
        },
    }
}

fn get_intersection_along(a: &Object, b: &Object, normal: Vector) -> Scalar {
    let (a_min, a_max) = project(&a, normal);
    let (b_min, b_max) = project(&b, normal);

    (a_max - b_min).min(b_max - a_min)
}

pub fn get_collision(a: &Object, b: &Object) -> Option<Collision> {
    match (a.shape, b.shape) {
        (crab_common::shapes::Shape::Sphere( .. ), crab_common::shapes::Shape::Sphere(..)) => {
            let normal = (a.p - b.p).normalize();
            let depth = get_intersection_along(&a, &b, normal);
            if depth > 0.0 {
                let penetration = normal * depth;
                let point = (a.p + b.p) / 2.0;
                Some( Collision { penetration, point })
            } else {
                None
            }
        }
        (crab_common::shapes::Shape::Sphere(..), crab_common::shapes::Shape::Box(..))
        | (crab_common::shapes::Shape::Box(..), crab_common::shapes::Shape::Sphere(..)) => todo!(),
        (crab_common::shapes::Shape::Box(..), crab_common::shapes::Shape::Box(..)) => {
            let faces = [Vector::OX, Vector::OY, Vector::OZ];

            let a_faces = faces.iter().map(|v| a.r.rotate(*v));
            let b_faces = faces.iter().map(|v| b.r.rotate(*v));

            let edges = a_faces.clone().zip(b_faces.clone()).map(|(v, u)| v.cross(u));

            let axes = a_faces.chain(b_faces).chain(edges);
            let (normal, depth) = axes.map(|normal| (normal, get_intersection_along(&a, &b, normal))).min_by(|(_, lhs), (_, rhs)| lhs.total_cmp(rhs)).unwrap();

            if depth > 0.0 {
                let penetration = normal * depth;
                let point = (a.p + b.p) / 2.0;
                Some( Collision { penetration, point })
            } else {
                None
            }
        },
    }
}

#[cfg(test)]
mod tests {
    use crab_common::iterator::CombinationsExt;
    use crab_common::random::gen_random_vector;
    use crab_common::shapes::AABB;
    use crab_common::{shapes::Shape, random::gen_random_sphere};
    use rand::prelude::*;
    use rand::rngs::SmallRng;

    use super::*;

    #[test]
    fn test_intersections() {
        let x = Object::new(
            Shape::sphere(3.),
            Vector {
                x: 0.,
                y: 0.,
                z: 0.,
            },
        );
        let y = Object::new(
            Shape::sphere(9.),
            Vector {
                x: 0.,
                y: 0.,
                z: 4.,
            },
        );
        let z = Object::new(
            Shape::sphere(8.),
            Vector {
                x: 500.,
                y: 6.,
                z: 7.,
            },
        );

        assert_eq!(get_collision(&x, &y).is_some(), true);
        assert_eq!(get_collision(&x, &z).is_some(), false);
        assert_eq!(get_collision(&y, &z).is_some(), false);
    }



    #[test]
    fn compare_ball_penetration() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(10., 10., 10.));

        let balls: Vec<_> = (0..100).map(|_| gen_random_sphere(&mut rng, 0.5, 1.0)).collect::<Vec<_>>().into_iter().map(|s| Object::new(Shape::Sphere(s), gen_random_vector(&mut rng, &bounds))).collect();

        let mut expected: Vec<_> = balls.iter().combinations().flat_map(|(a, b)| a.intersects(b)).collect();
        expected.sort_unstable_by(|(a, _), (b, _)| a.total_cmp(b));

        let mut collisions: Vec<_> = balls.iter().combinations().flat_map(|(a, b)| get_collision(a, b)).map(|collision| collision.penetration.into_parts()).collect();
        collisions.sort_unstable_by(|(a, _), (b, _)| a.total_cmp(b));

        assert_eq!(expected.len(), collisions.len());

        expected.into_iter().zip(collisions).for_each(|((e_depth, e_normal), (c_depth, c_normal))| {
            assert!((e_depth - c_depth).abs() < 0.01);
            assert!(Vector::distance(e_normal, c_normal) < 0.01);
        });
    }
}
