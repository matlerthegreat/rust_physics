use crab_common::arena::{Arena, StrongProxy, WeakProxy};
use crab_common::iterator::UniqueIndexExt;
use crab_common::math::IVector;
use crab_common::multihead_linked_list::MultiheadLinkedList;
use crab_common::objects::Object;
use crab_common::sort_pair;

use std::collections::HashMap;

use crate::brute_force;

#[derive(Debug)]
pub struct NaiveHashGrid {
    hash_map: HashMap<IVector, Vec<WeakProxy>>,
}

impl NaiveHashGrid {
    pub fn new() -> Self {
        NaiveHashGrid {
            hash_map: HashMap::new(),
        }
    }

    pub fn insert<'a>(&'a mut self, proxy: StrongProxy<'a, Object>) {
        for key in proxy.get_bounding_aabb().get_grid_overlaps() {
            self.hash_map
                .entry(key)
                .or_default()
                .push(StrongProxy::downgrade(&proxy))
        }
    }

    pub fn from<'a>(proxies: impl IntoIterator<Item = StrongProxy<'a, Object>>) -> Self {
        let mut accel = Self::new();
        for proxy in proxies.into_iter() {
            accel.insert(proxy);
        }
        accel
    }

    pub fn get_intersections<'a>(
        &'a self,
        objects: &'a Arena<Object>,
    ) -> impl Iterator<Item = (WeakProxy, WeakProxy)> + 'a {
        self.hash_map.iter().flat_map(|(_, proxies)| {
            let strong_proxies = proxies.iter().map(|proxy| objects.upgrade(proxy).unwrap());
            brute_force::naive(strong_proxies).map(sort_pair)
        })
    }

    pub fn update(&mut self, objects: &Arena<Object>) {
        for proxy in objects.iter() {
            for key in proxy.get_prev_bounding_aabb().get_grid_overlaps() {
                if let Some((index, _)) = self
                    .hash_map
                    .entry(key)
                    .or_default()
                    .iter()
                    .enumerate()
                    .find(|(_, p)| **p == StrongProxy::downgrade(&proxy))
                {
                    self.hash_map.entry(key).or_default().swap_remove(index);
                }
            }

            for key in proxy.get_bounding_aabb().get_grid_overlaps() {
                self.hash_map
                    .entry(key)
                    .or_default()
                    .push(StrongProxy::downgrade(&proxy));
            }
        }
    }

    pub fn remove(&mut self, proxy: &WeakProxy, objects: &Arena<Object>) {
        let object = objects.upgrade(proxy).unwrap();
        for key in object.get_prev_bounding_aabb().get_grid_overlaps() {
            if let Some((index, _)) = self
                .hash_map
                .entry(key)
                .or_default()
                .iter()
                .enumerate()
                .find(|(_, p)| **p == *proxy)
            {
                self.hash_map.entry(key).or_default().swap_remove(index);
            }
        }

        for key in object.get_bounding_aabb().get_grid_overlaps() {
            if let Some((index, _)) = self
                .hash_map
                .entry(key)
                .or_default()
                .iter()
                .enumerate()
                .find(|(_, p)| **p == *proxy)
            {
                self.hash_map.entry(key).or_default().swap_remove(index);
            }
        }
    }
}

impl Default for NaiveHashGrid {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
pub struct StaticHashGrid<const N: usize> {
    hash_table: [Vec<WeakProxy>; N],
}

impl<const N: usize> StaticHashGrid<N> {
    const P1: i32 = 73856093;
    const P2: i32 = 19349663;
    const P3: i32 = 83492791;
    fn hash(&IVector { x, y, z }: &IVector) -> usize {
        (x.wrapping_mul(Self::P1) ^ y.wrapping_mul(Self::P2) ^ z.wrapping_mul(Self::P3)) as usize
            % N
    }

    pub fn new() -> Self {
        StaticHashGrid {
            hash_table: std::array::from_fn(|_| Vec::new()),
        }
    }

    pub fn insert<'a>(&'a mut self, proxy: StrongProxy<'a, Object>) {
        for key in proxy
            .get_bounding_aabb()
            .get_grid_overlaps()
            .map(|v| Self::hash(&v))
            .unique_index::<16>()
        {
            self.hash_table[key].push(StrongProxy::downgrade(&proxy))
        }
    }

    pub fn from<'a>(proxies: impl IntoIterator<Item = StrongProxy<'a, Object>>) -> Self {
        let mut accel = Self::new();
        for proxy in proxies.into_iter() {
            accel.insert(proxy);
        }
        accel
    }

    pub fn get_intersections<'a>(
        &'a self,
        objects: &'a Arena<Object>,
    ) -> impl Iterator<Item = (WeakProxy, WeakProxy)> + 'a {
        self.hash_table.iter().flat_map(|proxies| {
            let strong_proxies = proxies.iter().map(|proxy| objects.upgrade(proxy).unwrap());
            brute_force::naive(strong_proxies).map(sort_pair)
        })
    }

    pub fn update(&mut self, objects: &Arena<Object>) {
        for proxy in objects.iter() {
            let aabb = proxy.get_bounding_aabb();
            let prev_aabb = proxy.get_prev_bounding_aabb();

            let stale_keys = prev_aabb
                .get_grid_overlaps_between(&aabb)
                .map(|v| Self::hash(&v))
                .unique_index::<16>();
            let new_keys = aabb
                .get_grid_overlaps_between(&prev_aabb)
                .map(|v| Self::hash(&v))
                .unique_index::<16>();

            for key in stale_keys {
                if let Some((index, _)) = self.hash_table[key]
                    .iter()
                    .enumerate()
                    .find(|&(_, p)| *p == StrongProxy::downgrade(&proxy))
                {
                    self.hash_table[key].swap_remove(index);
                }
            }

            for key in new_keys {
                self.hash_table[key].push(StrongProxy::downgrade(&proxy));
            }
        }
    }

    pub fn remove(&mut self, proxy: &WeakProxy, objects: &Arena<Object>) {
        let object = objects.upgrade(proxy).unwrap();
        let aabb = object.get_bounding_aabb();
        let prev_aabb = object.get_prev_bounding_aabb();

        let stale_keys = prev_aabb
            .get_grid_overlaps_between(&aabb)
            .map(|v| Self::hash(&v))
            .unique_index::<16>();
        let new_keys = aabb
            .get_grid_overlaps_between(&prev_aabb)
            .map(|v| Self::hash(&v))
            .unique_index::<16>();

        for key in stale_keys {
            if let Some((index, _)) = self.hash_table[key]
                .iter()
                .enumerate()
                .find(|&(_, p)| *p == *proxy)
            {
                self.hash_table[key].swap_remove(index);
            }
        }

        for key in new_keys {
            if let Some((index, _)) = self.hash_table[key]
                .iter()
                .enumerate()
                .find(|&(_, p)| *p == *proxy)
            {
                self.hash_table[key].swap_remove(index);
            }
        }
    }
}

impl<const N: usize> Default for StaticHashGrid<N> {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
pub struct PooledHashGrid<const N: usize> {
    hash_table: MultiheadLinkedList<WeakProxy, N>,
}

impl<const N: usize> PooledHashGrid<N> {
    const P1: i32 = 73856093;
    const P2: i32 = 19349663;
    const P3: i32 = 83492791;
    fn hash(&IVector { x, y, z }: &IVector) -> usize {
        (x.wrapping_mul(Self::P1) ^ y.wrapping_mul(Self::P2) ^ z.wrapping_mul(Self::P3)) as usize
            % N
    }

    pub fn new() -> Self {
        PooledHashGrid {
            hash_table: MultiheadLinkedList::new(),
        }
    }

    pub fn insert<'a>(&'a mut self, proxy: StrongProxy<'a, Object>) {
        for key in proxy
            .get_bounding_aabb()
            .get_grid_overlaps()
            .map(|v| Self::hash(&v))
            .unique_index::<16>()
        {
            self.hash_table.insert(key, StrongProxy::downgrade(&proxy))
        }
    }

    pub fn from<'a>(proxies: impl IntoIterator<Item = StrongProxy<'a, Object>>) -> Self {
        let mut accel = Self::new();
        for proxy in proxies.into_iter() {
            accel.insert(proxy);
        }
        accel
    }

    pub fn get_intersections<'a>(
        &'a self,
        objects: &'a Arena<Object>,
    ) -> impl Iterator<Item = (WeakProxy, WeakProxy)> + 'a {
        (0..N)
            .map(|key| self.hash_table.iter(key))
            .map(|proxies| proxies.map(|proxy| objects.upgrade(proxy).unwrap()))
            .flat_map(brute_force::naive)
            .map(sort_pair)
    }

    pub fn update(&mut self, objects: &Arena<Object>) {
        for proxy in objects.iter() {
            let aabb = proxy.get_bounding_aabb();
            let prev_aabb = proxy.get_prev_bounding_aabb();

            let stale_keys = prev_aabb
                .get_grid_overlaps_between(&aabb)
                .map(|v| Self::hash(&v))
                .unique_index::<16>();
            let new_keys = aabb
                .get_grid_overlaps_between(&prev_aabb)
                .map(|v| Self::hash(&v))
                .unique_index::<16>();

            for key in stale_keys {
                self.hash_table.remove(key, &StrongProxy::downgrade(&proxy));
            }

            for key in new_keys {
                self.hash_table.insert(key, StrongProxy::downgrade(&proxy));
            }
        }
    }

    pub fn remove(&mut self, proxy: &WeakProxy, objects: &Arena<Object>) {
        let object = objects.upgrade(proxy).unwrap();
        let aabb = object.get_bounding_aabb();
        let prev_aabb = object.get_prev_bounding_aabb();

        let stale_keys = prev_aabb
            .get_grid_overlaps_between(&aabb)
            .map(|v| Self::hash(&v))
            .unique_index::<16>();
        let new_keys = aabb
            .get_grid_overlaps_between(&prev_aabb)
            .map(|v| Self::hash(&v))
            .unique_index::<16>();

        for key in stale_keys {
            self.hash_table.remove(key, proxy);
        }

        for key in new_keys {
            self.hash_table.remove(key, proxy);
        }
    }
}

impl<const N: usize> Default for PooledHashGrid<N> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use crab_common::objects::Object;
    use crab_common::random::gen_random_vector;
    use crab_common::{math::Vector, random::gen_random_object, shapes::AABB};
    use rand::prelude::*;
    use rand::rngs::SmallRng;

    use crate::brute_force::naive;

    use super::*;

    #[test]
    fn compare_naive() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(100., 100., 100.));
        let objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();
        let accel = NaiveHashGrid::from(objects.iter());

        let expected: HashSet<_> = naive(objects.iter()).collect();
        let collisions: HashSet<_> = accel.get_intersections(&objects).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }

    #[test]
    fn update_naive() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(50., 50., 50.));
        let mut objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();
        let mut accel = NaiveHashGrid::from(objects.iter());

        for object in objects.values_mut() {
            let bounds = AABB::new(Vector::new(-10., -10., -10.), Vector::new(10., 10., 10.));
            let translation = gen_random_vector(&mut rng, &bounds);
            object.prev_p = object.p;
            object.p = object.p + translation;
        }
        accel.update(&objects);

        let collisions: HashSet<_> = accel.get_intersections(&objects).collect();
        let expected: HashSet<_> = naive(objects.iter()).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }

    #[test]
    fn compare_static() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(100., 100., 100.));
        let objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();
        let accel = StaticHashGrid::<128>::from(objects.iter());

        let expected: HashSet<_> = naive(objects.iter()).collect();
        let collisions: HashSet<_> = accel.get_intersections(&objects).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }

    #[test]
    fn update_static() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(50., 50., 50.));
        let mut objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();
        let mut accel = StaticHashGrid::<128>::from(objects.iter());

        for object in objects.values_mut() {
            let bounds = AABB::new(Vector::new(-10., -10., -10.), Vector::new(10., 10., 10.));
            let translation = gen_random_vector(&mut rng, &bounds);
            object.prev_p = object.p;
            object.p = object.p + translation;
        }
        accel.update(&objects);

        let collisions: HashSet<_> = accel.get_intersections(&objects).collect();
        let expected: HashSet<_> = naive(objects.iter()).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }

    #[test]
    fn compare_pooled() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(100., 100., 100.));
        let objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();
        let accel = PooledHashGrid::<128>::from(objects.iter());

        let expected: HashSet<_> = naive(objects.iter()).collect();
        let collisions: HashSet<_> = accel.get_intersections(&objects).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }

    #[test]
    fn update_pooled() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(50., 50., 50.));
        let mut objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();
        let mut accel = PooledHashGrid::<128>::from(objects.iter());

        for object in objects.values_mut() {
            let bounds = AABB::new(Vector::new(-10., -10., -10.), Vector::new(10., 10., 10.));
            let translation = gen_random_vector(&mut rng, &bounds);
            object.prev_p = object.p;
            object.p = object.p + translation;
        }
        accel.update(&objects);

        let collisions: HashSet<_> = accel.get_intersections(&objects).collect();
        let expected: HashSet<_> = naive(objects.iter()).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }
}
