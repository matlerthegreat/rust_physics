use std::collections::HashSet;

use crab_common::{
    arena::{Arena, WeakProxy},
    math::Scalar,
    objects::Object,
    sort_pair,
};

#[derive(Debug, Clone, Copy)]
enum EndPoint {
    Start { point: Scalar, proxy: WeakProxy },
    Stop { point: Scalar, proxy: WeakProxy },
}

#[derive(Debug, Default)]
pub struct PersistentSAP {
    axes: [Vec<EndPoint>; 3],
    overlaps: HashSet<(WeakProxy, WeakProxy)>,
}

impl PersistentSAP {
    pub fn new() -> Self {
        PersistentSAP::default()
    }

    fn bouble_down(&mut self, axis: usize, i: usize, objects: &Arena<Object>) {
        if i < 1 {
            return;
        }

        match (self.axes[axis][i - 1], self.axes[axis][i]) {
            (
                EndPoint::Stop {
                    point: lhs,
                    proxy: a,
                },
                EndPoint::Start {
                    point: rhs,
                    proxy: b,
                },
            ) if lhs > rhs => {
                // TODO: Remove on unsuccesfull upgrade
                let at = objects.upgrade(&a).unwrap();
                let bt = objects.upgrade(&b).unwrap();
                if at.get_bounding_aabb().intersects(&bt.get_bounding_aabb()) {
                    self.overlaps.insert(sort_pair((a, b)));
                }
                self.axes[axis].swap(i - 1, i);
                self.bouble_down(axis, i - 1, objects);
            }
            (
                EndPoint::Start {
                    point: lhs,
                    proxy: a,
                },
                EndPoint::Stop {
                    point: rhs,
                    proxy: b,
                },
            ) if lhs > rhs => {
                // TODO: Remove on unsuccesfull upgrade
                let at = objects.upgrade(&a).unwrap();
                let bt = objects.upgrade(&b).unwrap();
                if !at.get_bounding_aabb().intersects(&bt.get_bounding_aabb()) {
                    self.overlaps.remove(&sort_pair((a, b)));
                }
                self.axes[axis].swap(i - 1, i);
                self.bouble_down(axis, i - 1, objects);
            }
            (
                EndPoint::Start { point: lhs, .. } | EndPoint::Stop { point: lhs, .. },
                EndPoint::Start { point: rhs, .. } | EndPoint::Stop { point: rhs, .. },
            ) if lhs > rhs => {
                self.axes[axis].swap(i - 1, i);
                self.bouble_down(axis, i - 1, objects);
            }
            _ => (),
        }
    }

    pub fn insert(&mut self, proxy: WeakProxy, objects: &Arena<Object>) {
        let aabb = objects.upgrade(&proxy).unwrap().get_bounding_aabb();

        for axis in 0..3 {
            let i = self.axes[axis].len();
            self.axes[axis].push(EndPoint::Start {
                point: aabb.minimum.get(axis),
                proxy,
            });
            self.bouble_down(axis, i, objects);
            self.axes[axis].push(EndPoint::Stop {
                point: aabb.maximum.get(axis),
                proxy,
            });
            self.bouble_down(axis, i + 1, objects);
        }
    }

    pub fn update(&mut self, objects: &Arena<Object>) {
        for axis in 0..3 {
            for i in 0..self.axes[axis].len() {
                match &mut self.axes[axis][i] {
                    EndPoint::Start { point, proxy } => {
                        *point = objects
                            .upgrade(proxy)
                            .unwrap()
                            .get_bounding_aabb()
                            .minimum
                            .get(axis)
                    }
                    EndPoint::Stop { point, proxy } => {
                        *point = objects
                            .upgrade(proxy)
                            .unwrap()
                            .get_bounding_aabb()
                            .maximum
                            .get(axis)
                    }
                }
                self.bouble_down(axis, i, objects);
            }
        }
    }

    pub fn get_intersections(&self) -> impl Iterator<Item = (WeakProxy, WeakProxy)> + '_ {
        self.overlaps.iter().cloned()
    }

    pub fn remove(&mut self, in_proxy: &WeakProxy) {
        for axis in 0..3 {
            let indexes = self.axes[axis].iter().enumerate().filter_map(|(i, endpoint)| {
                match endpoint {
                    EndPoint::Start { proxy, .. } if proxy == in_proxy => Some(i),
                    EndPoint::Stop { proxy, .. } if proxy == in_proxy => Some(i),
                    _  => None
                }
            }).collect::<Vec<_>>();

            for index in indexes.into_iter().rev() {
                self.axes[axis].remove(index);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::brute_force;
    use crab_common::arena::{Arena, StrongProxy};
    use crab_common::objects::Object;
    use crab_common::random::gen_random_vector;
    use crab_common::{math::Vector, random::gen_random_object, shapes::AABB};
    use rand::prelude::*;
    use rand::rngs::SmallRng;

    use super::*;

    #[test]
    fn compare_persistent() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(10., 10., 10.));
        let objects: Arena<Object> = (0..1000)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();

        let expected: HashSet<_> = brute_force::naive(objects.iter()).map(sort_pair).collect();

        let mut persistent = PersistentSAP::new();
        for proxy in objects.iter() {
            persistent.insert(StrongProxy::downgrade(&proxy), &objects);
        }

        let collisions: HashSet<_> = persistent.get_intersections().collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }

    #[test]
    fn update_persistent() {
        let mut rng = SmallRng::seed_from_u64(5);
        let bounds = AABB::new(Vector::new(0., 0., 0.), Vector::new(0., 0., 0.));
        let mut objects: Arena<Object> = (0..10)
            .map(|_| gen_random_object(&mut rng, &bounds))
            .collect();

        let mut accel = PersistentSAP::new();
        for proxy in objects.iter() {
            accel.insert(StrongProxy::downgrade(&proxy), &objects);
        }

        let collisions: HashSet<_> = accel.get_intersections().collect();

        let expected: HashSet<_> = brute_force::naive(objects.iter()).map(sort_pair).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);

        for object in objects.values_mut() {
            let bounds = AABB::new(Vector::new(-10., -10., -10.), Vector::new(10., 10., 10.));
            let translation = gen_random_vector(&mut rng, &bounds);
            object.prev_p = object.p;
            object.p = object.p + translation;
        }
        accel.update(&objects);

        let collisions: HashSet<_> = accel.get_intersections().collect();
        let expected: HashSet<_> = brute_force::naive(objects.iter()).map(sort_pair).collect();

        assert_eq!(expected.difference(&collisions).count(), 0);
    }
}
