use crab_common::{
    arena::{StrongProxy, WeakProxy},
    iterator::CombinationsExt,
    objects::Object,
};

pub fn naive<'a, T: 'a>(objects: T) -> impl Iterator<Item = (WeakProxy, WeakProxy)> + 'a
where
    T: IntoIterator<Item = StrongProxy<'a, Object>>,
    T::IntoIter: Clone,
{
    objects
        .into_iter()
        .combinations()
        .filter(|(x, y)| x.get_bounding_aabb().intersects(&y.get_bounding_aabb()))
        .map(|(x, y)| (StrongProxy::downgrade(&x), StrongProxy::downgrade(&y)))
}

/*fn chunked_single<'a, T>(xs: T) -> impl Iterator<Item = (ObjectProxy<'a>, ObjectProxy<'a>)>
where
    T: IntoIterator<Item = (ObjectProxy<'a>, &'a ShapeRef<'a>)>,
    T::IntoIter: Clone,
{
    get_pairs_within(xs)
    .filter_map(|((x_obj, x_shape), (y_obj, y_shape))| {
        if x_shape.intersects(y_shape) {
            Some((x_obj, y_obj))
        } else {
            None
        }
    })
}

fn chunked_between<'a, T>(xs: T, ys: T) -> impl Iterator<Item = (&'a Object, &'a Object)>
where
    T: IntoIterator<Item = (&'a Object, &'a ShapeRef<'a>)>,
    T::IntoIter: Clone,
{
    get_pairs_between(xs, ys)
    .filter_map(|((x_obj, x_shape), (y_obj, y_shape))| {
        if x_shape.intersects(y_shape) {
            Some((x_obj, y_obj))
        } else {
            None
        }
    })
}

pub fn brute_force_chunked<'a, T, U>(chunks: U) -> impl Iterator<Item = (&'a Object, &'a Object)>
where
    T: IntoIterator<Item = (&'a Object, &'a ShapeRef<'a>)>,
    T::IntoIter: Clone,
    U: IntoIterator<Item = T>,
    U::IntoIter: Clone,
{
    let chunks = chunks.into_iter();
    chunks.clone().enumerate()
    .flat_map(move |(n, x)| {
        let x = x.into_iter();
        chunked_single(x.clone())
        .chain(chunks.clone().skip(n+1).flat_map(move |y| chunked_between(x.clone(), y.into_iter())))
    })
}

pub fn accel_brute_force<'a, const N: usize, I>(objects: I, all_objects_count: usize) -> Vec<(Vec<&'a Object>, ShapePool<'a, N>)>
where
    I: IntoIterator<Item = &'a Object>,
    I::IntoIter: Clone,
{
    let objects = objects.into_iter();
    let mut processed_objects_count = 0;

    let mut result = Vec::new();

    while processed_objects_count < all_objects_count {
        let new_objects = objects.clone().skip(processed_objects_count);
        let pool = ShapePool::new(new_objects.clone().map(|object| object.bounding_shape.to_owned()));
        processed_objects_count += pool.len();

        let processed_objects = new_objects.take(pool.len()).collect();

        result.push((processed_objects, pool));
    }

    result
}*/

#[cfg(test)]
mod tests {
    use crab_common::{arena::Arena, math::Vector, objects::Object, shapes::Shape};

    use super::*;

    #[test]
    fn test_brute_force() {
        let mut objects: Arena<Object> = Default::default();
        let x = objects.insert(Object::new(
            Shape::sphere(3.),
            Vector {
                x: 0.,
                y: 0.,
                z: 0.,
            },
        ));
        let y = objects.insert(Object::new(
            Shape::sphere(9.),
            Vector {
                x: 0.,
                y: 0.,
                z: 4.,
            },
        ));
        objects.insert(Object::new(
            Shape::sphere(8.),
            Vector {
                x: 500.,
                y: 6.,
                z: 7.,
            },
        ));
        //let collisions = vec![(&x.into(), &y.into())];
        //let proxies = objects.iter().map(ObjectProxy::from);
        let expected = [(x, y)];

        let collisions: Vec<_> = naive(objects.iter()).collect();

        assert_eq!(collisions.len(), expected.len());

        for ((a1, b1), (a2, b2)) in collisions.into_iter().zip(expected) {
            assert_eq!(a1, a2);
            assert_eq!(b1, b2);
        }
    }

    /*#[test]
    fn test_chunked_single() {
        let (objects, expected) = gen_known_collisions();

        let mem_pool = ShapePool::<64>::new(objects.iter().map(|o| o.bounding_shape));

        let input = objects.iter().zip(mem_pool.get_shapes());

        let collisions: Vec<_> = chunked_single(input).map(|(x, y)| (x.to_owned(), y.to_owned())).collect();

        assert_eq!(collisions.len(), expected.len());

        for (x, y) in collisions.into_iter().zip(expected) {
            assert_eq!(x, y);
        }
    }

    #[test]
    fn test_chunked_between() {
        let x = Object{ id: 0, bounding_shape: Shape::Sphere(Sphere{ center: Vector{x: 0., y: 0., z: 0.}, radius: 3. }) };
        let y = Object{ id: 2, bounding_shape: Shape::Sphere(Sphere{ center: Vector{x: 0., y: 0., z: 4.}, radius: 9. }) };
        let expected = vec![(x.clone(), y.clone())];

        let objects_a = [x];
        let mem_pool_a = ShapePool::<64>::new(objects_a.iter().map(|o| o.bounding_shape));
        let input_a = objects_a.iter().zip(mem_pool_a.get_shapes());

        let objects_b = [y];
        let mem_pool_b = ShapePool::<64>::new(objects_b.iter().map(|o| o.bounding_shape));
        let input_b = objects_b.iter().zip(mem_pool_b.get_shapes());

        let collisions: Vec<_> = chunked_between(input_a, input_b).map(|(x, y)| (x.to_owned(), y.to_owned())).collect();

        assert_eq!(collisions.len(), expected.len());

        for (x, y) in collisions.into_iter().zip(expected) {
            assert_eq!(x, y);
        }
    }

    #[test]
    fn test_brute_force_chunked() {
        let (objects, expected) = gen_known_collisions();

        let mem_pool = ShapePool::<64>::new(objects.iter().map(|o| o.bounding_shape));
        let input = [objects.iter().zip(mem_pool.get_shapes())];

        let collisions: Vec<_> = brute_force_chunked(input).map(|(x, y)| (x.to_owned(), y.to_owned())).collect();

        assert_eq!(collisions.len(), expected.len());

        for (x, y) in collisions.into_iter().zip(expected) {
            assert_eq!(x, y);
        }
    }

    fn gen_random_objects(n: usize) -> Vec<Object> {
        let mut rng = SmallRng::seed_from_u64(5);
        let mut random_scalar = || {rng.gen_range(0..1000) as f32 / 10.0};

        let shapes = std::iter::repeat_with(|| Shape::Sphere(Sphere{ center: Vector{x: random_scalar(), y: random_scalar(), z: random_scalar()}, radius: random_scalar() }));
        shapes.enumerate().map(|(id, bounding_shape)| Object{id, bounding_shape}).take(n).collect()
    }

    #[test]
    fn test_brute_force_chunked_random() {
        let objects = gen_random_objects(1000);
        let mut expected: Vec<_> = naive(objects.iter()).map(|(x, y)| (x.to_owned(), y.to_owned())).collect();
        expected.sort();

        let accel = accel_brute_force::<64, _>(objects.iter(), objects.len());

        let input: Vec<_> = accel.iter().map(|(objects, shapes)| objects.into_iter().map(|o| o.to_owned()).zip(shapes.get_shapes())).collect();

        let mut collisions: Vec<_> = brute_force_chunked(input).map(|(x, y)| (x.to_owned(), y.to_owned())).collect();
        collisions.sort();

        assert_eq!(collisions.len(), expected.len());

        for (x, y) in collisions.into_iter().zip(expected) {
            assert_eq!(x, y);
        }
    }*/
}
