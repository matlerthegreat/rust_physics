use crab_broad_phase::brute_force::naive;
use crab_broad_phase::spatial_hash_grid::{NaiveHashGrid, StaticHashGrid, PooledHashGrid};
use crab_common::arena::Arena;
use crab_common::math::Vector;
use crab_common::objects::Object;
use crab_common::random::gen_random_object;
use crab_common::shapes::AABB;
use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId, Throughput};
use rand::SeedableRng;
use rand::rngs::SmallRng;


fn generate_objects() -> impl Iterator<Item = Object> {
    let world_size = AABB{minimum: Vector { x: -10., y: -10., z: -10. }, maximum: Vector { x: 10., y: 10., z: 10. }};
    let mut rng = SmallRng::seed_from_u64(65536);
    std::iter::repeat_with(move || gen_random_object(&mut rng, &world_size))
}

fn gen_random_world(size: usize) -> Arena<Object> {
    Arena::from(generate_objects().take(size))
}

pub fn comparison_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("BruteForce Comparison");
    let inputs = [500, 1000, 1500, 2000];
    for i in inputs.iter() {
        group.throughput(Throughput::Elements(*i as u64));
        group.bench_with_input(BenchmarkId::new("Naive", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                b.iter(|| naive(objects.iter()).collect::<Vec<_>>())
            });
        /*group.bench_with_input(BenchmarkId::new("Chunked 64", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = accel_brute_force::<64, _>(objects.iter(), objects.len());
                let dummy = accel.iter().map(|(objects, shapes)| objects.into_iter().map(|o| o.to_owned()).zip(shapes.get_shapes()));
                b.iter(|| brute_force_chunked(dummy.clone()).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("Chunked 256", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = accel_brute_force::<256, _>(objects.iter(), objects.len());
                let dummy = accel.iter().map(|(objects, shapes)| objects.into_iter().map(|o| o.to_owned()).zip(shapes.get_shapes()));
                b.iter(|| brute_force_chunked(dummy.clone()).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("Chunked 1024", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = accel_brute_force::<1024, _>(objects.iter(), objects.len());
                let dummy = accel.iter().map(|(objects, shapes)| objects.into_iter().map(|o| o.to_owned()).zip(shapes.get_shapes()));
                b.iter(|| brute_force_chunked(dummy.clone()).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("Chunked 4096", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = accel_brute_force::<4096, _>(objects.iter(), objects.len());
                let dummy = accel.iter().map(|(objects, shapes)| objects.into_iter().map(|o| o.to_owned()).zip(shapes.get_shapes()));
                b.iter(|| brute_force_chunked(dummy.clone()).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("Chunked 16384", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = accel_brute_force::<16384, _>(objects.iter(), objects.len());
                let dummy = accel.iter().map(|(objects, shapes)| objects.into_iter().map(|o| o.to_owned()).zip(shapes.get_shapes()));
                b.iter(|| brute_force_chunked(dummy.clone()).collect::<Vec<_>>())
            });*/
        group.bench_with_input(BenchmarkId::new("HashGrid Naive", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = NaiveHashGrid::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
            });

        group.bench_with_input(BenchmarkId::new("HashGrid Static 128", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = StaticHashGrid::<128>::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("HashGrid Static 512", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = StaticHashGrid::<512>::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("HashGrid Static 1024", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = StaticHashGrid::<1024>::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
                });

        group.bench_with_input(BenchmarkId::new("HashGrid Pooled 128", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = PooledHashGrid::<128>::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("HashGrid Pooled 512", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = PooledHashGrid::<512>::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
            });
        group.bench_with_input(BenchmarkId::new("HashGrid Pooled 1024", i), i, 
            |b, i| {
                let objects = gen_random_world(*i);
                let accel = PooledHashGrid::<1024>::from(objects.iter());
                b.iter(|| accel.get_intersections(&objects).collect::<Vec<_>>())
                });
        }
    group.finish();
}

criterion_group!(benches, comparison_benchmark);
criterion_main!(benches);