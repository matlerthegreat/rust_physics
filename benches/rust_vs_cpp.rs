use crab;
use crab_cancer as cancer;
use crab_common::{
    math::Vector,
    objects::Object,
    random::{gen_random_object, gen_random_vector},
    shapes::AABB,
};
use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use rand::prelude::*;
use std::iter;

fn generate_objects() -> impl Iterator<Item = Object> {
    let bounds = AABB::new(Vector::lift(-10.0), Vector::lift(10.0));
    let mut rng = SmallRng::seed_from_u64(65536);
    let mut rng2 = SmallRng::seed_from_u64(65536);
    iter::repeat_with(move || gen_random_object(&mut rng, &bounds)).map(move |object| Object {
        shape: object.shape,
        p: object.p,
        prev_p: object.p
            + gen_random_vector(&mut rng2, &AABB::new(Vector::lift(-0.1), Vector::lift(0.1))),
    })
}

fn populate_crab_engine(engine: &mut crab::Crab, size: u64) {
    for object in generate_objects().take(size as usize) {
        engine.insert(object);
    }
}

fn populate_cancer_engine(engine: &mut cancer::Cancer, size: u64) {
    for object in generate_objects().take(size as usize) {
        engine.insert(object);
    }
}

const SIZES: [u64; 5] = [100, 250, 500, 750, 1000];

pub fn populate_engine(c: &mut Criterion) {
    let mut group = c.benchmark_group("populate PooledHashGrid");
    for size in SIZES.iter() {
        group.bench_with_input(BenchmarkId::new("crab", size), size, |b, size| {
            b.iter(|| {
                let mut engine = crab::Crab::new_pooled();
                populate_crab_engine(&mut engine, *size)
            })
        });

        group.bench_with_input(BenchmarkId::new("cancer", size), size, |b, size| {
            b.iter(|| {
                let mut engine = cancer::Cancer::hash_grid();
                populate_cancer_engine(&mut engine, *size)
            })
        });
    }
    group.finish();

    let mut group = c.benchmark_group("populate SAP");
    for size in SIZES.iter() {
        group.bench_with_input(BenchmarkId::new("crab", size), size, |b, size| {
            b.iter(|| {
                let mut engine = crab::Crab::new_sap();
                populate_crab_engine(&mut engine, *size)
            })
        });

        group.bench_with_input(BenchmarkId::new("cancer", size), size, |b, size| {
            b.iter(|| {
                let mut engine = cancer::Cancer::sap();
                populate_cancer_engine(&mut engine, *size)
            })
        });
    }
    group.finish();
}

pub fn simulate_engine(c: &mut Criterion) {
    let time = 0.1666;

    let mut group = c.benchmark_group("simulate Naive");
    group
        .sample_size(100)
        .sampling_mode(criterion::SamplingMode::Flat);
    for size in SIZES.iter() {
        group.throughput(Throughput::Elements(*size));

        group.bench_with_input(BenchmarkId::new("crab", size), size, |b, size| {
            let mut engine = crab::Crab::new_naive();
            populate_crab_engine(&mut engine, *size);
            b.iter(|| engine.update(time))
        });

        group.bench_with_input(BenchmarkId::new("cancer", size), size, |b, size| {
            let mut engine = cancer::Cancer::naive();
            populate_cancer_engine(&mut engine, *size);
            b.iter(|| engine.update(time))
        });
    }
    group.finish();

    let mut group = c.benchmark_group("simulate PooledHashGrid");
    group
        .sample_size(100)
        .sampling_mode(criterion::SamplingMode::Flat);
    for size in SIZES.iter() {
        group.throughput(Throughput::Elements(*size));

        group.bench_with_input(BenchmarkId::new("crab", size), size, |b, size| {
            let mut engine = crab::Crab::new_pooled();
            populate_crab_engine(&mut engine, *size);
            b.iter(|| engine.update(time))
        });

        group.bench_with_input(BenchmarkId::new("cancer", size), size, |b, size| {
            let mut engine = cancer::Cancer::hash_grid();
            populate_cancer_engine(&mut engine, *size);
            b.iter(|| engine.update(time))
        });
    }
    group.finish();

    let mut group = c.benchmark_group("simulate SAP");
    group
        .sample_size(100)
        .sampling_mode(criterion::SamplingMode::Flat);
    for size in SIZES.iter() {
        group.throughput(Throughput::Elements(*size));

        group.bench_with_input(BenchmarkId::new("crab", size), size, |b, size| {
            let mut engine = crab::Crab::new_sap();
            populate_crab_engine(&mut engine, *size);
            b.iter(|| engine.update(time))
        });

        group.bench_with_input(BenchmarkId::new("cancer", size), size, |b, size| {
            let mut engine = cancer::Cancer::sap();
            populate_cancer_engine(&mut engine, *size);
            b.iter(|| engine.update(time))
        });
    }
    group.finish();
}

criterion_group!(benches, populate_engine, simulate_engine);
criterion_main!(benches);
