
use std::iter;
use crab_common::{math::Vector, shapes::AABB, random::{gen_random_object, gen_random_vector}, objects::Object};
use crab;
use crab_cancer as cancer;
use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId, Throughput};
use rand::prelude::*;

fn generate_objects() -> impl Iterator<Item = Object> {
    let bounds = AABB::new(Vector::lift(-10.0), Vector::lift(10.0));
    let mut rng = SmallRng::seed_from_u64(65536);
    let mut rng2 = SmallRng::seed_from_u64(65536);
    iter::repeat_with(move || gen_random_object(&mut rng, &bounds))
    .map(move |object| Object { shape: object.shape, p: object.p, prev_p: object.p + gen_random_vector(&mut rng2, &AABB::new(Vector::lift(-0.1), Vector::lift(0.1))) })
}

fn populate_crab_engine(engine: &mut crab::Crab, size: u64) {
    for object in generate_objects().take(size as usize) {
        engine.insert(object);
    }
}

fn populate_cancer_engine(engine: &mut cancer::Cancer, size: u64) {
    for object in generate_objects().take(size as usize) {
        engine.insert(object);
    }
}

const SIZES: [u64; 5] = [100, 250, 500, 750, 1000];

pub fn populate_engine(c: &mut Criterion) {

    let mut group = c.benchmark_group("populate_engine");

    for size in SIZES.iter() {

        group.throughput(Throughput::Elements(*size));

        group.bench_with_input(BenchmarkId::new("crab naive", size), size, 
            |b, size| {
                b.iter(|| {
                    let mut engine = crab::Crab::new_naive();
                    populate_crab_engine(&mut engine, *size)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab simple", size), size, 
            |b, size| {
                b.iter(|| {
                    let mut engine = crab::Crab::new_simple();
                    populate_crab_engine(&mut engine, *size)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab static", size), size, 
            |b, size| {
                b.iter(|| {
                    let mut engine = crab::Crab::new_static();
                    populate_crab_engine(&mut engine, *size)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab pooled", size), size, 
            |b, size| {
                b.iter(|| {
                    let mut engine = crab::Crab::new_pooled();
                    populate_crab_engine(&mut engine, *size)
                })
            });

        group.bench_with_input(BenchmarkId::new("cancer", size), size, 
            |b, size| {
                b.iter(|| {
                    let mut engine = cancer::Cancer::new();
                    populate_cancer_engine(&mut engine, *size)
                    
                })
            });
    }
    group.finish();
}

pub fn simulate_engine(c: &mut Criterion) {
    let mut group = c.benchmark_group("simulate_engine");
    group.sample_size(100).sampling_mode(criterion::SamplingMode::Flat);

    let time = 0.1666;

    for size in SIZES.iter() {
        group.throughput(Throughput::Elements(*size));

        group.bench_with_input(BenchmarkId::new("crab naive", size), size, 
            |b, size| {
                let mut engine = crab::Crab::new_naive();
                populate_crab_engine(&mut engine, *size);
                b.iter(|| {
                    engine.update(time)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab simple", size), size, 
            |b, size| {
                let mut engine = crab::Crab::new_simple();
                populate_crab_engine(&mut engine, *size);
                b.iter(|| {
                    engine.update(time)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab static", size), size, 
            |b, size| {
                let mut engine = crab::Crab::new_static();
                populate_crab_engine(&mut engine, *size);
                b.iter(|| {
                    engine.update(time)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab pooled", size), size, 
            |b, size| {
                let mut engine = crab::Crab::new_pooled();
                populate_crab_engine(&mut engine, *size);
                b.iter(|| {
                    engine.update(time)
                })
            });

        group.bench_with_input(BenchmarkId::new("crab sap", size), size, 
            |b, size| {
                let mut engine = crab::Crab::new_sap();
                populate_crab_engine(&mut engine, *size);
                engine.update(time);
                b.iter(|| {
                    engine.update(time)
                })
            });

        group.bench_with_input(BenchmarkId::new("cancer spatial_hash_grid", size), size, 
            |b, size| {
                let mut engine = cancer::Cancer::new();
                populate_cancer_engine(&mut engine, *size);
                b.iter(|| {
                    engine.update(time)
                })
            });
    }
    group.finish();
}

criterion_group!(benches, populate_engine, simulate_engine);
criterion_main!(benches);