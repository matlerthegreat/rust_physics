use crate::math::{IVector, Scalar, Vector};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Sphere {
    pub radius: Scalar,
}

impl Sphere {
    pub fn new(radius: Scalar) -> Sphere {
        Sphere { radius }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Plane {
    pub distance: Scalar,
}

impl Plane {
    pub fn new(distance: Scalar) -> Self {
        Self { distance }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Box {
    pub extents: Vector,
}

impl Box {
    pub fn new(extents: Vector) -> Self {
        Box { extents }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct AABB {
    pub minimum: Vector,
    pub maximum: Vector,
}

impl AABB {
    pub fn new(minimum: Vector, maximum: Vector) -> AABB {
        AABB { minimum, maximum }
    }
    // Get all unit sized grid cells (cubes?) that this shape overlapps with
    pub fn get_grid_overlaps(&self) -> impl Iterator<Item = IVector> {
        let min = self.minimum.floor();
        let max = self.maximum.ceil();

        (min.x..max.x).flat_map(move |x| {
            (min.y..max.y).flat_map(move |y| (min.z..max.z).map(move |z| IVector::new(x, y, z)))
        })
    }

    pub fn get_grid_overlaps_between(&self, other: &AABB) -> impl Iterator<Item = IVector> {
        let min = other.minimum.floor();
        let max = other.maximum.ceil();

        self.get_grid_overlaps()
            .filter(move |&IVector { x, y, z }| {
                min.x > x || max.x <= x || min.y > y || max.y <= y || min.z > z || max.z <= z
            })
    }

    pub fn intersects(&self, b: &AABB) -> bool {
        (self.minimum.x <= b.maximum.x && self.maximum.x >= b.minimum.x)
            && (self.minimum.y <= b.maximum.y && self.maximum.y >= b.minimum.y)
            && (self.minimum.z <= b.maximum.z && self.maximum.z >= b.minimum.z)
    }

    pub fn translate(&mut self, translation: Vector) {
        self.minimum += translation;
        self.maximum += translation;
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Shape {
    Sphere(Sphere),
    Box(Box),
    Plane(Plane),
}

impl Shape {
    pub fn sphere(radius: Scalar) -> Shape {
        Shape::Sphere(Sphere::new(radius))
    }
    pub fn new_box(extents: Vector) -> Shape {
        Shape::Box(Box::new(extents))
    }
    pub fn new_plane(distance: Scalar) -> Shape {
        Shape::Plane(Plane::new(distance))
    }
}
