use crate::{
    math::{Scalar, Vector},
    objects::Object,
    shapes::{Box, Shape, Sphere, AABB},
};

pub fn gen_random_scalar(rng: &mut impl rand::Rng, min: Scalar, max: Scalar) -> Scalar {
    min + rng.gen::<Scalar>() * (max - min)
}

pub fn gen_random_vector(rng: &mut impl rand::Rng, bounds: &AABB) -> Vector {
    let x = gen_random_scalar(rng, bounds.minimum.x, bounds.maximum.x);
    let y = gen_random_scalar(rng, bounds.minimum.y, bounds.maximum.y);
    let z = gen_random_scalar(rng, bounds.minimum.z, bounds.maximum.z);
    Vector::new(x, y, z)
}

pub fn gen_random_sphere(rng: &mut impl rand::Rng, min: Scalar, max: Scalar) -> Sphere {
    let radius = gen_random_scalar(rng, min, max);
    Sphere::new(radius)
}

pub fn gen_random_box(rng: &mut impl rand::Rng, bounds: &AABB) -> Box {
    let extents = gen_random_vector(rng, bounds);
    Box::new(extents)
}

pub fn gen_random_shape(rng: &mut impl rand::Rng, min: Scalar, max: Scalar) -> Shape {
    if rng.gen_bool(0.5) {
        Shape::Sphere(gen_random_sphere(rng, min, max))
    } else {
        Shape::Box(gen_random_box(
            rng,
            &AABB::new(Vector::lift(min), Vector::lift(max)),
        ))
    }
}

pub fn gen_random_object(rng: &mut impl rand::Rng, bounds: &AABB) -> Object {
    let shape = gen_random_shape(rng, 0.1, 0.5);
    let p = gen_random_vector(rng, bounds);
    Object::new(shape, p)
}

pub fn gen_random_aabb(rng: &mut impl rand::Rng, bounds: &AABB) -> AABB {
    let minimum = gen_random_vector(rng, bounds);
    let dimension = gen_random_vector(
        rng,
        &AABB::new(Vector::new(0., 0., 0.), Vector::new(1., 1., 1.)),
    );
    let maximum = minimum + dimension;
    AABB::new(minimum, maximum)
}

/*pub fn gen_random_objects(rng: &mut impl rand::Rng, bounds: &AABB) -> impl Iterator<Item = Object> {
    (0..).map(|id| gen_random_object(rng, bounds, id))
}*/
