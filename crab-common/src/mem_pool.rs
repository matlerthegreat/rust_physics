use std::slice;

use crate::shapes::{Sphere, AABB, Shape};

type Scalar = f32;



impl From<[Scalar; 4]> for Sphere {
    fn from(value: [Scalar; 4]) -> Self {
        unsafe {
            transmute(value)
        }
    }
}

impl<'a> From<&'a mut [Scalar; 4]> for &'a mut Sphere {
    fn from(value: &'a mut [Scalar; 4]) -> Self {
        unsafe {
            transmute(value)
        }
    }
}

impl From<[Scalar; 6]> for AABB {
    fn from(value: [Scalar; 6]) -> Self {
        unsafe {
            transmute(value)
        }
    }
}

impl<'a> From<&'a mut [Scalar; 6]> for &'a mut AABB {
    fn from(value: &'a mut [Scalar; 6]) -> Self {
        unsafe {
            transmute(value)
        }
    }
}

fn split_slice_mut<const N: usize>(input: &mut [Scalar]) -> Option<(&mut [Scalar; N], &mut [Scalar])> {
    if N > input.len() {
        None
    } else {
        Some(input.split_array_mut())
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ShapeRef<'a> {
    Sphere(&'a Sphere),
    AABB(&'a AABB),
}

impl<'a> ShapeRef<'a> {
    fn sphere(data: &'a mut [Scalar; 4], sphere: Sphere) -> ShapeRef<'a> {
        let result: &mut Sphere = data.into();
        *result = sphere;
        ShapeRef::Sphere(result)
    }
    fn aabb(data: &'a mut [Scalar; 6], aabb: AABB) -> ShapeRef<'a> {
        let result: &mut AABB = data.into();
        *result = aabb;
        ShapeRef::AABB(result)
    }
    pub fn intersects(&self, other: &Self) -> bool {
        match (self, other) {
            (ShapeRef::Sphere(sphere_a), ShapeRef::Sphere(sphere_b)) => sphere_a.intersects_sphere(sphere_b),
            (ShapeRef::AABB(aabb), ShapeRef::Sphere(sphere)) |
            (ShapeRef::Sphere(sphere), ShapeRef::AABB(aabb)) => aabb.intersects_sphere(sphere),
            (ShapeRef::AABB(a), ShapeRef::AABB(b)) => a.intersects_aabb(b)
        }
    }
}

pub struct ShapePool<'a, const N: usize> {
    raw_ptr: *mut Scalar,
    shapes: Vec<ShapeRef<'a>>,
}

impl<'a, const N: usize> ShapePool<'a, N> {
    pub fn new(descs: impl IntoIterator<Item = Shape>) -> ShapePool<'a, N> {
        let mut buf = vec![Scalar::default(); N].into_boxed_slice();
        let raw_ptr = buf.as_mut_ptr();

        let mut pool = Box::leak(buf);

        let mut shapes = Vec::new();


        for desc in descs {
            let t = match desc {
                Shape::Sphere(s) =>
                    split_slice_mut(pool).map(|(data, rest)| (ShapeRef::sphere(data, s), rest)),
                Shape::AABB(s) =>
                    split_slice_mut(pool).map(|(data, rest)| (ShapeRef::aabb(data, s), rest)),
            };
            if let Some((shape, rest)) = t {
                shapes.push(shape);
                pool = rest;
            } else {
                break;
            }
        }

        ShapePool { raw_ptr, shapes }
    }

    pub fn get_shapes(&self) -> &[ShapeRef<'a>] {
        self.shapes.as_slice()
    }

    pub fn len(&self) -> usize {
        self.get_shapes().len()
    }

    pub fn is_empty(&self) -> bool {
        self.get_shapes().is_empty()
    }
}

impl<'a, const N: usize> Drop for ShapePool<'a, N> {
    fn drop(&mut self) {
        self.shapes.clear();
        unsafe {
            let raw_slice = slice::from_raw_parts_mut(self.raw_ptr, N);
            drop(Box::from_raw(raw_slice));
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{math::Vector, shapes::Shape};

    use super::*;

    #[test]
    fn convert_sphere_from_raw() {
        let raw = [1., 2., 3., 4.];
        let sphere: Sphere = raw.into();

        assert_eq!(sphere, Sphere{center: Vector::new(1., 2., 3.), radius: 4.})
    }

    #[test]
    fn convert_aabb_from_raw() {
        let raw = [1., 2., 3., 4., 5., 6.];
        let aabb: AABB = raw.into();

        assert_eq!(aabb, AABB{minimum: Vector::new(1., 2., 3.), maximum: Vector::new(4., 5., 6.)})
    }

    #[test]
    fn convert_sphere_from_raw_ref() {
        let raw = [1., 2., 3., 4.];
        let sphere: &Sphere = &raw.into();

        assert_eq!(sphere, &Sphere{center: Vector::new(1., 2., 3.), radius: 4.})
    }

    #[test]
    fn convert_aabb_from_raw_ref() {
        let raw = [1., 2., 3., 4., 5., 6.];
        let aabb: &AABB = &raw.into();

        assert_eq!(aabb, &AABB{minimum: Vector::new(1., 2., 3.), maximum: Vector::new(4., 5., 6.)})
    }

    #[test]
    fn fill_pool() {
        let shapes = [
            Shape::new_sphere(Vector::new(1., 2., 3.), 4.),
            Shape::new_sphere(Vector::new(5., 6., 7.), 8.),
            Shape::new_sphere(Vector::new(9., 0., 0.), 9.)];

        let shapes_pool = ShapePool::<32>::new(shapes);
        let pooled_shapes = shapes_pool.get_shapes();

        let buffer: [[f32; 4]; 3] = [[1., 2., 3., 4.], [5., 6., 7., 8.], [9., 0., 0., 9.]];

        unsafe {
            assert_eq!(buffer.flatten(), slice::from_raw_parts(shapes_pool.raw_ptr, buffer.flatten().len()))
        }

        pooled_shapes.iter().zip(shapes).for_each(|(pooled_shape, shape)| {
            match (pooled_shape, shape) {
                (ShapeRef::Sphere(s1), Shape::Sphere(s2)) => assert_eq!(**s1, s2),
                (ShapeRef::AABB(s1), Shape::AABB(s2)) => assert_eq!(**s1, s2),
                _ => assert!(false),
            }
        });
    }

    #[test]
    fn overfill_pool() {
        let shapes = [Shape::Sphere(Sphere{ center: Vector{x: 1., y: 2., z: 3.}, radius: 4. }),
        Shape::Sphere(Sphere{ center: Vector{x: 5., y: 6., z: 7.}, radius: 8. }),
        Shape::Sphere(Sphere{ center: Vector{x: 9., y: 0., z: 0.}, radius: 9. })];

        let shapes_pool = ShapePool::<10>::new(shapes);
        let pooled_shapes = shapes_pool.get_shapes();

        let buffer: [[f32; 4]; 2] = [[1., 2., 3., 4.], [5., 6., 7., 8.]];

        unsafe {
            assert_eq!(&buffer.flatten()[0..8], slice::from_raw_parts(shapes_pool.raw_ptr, 8))
        }

        pooled_shapes.iter().zip(shapes).for_each(|(pooled_shape, shape)| {
            match (pooled_shape, shape) {
                (ShapeRef::Sphere(s1), Shape::Sphere(s2)) => assert_eq!(**s1, s2),
                (ShapeRef::AABB(s1), Shape::AABB(s2)) => assert_eq!(**s1, s2),
                _ => assert!(false),
            }
        });
    }
}
