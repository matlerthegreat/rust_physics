type Index = u32;

#[derive(Debug)]
struct Node<T> {
    value: T,
    next: Option<Index>,
}

#[derive(Debug)]
pub struct MultiheadLinkedList<T: PartialEq, const N: usize> {
    heads: [Option<Index>; N],
    mem_pool: Vec<Node<T>>,
    free: Option<Index>,
}

impl<T: PartialEq, const N: usize> MultiheadLinkedList<T, N> {
    pub fn new() -> Self {
        MultiheadLinkedList {
            heads: [None; N],
            mem_pool: Vec::new(),
            free: None,
        }
    }

    pub fn insert(&mut self, head: usize, value: T) {
        self.heads[head] = Some(self.insert_node(value, self.heads[head]))
    }

    pub fn iter(&self, head: usize) -> impl Iterator<Item = &T> + Clone {
        let get_node = |index: Index| &self.mem_pool[index as usize];
        let first = self.heads[head].map(get_node);

        std::iter::successors(first, move |Node { next, .. }| next.map(get_node))
            .map(|Node { value, .. }| value)
    }

    pub fn remove(&mut self, head: usize, value: &T) {
        if let Some(i) = self.heads[head] {
            if self.mem_pool[i as usize].value == *value {
                self.heads[head] = self.mem_pool[i as usize].next;
                self.mem_pool[i as usize].next = self.free;
                self.free = Some(i);
                return;
            }
        }

        let first = self.heads[head].and_then(|i| self.mem_pool[i as usize].next.map(|j| (i, j)));

        let node = std::iter::successors(first, |&(_, i)| {
            self.mem_pool[i as usize].next.map(|j| (i, j))
        })
        .find(|&(_, j)| self.mem_pool[j as usize].value == *value);

        if let Some((i, j)) = node {
            self.mem_pool[i as usize].next = self.mem_pool[j as usize].next;
            self.mem_pool[j as usize].next = self.free;
            self.free = Some(j);
        }
    }

    fn insert_node(&mut self, value: T, tail: Option<Index>) -> Index {
        if let Some(index) = self.pop_free_node() {
            self.mem_pool[index as usize] = Node { value, next: tail };
            index
        } else {
            self.push_node(value, tail)
        }
    }

    fn push_node(&mut self, value: T, tail: Option<Index>) -> Index {
        self.mem_pool.push(Node { value, next: tail });
        (self.mem_pool.len() - 1).try_into().unwrap()
    }

    fn pop_free_node(&mut self) -> Option<Index> {
        if let Some(index) = self.free {
            self.free = self.mem_pool[index as usize].next;
            Some(index)
        } else {
            None
        }
    }
}

impl<T: PartialEq, const N: usize> Default for MultiheadLinkedList<T, N> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert() {
        let mut lists: MultiheadLinkedList<_, 10> = MultiheadLinkedList::new();

        lists.insert(2, "a");

        let values: Vec<_> = lists.iter(2).cloned().collect();
        assert_eq!(values, vec!["a"]);
    }

    #[test]
    fn insert_many() {
        let mut lists: MultiheadLinkedList<_, 10> = MultiheadLinkedList::new();

        for v in ["a", "b", "c", "d"] {
            lists.insert(2, v);
        }

        let values: Vec<_> = lists.iter(2).cloned().collect();
        assert_eq!(values, vec!["d", "c", "b", "a"]);
    }

    #[test]
    fn remove() {
        let mut lists: MultiheadLinkedList<_, 10> = MultiheadLinkedList::new();

        for v in ["a", "b", "c", "d"] {
            lists.insert(2, v);
        }

        lists.remove(2, &"b");

        let values: Vec<_> = lists.iter(2).cloned().collect();
        assert_eq!(values, vec!["d", "c", "a"]);
    }

    #[test]
    fn remove_nonexistent() {
        let mut lists: MultiheadLinkedList<_, 10> = MultiheadLinkedList::new();

        for v in ["a", "b", "c", "d"] {
            lists.insert(2, v);
        }

        lists.remove(2, &"z");

        let values: Vec<_> = lists.iter(2).cloned().collect();
        assert_eq!(values, vec!["d", "c", "b", "a"]);
    }

    #[test]
    fn remove_many() {
        let mut lists: MultiheadLinkedList<_, 10> = MultiheadLinkedList::new();

        for v in ["a", "b", "c", "d"] {
            lists.insert(2, v);
        }

        lists.remove(2, &"b");
        lists.remove(2, &"c");

        let values: Vec<_> = lists.iter(2).cloned().collect();
        assert_eq!(values, vec!["d", "a"]);
    }
}
