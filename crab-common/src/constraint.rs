use crate::{
    arena::{Arena, WeakProxy},
    math::Vector,
    objects::Object,
};

#[derive(Debug)]
pub struct Constraint {
    a: WeakProxy,
    b: WeakProxy,
    l: f32,
}

impl Constraint {
    pub fn new(a: WeakProxy, b: WeakProxy, l: f32) -> Constraint {
        Constraint { a, b, l }
    }

    pub fn project(&self, objects: &mut Arena<Object>) {
        let proxies = [self.a, self.b];
        let [mut a, mut b] = objects.upgrade_many_mut(&proxies);

        let d = Vector::distance(a.p, b.p) - self.l;
        if d > 0.0 {
            let diff = (a.p - b.p).normalize() * (d / 2.0);
            a.p -= diff;
            b.p += diff;
        }
    }
}
