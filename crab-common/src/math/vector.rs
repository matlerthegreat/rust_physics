use std::{ops::{Add, AddAssign, Div, Mul, Neg, Sub, SubAssign}, iter::Sum};

use super::{IVector, Integer, Scalar};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vector {
    pub x: Scalar,
    pub y: Scalar,
    pub z: Scalar,
}
impl Vector {
    pub fn new(x: Scalar, y: Scalar, z: Scalar) -> Self {
        Self { x, y, z }
    }
    pub fn lift(x: Scalar) -> Self {
        Self::new(x, x, x)
    }
    pub fn combine(self, other: Self, f: fn(Scalar, Scalar) -> Scalar) -> Self {
        Self::new(f(self.x, other.x), f(self.y, other.y), f(self.z, other.z))
    }
    pub fn reduce(self, f: impl Fn(Scalar, Scalar) -> Scalar) -> Scalar {
        f(f(self.x, self.y), self.z)
    }
    pub fn map(self, f: impl Fn(Scalar) -> Scalar) -> Self {
        Vector::new(f(self.x), f(self.y), f(self.z))
    }
    pub fn get(&self, axis: usize) -> Scalar {
        match axis {
            0 => self.x,
            1 => self.y,
            2 => self.z,
            _ => panic!(),
        }
    }
    pub fn into_parts(self) -> (Scalar, Vector) {
        let length = self.length();
        (length, self / length)
    }
    pub fn combine_mut(&mut self, other: Self, f: fn(&mut Scalar, Scalar)) {
        f(&mut self.x, other.x);
        f(&mut self.y, other.y);
        f(&mut self.z, other.z);
    }

    pub fn distance(a: Vector, b: Vector) -> Scalar {
        (a - b).length()
    }
    pub fn distance_squared(a: Vector, b: Vector) -> Scalar {
        (a - b).length_squared()
    }
    pub fn dot(self, other: Vector) -> Scalar {
        self.combine(other, Mul::mul).reduce(Add::add)
    }
    pub fn length_squared(self) -> Scalar {
        self.dot(self)
    }
    pub fn length(&self) -> Scalar {
        self.length_squared().sqrt()
    }
    pub fn floor(self) -> IVector {
        let v = self.map(|x| x.floor());
        IVector {
            x: v.x as Integer,
            y: v.y as Integer,
            z: v.z as Integer,
        }
    }
    pub fn ceil(self) -> IVector {
        let v = self.map(|x| x.ceil());
        IVector {
            x: v.x as Integer,
            y: v.y as Integer,
            z: v.z as Integer,
        }
    }
    pub fn normalize(self) -> Vector {
        self * (1.0 / self.length())
    }
    pub fn normalize_mut(&mut self) -> Scalar {
        let length = self.length();
        self.x /= length;
        self.y /= length; 
        self.z /= length; 
        length
    }
    pub fn cross(self, other: Vector) -> Vector {
        let x = self.y * other.z - self.z * other.y;
        let y = self.z * other.x - self.x * other.z;
        let z = self.x * other.y - self.y * other.x;

        Vector { x, y, z }
    }
    pub const OX: Vector = Vector {
        x: 1.0,
        y: 0.0,
        z: 0.0,
    };
    pub const OY: Vector = Vector {
        x: 0.0,
        y: 1.0,
        z: 0.0,
    };
    pub const OZ: Vector = Vector {
        x: 0.0,
        y: 0.0,
        z: 1.0,
    };
}

impl Sum for Vector {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.reduce(|a, b| a + b).unwrap_or(Vector::lift(0.0))
    }
}

impl Sub for Vector {
    type Output = Vector;

    fn sub(self, other: Self) -> Self::Output {
        self.combine(other, Sub::sub)
    }
}

impl SubAssign for Vector {
    fn sub_assign(&mut self, other: Self) {
        self.combine_mut(other, SubAssign::sub_assign)
    }
}

impl Add for Vector {
    type Output = Vector;

    fn add(self, other: Self) -> Self::Output {
        self.combine(other, Add::add)
    }
}

impl AddAssign for Vector {
    fn add_assign(&mut self, other: Self) {
        self.combine_mut(other, AddAssign::add_assign)
    }
}

impl Mul<Scalar> for Vector {
    type Output = Vector;

    fn mul(self, s: Scalar) -> Self::Output {
        self.combine(Vector::lift(s), Mul::mul)
    }
}

impl Div<Scalar> for Vector {
    type Output = Vector;

    fn div(self, s: Scalar) -> Self::Output {
        self.combine(Vector::lift(s), Div::div)
    }
}

impl Mul for Vector {
    type Output = Vector;

    fn mul(self, other: Self) -> Self::Output {
        self.combine(other, Mul::mul)
    }
}

impl Neg for Vector {
    type Output = Vector;

    fn neg(self) -> Self::Output {
        self.map(|x| -x)
    }
}

impl Mul<Vector> for Scalar {
    type Output = Vector;

    fn mul(self, v: Vector) -> Vector {
        v.combine(Vector::lift(self), Mul::mul)
    }
}

impl From<Vector> for [f32; 3] {
    fn from(v: Vector) -> Self {
        [v.x, v.y, v.z]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cross() {
        let v = Vector::OX;
        let u = Vector::OY;

        let expected = Vector::OZ;

        assert_eq!(v.cross(u), expected);
    }
}