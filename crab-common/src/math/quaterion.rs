use std::ops::{Div, Mul, MulAssign};

use super::{vector::Vector, Scalar};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Quaterion {
    pub(super) a: Scalar,
    pub(super) b: Scalar,
    pub(super) c: Scalar,
    pub(super) d: Scalar,
}

impl Quaterion {
    pub fn new(a: Scalar, b: Scalar, c: Scalar, d: Scalar) -> Self {
        Self { a, b, c, d }
    }

    pub fn identity() -> Self {
        Self { a: 1.0, b: 0.0, c: 0.0, d: 0.0 }
    }

    pub fn into_parts(self) -> (Scalar, Vector) {
        (self.a, Vector::new(self.b, self.c, self.d))
    }

    pub fn from_parts(r: Scalar, v: Vector) -> Self {
        Self {
            a: r,
            b: v.x,
            c: v.y,
            d: v.z,
        }
    }

    pub fn from_rotation(angle: Scalar, axis: Vector) -> Self {
        let (s, r) = (angle / 2.0).sin_cos();
        let v = s * axis.normalize();

        Self::from_parts(r, v)
    }

    pub fn conjugate(self) -> Self {
        let (s, v) = self.into_parts();
        Self::from_parts(s, -v)
    }

    pub fn norm_squared(self) -> Scalar {
        self.a.powi(2) + self.b.powi(2) + self.c.powi(2) + self.d.powi(2)
    }

    pub fn norm(self) -> Scalar {
        self.norm_squared().sqrt()
    }

    pub fn reciprocal(self) -> Self {
        self.conjugate() / self.norm_squared()
    }

    pub fn rotate(self, v: Vector) -> Vector {
        let p = Self::from_parts(0.0, v);
        (self * p * self.conjugate()).into_parts().1
    }

    fn map(self, op: impl Fn(Scalar) -> Scalar) -> Self {
        let a = op(self.a);
        let b = op(self.b);
        let c = op(self.c);
        let d = op(self.d);

        Self { a, b, c, d }
    }
}

impl Mul for Quaterion {
    type Output = Quaterion;

    fn mul(self, rhs: Self) -> Self::Output {
        let (r1, v1) = self.into_parts();
        let (r2, v2) = rhs.into_parts();
        Self::from_parts(r1 * r2 - v1.dot(v2), r1 * v2 + r2 * v1 + v1.cross(v2))
    }
}

impl MulAssign for Quaterion {
    fn mul_assign(&mut self, rhs: Self) {
        *self = *self * rhs;
    }
}

impl Div<Scalar> for Quaterion {
    type Output = Quaterion;

    fn div(self, rhs: Scalar) -> Self::Output {
        self.map(|r| r / rhs)
    }
}

#[cfg(test)]
mod tests {
    use std::f32::consts::PI;

    use super::*;

    #[test]
    fn mul() {
        let p = Quaterion::new(1.0, 2.0, 3.0, 4.0);
        let q = Quaterion::new(5.0, 6.0, 7.0, 8.0);

        let a = p.a * q.a - p.b * q.b - p.c * q.c - p.d * q.d;
        let b = p.a * q.b + p.b * q.a + p.c * q.d - p.d * q.c;
        let c = p.a * q.c - p.b * q.d + p.c * q.a + p.d * q.b;
        let d = p.a * q.d + p.b * q.c - p.c * q.b + p.d * q.a;

        let expected = Quaterion::new(a, b, c, d);

        assert_eq!(p * q, expected);
    }

    #[test]
    fn rotate() {
        let axis = Vector::OZ;
        let angle = PI / 2.0;
        let r = Quaterion::from_rotation(angle, axis);
        let v = Vector::OX;

        let expected = Vector::OY;

        assert!(Vector::distance(r.rotate(v), expected) < 1e-3);
    }

    #[test]
    fn compond_rotate() {
        let angle = PI / 2.0;
        let p = Quaterion::from_rotation(angle, Vector::OZ);
        let q = Quaterion::from_rotation(angle, Vector::OX);

        let r = q * p;

        let v = Vector::OX;

        let expected = Vector::OZ;

        assert!(Vector::distance(r.rotate(v), expected) < 1e-3);
    }

    #[test]
    fn rotate_identity() {
        let r = Quaterion::identity();

        assert!(Vector::distance(r.rotate(Vector::OX), Vector::OX) < 1e-3);
        assert!(Vector::distance(r.rotate(Vector::OY), Vector::OY) < 1e-3);
        assert!(Vector::distance(r.rotate(Vector::OZ), Vector::OZ) < 1e-3);
    }

    #[test]
    fn compound_rotate_identity() {
        let angle = PI / 2.0;
        let p = Quaterion::from_rotation(angle, Vector::OZ);
        let q = Quaterion::from_rotation(angle, Vector::OX);

        let r = Quaterion::identity() * q * Quaterion::identity() * p * Quaterion::identity();

        let v = Vector::OX;

        let expected = Vector::OZ;

        assert!(Vector::distance(r.rotate(v), expected) < 1e-3);
    }
}
