use std::{
    hash::Hash,
    ops::{Add, AddAssign, Div, Mul, Neg, Sub, SubAssign},
};

use super::Integer;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct IVector {
    pub x: Integer,
    pub y: Integer,
    pub z: Integer,
}
impl IVector {
    pub fn new(x: Integer, y: Integer, z: Integer) -> Self {
        Self { x, y, z }
    }
    pub fn lift(x: Integer) -> Self {
        Self::new(x, x, x)
    }
    pub fn combine(self, other: Self, f: fn(Integer, Integer) -> Integer) -> Self {
        Self::new(f(self.x, other.x), f(self.y, other.y), f(self.z, other.z))
    }
    pub fn reduce(self, f: impl Fn(Integer, Integer) -> Integer) -> Integer {
        f(f(self.x, self.y), self.z)
    }
    pub fn map(self, f: impl Fn(Integer) -> Integer) -> Self {
        IVector::new(f(self.x), f(self.y), f(self.z))
    }
    pub fn get(&self, axis: usize) -> Integer {
        match axis {
            0 => self.x,
            1 => self.y,
            2 => self.z,
            _ => panic!(),
        }
    }
    pub fn combine_mut(&mut self, other: Self, f: fn(&mut Integer, Integer)) {
        f(&mut self.x, other.x);
        f(&mut self.y, other.y);
        f(&mut self.z, other.z);
    }

    pub fn distance_squared(a: IVector, b: IVector) -> Integer {
        (a - b).length_squared()
    }
    pub fn dot(self, other: IVector) -> Integer {
        self.combine(other, Mul::mul).reduce(Add::add)
    }
    pub fn length_squared(self) -> Integer {
        self.dot(self)
    }
    pub fn cross(self, other: IVector) -> IVector {
        let x = self.y * other.z - self.z * other.y;
        let y = self.z * other.x - self.x * other.z;
        let z = self.x * other.y - self.y * other.x;

        IVector { x, y, z }
    }
    pub const OX: IVector = IVector { x: 1, y: 0, z: 0 };
    pub const OY: IVector = IVector { x: 0, y: 1, z: 0 };
    pub const OZ: IVector = IVector { x: 0, y: 0, z: 1 };
}

impl Sub for IVector {
    type Output = IVector;

    fn sub(self, other: Self) -> Self::Output {
        self.combine(other, Sub::sub)
    }
}

impl SubAssign for IVector {
    fn sub_assign(&mut self, other: Self) {
        self.combine_mut(other, SubAssign::sub_assign)
    }
}

impl Add for IVector {
    type Output = IVector;

    fn add(self, other: Self) -> Self::Output {
        self.combine(other, Add::add)
    }
}

impl AddAssign for IVector {
    fn add_assign(&mut self, other: Self) {
        self.combine_mut(other, AddAssign::add_assign)
    }
}

impl Mul<Integer> for IVector {
    type Output = IVector;

    fn mul(self, s: Integer) -> Self::Output {
        self.combine(IVector::lift(s), Mul::mul)
    }
}

impl Div<Integer> for IVector {
    type Output = IVector;

    fn div(self, s: Integer) -> Self::Output {
        self.combine(IVector::lift(s), Div::div)
    }
}

impl Mul for IVector {
    type Output = IVector;

    fn mul(self, other: Self) -> Self::Output {
        self.combine(other, Mul::mul)
    }
}

impl Neg for IVector {
    type Output = IVector;

    fn neg(self) -> Self::Output {
        self.map(|x| -x)
    }
}

impl Mul<IVector> for Integer {
    type Output = IVector;

    fn mul(self, v: IVector) -> IVector {
        v.combine(IVector::lift(self), Mul::mul)
    }
}

impl Eq for IVector {}

impl Hash for IVector {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
        self.z.hash(state);
    }
}
