use crate::{
    math::{Scalar, Vector, Quaterion},
    shapes::{Box, Shape, Sphere, AABB},
};

#[derive(Debug, Clone, Copy)]
pub struct Object {
    pub shape: Shape,
    pub p: Vector,
    pub prev_p: Vector,
    //pub r: Quaterion,
    pub inv_m: Scalar,
}

impl Object {
    pub fn new(shape: Shape, p: Vector) -> Object {
        Object {
            shape,
            p,
            prev_p: p,
            //r: Quaterion::identity(),
            inv_m: 1.0,
        }
    }

    pub fn get_bounding_aabb(&self) -> AABB {
        match self.shape {
            Shape::Sphere(Sphere { radius }) => {
                AABB::new(self.p - Vector::lift(radius), self.p + Vector::lift(radius))
            }
            Shape::Box(Box { extents }) => AABB::new(self.p - extents, self.p + extents),
            Shape::Plane(_) => todo!(),
        }
    }

    pub fn intersects(&self, other: &Object) -> Option<(Scalar, Vector)> {
        match (self.shape, other.shape) {
            (Shape::Sphere(s1), Shape::Sphere(s2)) => {
                let pen = (s1.radius + s2.radius) - Vector::distance(self.p, other.p);
                if pen > 0.0 {
                    Some((pen, (self.p - other.p).normalize()))
                } else {
                    None
                }
            }
            (Shape::Sphere(a), Shape::Box(b)) => {
                // TODO: Implement proper collision detection
                let normals = [Vector::OX, Vector::OY, Vector::OZ];

                let (pen, normal) = (0..3)
                    .flat_map(|axis| {
                        [
                            (
                                self.p.get(axis) + a.radius - other.p.get(axis)
                                    + b.extents.get(axis),
                                -normals[axis],
                            ),
                            (
                                other.p.get(axis) + b.extents.get(axis) - self.p.get(axis)
                                    + a.radius,
                                normals[axis],
                            ),
                        ]
                    })
                    .reduce(|(pen1, normal1), (pen2, normal2)| {
                        if pen1 < pen2 {
                            (pen1, normal1)
                        } else {
                            (pen2, normal2)
                        }
                    })
                    .unwrap();

                if pen > 0.0 {
                    Some((pen, normal))
                } else {
                    None
                }
            }
            (Shape::Box(a), Shape::Sphere(b)) => {
                // TODO: Implement proper collision detection
                let normals = [Vector::OX, Vector::OY, Vector::OZ];

                let (pen, normal) = (0..3)
                    .flat_map(|axis| {
                        [
                            (
                                self.p.get(axis) + a.extents.get(axis) - other.p.get(axis)
                                    + b.radius,
                                -normals[axis],
                            ),
                            (
                                other.p.get(axis) + b.radius - self.p.get(axis)
                                    + a.extents.get(axis),
                                normals[axis],
                            ),
                        ]
                    })
                    .reduce(|(pen1, normal1), (pen2, normal2)| {
                        if pen1 < pen2 {
                            (pen1, normal1)
                        } else {
                            (pen2, normal2)
                        }
                    })
                    .unwrap();

                if pen > 0.0 {
                    Some((pen, normal))
                } else {
                    None
                }
            }
            (Shape::Box(a), Shape::Box(b)) => {
                let normals = [Vector::OX, Vector::OY, Vector::OZ];

                let (pen, normal) = (0..3)
                    .flat_map(|axis| {
                        [
                            (
                                self.p.get(axis) + a.extents.get(axis) - other.p.get(axis)
                                    + b.extents.get(axis),
                                -normals[axis],
                            ),
                            (
                                other.p.get(axis) + b.extents.get(axis) - self.p.get(axis)
                                    + a.extents.get(axis),
                                normals[axis],
                            ),
                        ]
                    })
                    .reduce(|(pen1, normal1), (pen2, normal2)| {
                        if pen1 < pen2 {
                            (pen1, normal1)
                        } else {
                            (pen2, normal2)
                        }
                    })
                    .unwrap();

                if pen > 0.0 {
                    Some((pen, normal))
                } else {
                    None
                }
            },
            (Shape::Plane(a), Shape::Sphere(b)) => {
                let proj = self.p.dot(other.p);
                let pen = a.distance - proj + b.radius;
                if pen > 0.0 {
                    Some((pen, -self.p))
                } else {
                    None
                }
            },
            (Shape::Sphere(a), Shape::Plane(b)) => {
                let proj = other.p.dot(self.p);
                let pen = b.distance - proj + a.radius;
                if pen > 0.0 {
                    Some((pen, other.p))
                } else {
                    None
                }
            },
            (Shape::Plane(_), Shape::Plane(_)) => None,
            (Shape::Plane(a), Shape::Box(b)) => {
                let radius = b.extents.reduce(Scalar::min);
                let proj = self.p.dot(other.p);
                let pen = a.distance - proj + radius;
                if pen > 0.0 {
                    Some((pen, -self.p))
                } else {
                    None
                }
            },
            (Shape::Box(a), Shape::Plane(b)) => {
                let radius = a.extents.reduce(Scalar::min);
                let proj = other.p.dot(self.p);
                let pen = b.distance - proj + radius;
                if pen > 0.0 {
                    Some((pen, other.p))
                } else {
                    None
                }
            },
        }
    }

    pub fn get_prev_bounding_aabb(&self) -> AABB {
        match self.shape {
            Shape::Sphere(Sphere { radius }) => AABB::new(
                self.prev_p - Vector::lift(radius),
                self.prev_p + Vector::lift(radius),
            ),
            Shape::Box(Box { extents }) => AABB::new(self.prev_p - extents, self.prev_p + extents),
            Shape::Plane(_) => todo!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_intersections() {
        let x = Object::new(
            Shape::sphere(3.),
            Vector {
                x: 0.,
                y: 0.,
                z: 0.,
            },
        );
        let y = Object::new(
            Shape::sphere(9.),
            Vector {
                x: 0.,
                y: 0.,
                z: 4.,
            },
        );
        let z = Object::new(
            Shape::sphere(8.),
            Vector {
                x: 500.,
                y: 6.,
                z: 7.,
            },
        );

        assert_eq!(x.intersects(&y).is_some(), true);
        assert_eq!(x.intersects(&z).is_some(), false);
        assert_eq!(y.intersects(&z).is_some(), false);
    }
}
