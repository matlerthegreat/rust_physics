pub mod arena;
pub mod constraint;
pub mod iterator;
pub mod math;
pub mod multihead_linked_list;
pub mod objects;
pub mod random;
pub mod shapes;

pub fn sort_pair<T: Eq + Ord>((a, b): (T, T)) -> (T, T) {
    if a < b {
        (a, b)
    } else {
        (b, a)
    }
}
