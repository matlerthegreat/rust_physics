use std::collections::HashSet;
use std::hash::Hash;

pub struct Unique<I>
where
    I: Iterator,
{
    visited: HashSet<I::Item>,
    inner: I,
}

impl<I> Iterator for Unique<I>
where
    I: Iterator,
    I::Item: Hash + Eq + Clone,
{
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner
            .by_ref()
            .find(|value| self.visited.insert(value.clone()))
    }
}

pub trait UniqueExt: Iterator {
    fn unique(self) -> Unique<Self>
    where
        Self::Item: Hash + Eq + Clone,
        Self: Sized,
    {
        Unique {
            visited: HashSet::new(),
            inner: self,
        }
    }
}

impl<I: Iterator> UniqueExt for I {}
