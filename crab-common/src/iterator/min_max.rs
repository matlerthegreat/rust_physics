pub trait MinMaxExt: Iterator {
    fn min_max(mut self) -> Option<(Self::Item, Self::Item)>
    where
        Self::Item: PartialOrd + Copy,
        Self: Sized,
    {
        let first = self.next();
        first.map(|v| self.fold((v, v), |(min, max), v| {
            let min = if min < v {min} else {v};
            let max = if max > v {max} else {v};
            (min, max)
        }))
    }
}

impl<I: Iterator> MinMaxExt for I {}
