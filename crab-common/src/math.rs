pub type Integer = i32;
pub type Scalar = f32;

mod ivector;
mod quaterion;
mod vector;

pub use ivector::IVector;
pub use quaterion::Quaterion;
pub use vector::Vector;

/*#[derive(Debug, Clone, Copy, PartialEq)]
pub struct TVector<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

pub type Vector = TVector<Scalar>;
pub type Point = Vector;
pub type IVector = TVector<Integer>;

impl<T: Copy> TVector<T> {
    pub fn new(x: T, y: T, z: T) -> TVector<T> {
        TVector { x, y, z }
    }
    pub fn lift(x: T) -> TVector<T> {
        TVector::new(x, x, x)
    }
    pub fn combine(&self, other: &TVector<T>, f: fn(T, T) -> T) -> TVector<T> {
        TVector::new(f(self.x, other.x), f(self.y, other.y), f(self.z, other.z))
    }
    pub fn reduce(&self, f: fn(T, T) -> T) -> T {
        f(f(self.x, self.y), self.z)
    }
    pub fn map<U: Copy>(&self, f: fn(T) -> U) -> TVector<U> {
        TVector::new(f(self.x), f(self.y), f(self.z))
    }
    pub fn get(&self, axis: usize) -> T {
        match axis {
            0 => self.x,
            1 => self.y,
            2 => self.z,
            _ => panic!(),
        }
    }
    pub fn combine_mut(&mut self, other: &TVector<T>, f: fn(&mut T, T)) {
        f(&mut self.x, other.x);
        f(&mut self.y, other.y);
        f(&mut self.z, other.z);
    }

    pub fn distance(a: Vector, b: Vector) -> Scalar {
        (a - b).length()
    }
    pub fn distance_squared(a: &Vector, b: &Vector) -> Scalar {
        (a - b).length_squared()
    }
}

impl Vector {
    pub fn dot(&self, other: &Vector) -> Scalar {
        self.combine(other, Mul::mul).reduce(Add::add)
    }
    pub fn length_squared(&self) -> Scalar {
        self.dot(self)
    }
    pub fn length(&self) -> Scalar {
        self.length_squared().sqrt()
    }
    pub fn floor(&self) -> IVector {
        self.map(|x| x.floor() as Integer)
    }
    pub fn ceil(&self) -> IVector {
        self.map(|x| x.ceil() as Integer)
    }
    pub fn normalize(self) -> Vector {
        self * (1.0 / self.length())
    }
    pub fn cross(&self, other: &Vector) -> Vector {
        let x = self.y*other.z - self.z*other.y;
        let y = self.z*other.x - self.x*other.z;
        let z = self.x*other.y - self.y*other.x;

        Vector{ x, y, z }
    }
    pub const OX: Vector = Vector {
        x: 1.0,
        y: 0.0,
        z: 0.0,
    };
    pub const OY: Vector = Vector {
        x: 0.0,
        y: 1.0,
        z: 0.0,
    };
    pub const OZ: Vector = Vector {
        x: 0.0,
        y: 0.0,
        z: 1.0,
    };
}

impl<T: Copy + Sub + Sub<Output = T>> Sub for &TVector<T> {
    type Output = TVector<T>;

    fn sub(self, other: Self) -> TVector<T> {
        self.combine(other, Sub::sub)
    }
}

impl<T: Copy + Sub + Sub<Output = T>> Sub for TVector<T> {
    type Output = TVector<T>;

    fn sub(self, other: Self) -> TVector<T> {
        self.combine(&other, Sub::sub)
    }
}

impl<T: Copy + SubAssign> SubAssign for &mut TVector<T> {
    fn sub_assign(&mut self, other: Self) {
        self.combine_mut(other, SubAssign::sub_assign)
    }
}

impl<T: Copy + SubAssign> SubAssign for TVector<T> {
    fn sub_assign(&mut self, other: Self) {
        self.combine_mut(&other, SubAssign::sub_assign)
    }
}

impl<T: Copy + Add + Add<Output = T>> Add for &TVector<T> {
    type Output = TVector<T>;

    fn add(self, other: Self) -> TVector<T> {
        self.combine(other, Add::add)
    }
}

impl<T: Copy + Add + Add<Output = T>> Add for TVector<T> {
    type Output = TVector<T>;

    fn add(self, other: Self) -> TVector<T> {
        self.combine(&other, Add::add)
    }
}

impl<T: Copy + AddAssign> AddAssign for &mut TVector<T> {
    fn add_assign(&mut self, other: Self) {
        self.combine_mut(other, AddAssign::add_assign)
    }
}

impl<T: Copy + AddAssign> AddAssign for TVector<T> {
    fn add_assign(&mut self, other: Self) {
        self.combine_mut(&other, AddAssign::add_assign)
    }
}

impl<T: Copy + Mul<Output = T>> Mul<T> for &TVector<T> {
    type Output = TVector<T>;

    fn mul(self, scale: T) -> TVector<T> {
        self.combine(&TVector::lift(scale), Mul::mul)
    }
}

impl<T: Copy + Mul<Output = T>> Mul<T> for TVector<T> {
    type Output = TVector<T>;

    fn mul(self, scale: T) -> TVector<T> {
        self.combine(&TVector::lift(scale), Mul::mul)
    }
}

impl<T: Copy + Div<Output = T>> Div<T> for &TVector<T> {
    type Output = TVector<T>;

    fn div(self, scale: T) -> TVector<T> {
        self.combine(&TVector::lift(scale), Div::div)
    }
}

impl<T: Copy + Div<Output = T>> Div<T> for TVector<T> {
    type Output = TVector<T>;

    fn div(self, scale: T) -> TVector<T> {
        self.combine(&TVector::lift(scale), Div::div)
    }
}

impl<T: Copy + Mul<Output = T>> Mul for &TVector<T> {
    type Output = TVector<T>;

    fn mul(self, other: Self) -> TVector<T> {
        self.combine(other, Mul::mul)
    }
}

impl<T: Copy + Mul<Output = T>> Mul for TVector<T> {
    type Output = TVector<T>;

    fn mul(self, other: Self) -> TVector<T> {
        self.combine(&other, Mul::mul)
    }
}

impl<T: Copy + Neg<Output = T>> Neg for &TVector<T> {
    type Output = TVector<T>;

    fn neg(self) -> Self::Output {
        self.map(|x| -x)
    }
}

impl<T: Copy + Neg<Output = T>> Neg for TVector<T> {
    type Output = TVector<T>;

    fn neg(self) -> Self::Output {
        self.map(|x| -x)
    }
}

impl Mul<Vector> for Scalar {
    type Output = Vector;

    fn mul(self, v: Vector) -> Vector {
        v.combine(&TVector::lift(self), Mul::mul)
    }
}

impl<T: Eq> Eq for TVector<T> {}

impl<T: Hash> Hash for TVector<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
        self.z.hash(state);
    }
}

impl From<IVector> for Vector {
    fn from(value: IVector) -> Self {
        value.map(|x| x as Scalar)
    }
}

impl From<&IVector> for Vector {
    fn from(value: &IVector) -> Self {
        value.map(|x| x as Scalar)
    }
}

impl From<Vector> for [Scalar; 3] {
    fn from(value: Vector) -> Self {
        unsafe { transmute(value) }
    }
}

impl<'a, T> From<&'a mut TVector<T>> for &'a mut [T; 3] {
    fn from(value: &'a mut TVector<T>) -> Self {
        unsafe { transmute(value) }
    }
}

impl<'a, T> From<&'a [T; 3]> for &'a TVector<T> {
    fn from(value: &'a [T; 3]) -> Self {
        unsafe { &(*(value.as_ptr() as *const TVector<T>)) }
    }
}*/

/*#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn vector_addition() {
        let a = Vector{xyz: [1., 2., 3.]};
        let b = Vector{xyz: [3., 2., 1.]};
        assert_eq!(&a + &b, Vector{xyz: [4., 4., 4.]});
    }

    #[test]
    fn vector_length() {
        let a = Vector{xyz: [2., 3., 6.]};
        assert_eq!(a.length(), 7.);
    }
}*/
