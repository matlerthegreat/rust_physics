use super::common::{Gen, Offset};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct WeakProxy {
    pub(super) gen: Gen,
    pub(super) offset: Offset,
}

impl From<WeakProxy> for (Gen, Offset) {
    fn from(WeakProxy { gen, offset }: WeakProxy) -> Self {
        (gen, offset)
    }
}
