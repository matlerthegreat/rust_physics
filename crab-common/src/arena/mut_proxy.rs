use std::ops::{Deref, DerefMut};

use super::{
    common::{Gen, Offset},
    weak_proxy::WeakProxy,
};

#[derive(Debug)]
pub struct MutProxy<'a, T> {
    pub(super) gen: Gen,
    pub(super) offset: Offset,
    pub(super) value: &'a mut T,
}
impl<'a, T> MutProxy<'a, T> {
    pub fn downgrade(this: &MutProxy<'a, T>) -> WeakProxy {
        WeakProxy {
            gen: this.gen,
            offset: this.offset,
        }
    }
}

impl<'a, T> Deref for MutProxy<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<'a, T> DerefMut for MutProxy<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value
    }
}
