#[derive(Debug, PartialEq)]
pub enum Error {
    Free,
    GenMissmatch,
}

pub(super) type Gen = u16;
pub(super) type Offset = u16;
