---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Rust ^gGGdEpnp

C++ ^p7S5af9d

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "freedraw",
			"version": 79,
			"versionNonce": 123992782,
			"isDeleted": false,
			"id": "M4nb16Bzj1iSRD03xFNCm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -537.288325281886,
			"y": 174.2807910920596,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 1807108494,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 79,
			"versionNonce": 218652946,
			"isDeleted": false,
			"id": "BKp-sPHipCS8-mRQZC0o5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -421.2885466226425,
			"y": 161.9678790547175,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 546884686,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 79,
			"versionNonce": 1861710094,
			"isDeleted": false,
			"id": "2NYR4KW4QYzWujTcWlvbt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -229.42797824884718,
			"y": 115.50347664366751,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 1327580942,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 79,
			"versionNonce": 859729618,
			"isDeleted": false,
			"id": "MguO1BU_3IeWq95gBuIoW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -36.476364395868586,
			"y": 36.78697477408707,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 745716174,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 81,
			"versionNonce": 2034376526,
			"isDeleted": false,
			"id": "SnV7-Gmo0-ArASyHaj7NR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 155.10812971699977,
			"y": -71.45180426197788,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.34012348946200877,
			"seed": 1016079502,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.34012348946200877
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 296,
			"versionNonce": 1257030802,
			"isDeleted": false,
			"id": "c1DBIXlzla03K-H99SQ71",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 153.69683809514106,
			"y": -73.71561320157889,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 691.1441821513214,
			"height": 244.61062955763006,
			"seed": 333548494,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-190.7032650719896,
					108.28957670007554
				],
				[
					-384.53610796584695,
					185.58374397736006
				],
				[
					-575.0582683226688,
					234.8243491562914
				],
				[
					-691.1441821513214,
					244.61062955763006
				]
			]
		},
		{
			"type": "line",
			"version": 272,
			"versionNonce": 983369102,
			"isDeleted": false,
			"id": "f8_KkP0vAG_XoSTqet5bg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -538.7172571914584,
			"y": 176.59010577165802,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 683.7737156240881,
			"height": 0,
			"seed": 195462862,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					683.7737156240881,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 134,
			"versionNonce": 231367250,
			"isDeleted": false,
			"id": "gGGdEpnp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.874820892634,
			"y": -132.23524621690868,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 65.91227722167969,
			"height": 35,
			"seed": 2055122254,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Rust",
			"rawText": "Rust",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Rust",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "text",
			"version": 85,
			"versionNonce": 1413555150,
			"isDeleted": false,
			"id": "p7S5af9d",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.2467498469856,
			"y": -101.7614428283194,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 53.0322265625,
			"height": 35,
			"seed": 548624910,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "C++",
			"rawText": "C++",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C++",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"id": "7y4S9gibnh8cg_nffRBXG",
			"type": "line",
			"x": 145.99451220427545,
			"y": 43.848651659423126,
			"width": 685.1719075462734,
			"height": 130.3864336823858,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1859442450,
			"version": 431,
			"versionNonce": 1760093902,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695917132093,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-190.12428860431785,
					53.46540316602175
				],
				[
					-381.3755775219026,
					95.4572701197401
				],
				[
					-569.9012527152606,
					121.0964946924649
				],
				[
					-685.1719075462734,
					130.3864336823858
				]
			],
			"lastCommittedPoint": [
				-685.3213914572239,
				344.08691684238215
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"type": "line",
			"version": 370,
			"versionNonce": 1654319118,
			"isDeleted": true,
			"id": "zmoxM1r-Soke-3S_GMvxl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0,
			"x": -538.0316490507741,
			"y": 172.59289667432265,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 691.9766749823971,
			"height": 329.2589641975111,
			"seed": 944572882,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					114.6971774530349,
					-14.592078262931594
				],
				[
					307.7987773580857,
					-72.7929914832364
				],
				[
					500.398177768872,
					-173.1527270366288
				],
				[
					691.9766749823971,
					-329.2589641975111
				]
			]
		},
		{
			"type": "freedraw",
			"version": 95,
			"versionNonce": 1458694098,
			"isDeleted": true,
			"id": "Fu_lHo_dTuN9OoFWR0hVE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -536.753751746119,
			"y": 169.60327714977956,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 2.1903442731077583,
			"height": 0.2334166663484135,
			"seed": 231902798,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.2145547135122188,
					0
				],
				[
					-0.5139882147874257,
					0
				],
				[
					-1.6009082469754417,
					0
				],
				[
					-2.1903442731077583,
					0
				],
				[
					-1.8343249133237431,
					0
				],
				[
					-1.574973061825506,
					0
				],
				[
					-1.1953762609963594,
					0
				],
				[
					-0.8134217160626327,
					0
				],
				[
					-0.5187037029964745,
					0
				],
				[
					-0.5187037029964745,
					-0.2334166663484135
				],
				[
					-0.306506733588836,
					-0.2334166663484135
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 83,
			"versionNonce": 928953934,
			"isDeleted": true,
			"id": "28M0vhRQGPOHx0nYDjjJH",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -421.43177210538386,
			"y": 157.19211218353752,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.5399233999371935,
			"seed": 874570766,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.5399233999371935
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 82,
			"versionNonce": 2076777874,
			"isDeleted": true,
			"id": "jD0zq_fgJdT3CugYcCweV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0,
			"x": -229.99709728223513,
			"y": 100.10641192467517,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 730504014,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 84,
			"versionNonce": 1460583566,
			"isDeleted": true,
			"id": "FbCReKIVHaBvCp8Dg3pUl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0,
			"x": -38.06022296482172,
			"y": -1.4604886102363537,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 118584846,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 84,
			"versionNonce": 1941150546,
			"isDeleted": true,
			"id": "GkI_kIAT8gvRMqjyeN63N",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0,
			"x": 154.8197489944033,
			"y": -159.33503384951564,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 1098729678,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"id": "6PvdcKu-mfOzpWrC2SodK",
			"type": "image",
			"x": -585.6993648181698,
			"y": -240.56742103600564,
			"width": 805.6923512113235,
			"height": 453.20194755636953,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 567707346,
			"version": 316,
			"versionNonce": 1221264142,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1695917125842,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "0d998025c9babb09fb2e6c8d650ac569f6609bed",
			"scale": [
				1,
				1
			]
		},
		{
			"id": "4Dqd5cRAcIQRv7qrfCZyG",
			"type": "freedraw",
			"x": 144.97245734320035,
			"y": -174.94326370640857,
			"width": 0.0001,
			"height": 0.0001,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 5222094,
			"version": 6,
			"versionNonce": 1481079310,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1695917117330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0.0001,
				0.0001
			]
		},
		{
			"id": "3EPdABWQZlxbRSKqwErfp",
			"type": "freedraw",
			"x": -44.46248307556573,
			"y": -34.37411346674509,
			"width": 0.0001,
			"height": 0.0001,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1865759118,
			"version": 5,
			"versionNonce": 1301247442,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1695917117331,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0.0001,
				0.0001
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#e03131",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 28,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 664.352658994988,
		"scrollY": 257.91877064683155,
		"zoom": {
			"value": 1.7638394045014292
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%