---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "diamond",
			"version": 343,
			"versionNonce": 962566355,
			"isDeleted": false,
			"id": "y8lg0An-uOxlsda0KsYb3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0.31204179593418147,
			"x": -425.8677884575795,
			"y": -459.3814957186028,
			"strokeColor": "#e03131",
			"backgroundColor": "#ffc9c9",
			"width": 109.37399281094117,
			"height": 62.09183042657534,
			"seed": 2076999261,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 274,
			"versionNonce": 1084314077,
			"isDeleted": false,
			"id": "c71tew9k0K0emJkMUHpC2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -167.05676638014933,
			"y": -376.57232584693514,
			"strokeColor": "#f08c00",
			"backgroundColor": "#ffec99",
			"width": 192.6518883221233,
			"height": 90.43069001296813,
			"seed": 2110171837,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 250,
			"versionNonce": 305858163,
			"isDeleted": false,
			"id": "gfsncpsdDkTiA3VJD3WeT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0.4472916559344631,
			"x": -216.92081756131523,
			"y": -340.09808491166183,
			"strokeColor": "#1971c2",
			"backgroundColor": "#a5d8ff",
			"width": 184.7196357194545,
			"height": 117.5343476886998,
			"seed": 746343197,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 294,
			"versionNonce": 129306173,
			"isDeleted": false,
			"id": "mj9B_c8Gl7EHxtvXH3rmm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 5.780680486671173,
			"x": -380.66490096131344,
			"y": -198.2833031191546,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 146.7199790493538,
			"height": 76.16208595845752,
			"seed": 1214177149,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 393,
			"versionNonce": 1150795795,
			"isDeleted": false,
			"id": "P1xzXEYIfSVVh5a_I3mik",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0.13904211561648694,
			"x": -421.18781877389176,
			"y": -442.70483791065607,
			"strokeColor": "#e03131",
			"backgroundColor": "#ffc9c9",
			"width": 109.37399281094117,
			"height": 62.09183042657534,
			"seed": 981310771,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 312,
			"versionNonce": 1104297629,
			"isDeleted": false,
			"id": "YbDqK7zJiwMwmVheuURz_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 5.656851415903199,
			"x": -348.1132789983613,
			"y": -222.6305906765973,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 146.7199790493538,
			"height": 76.16208595845752,
			"seed": 1622338707,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 303,
			"versionNonce": 711738803,
			"isDeleted": false,
			"id": "3Ow9ifTLP9PT0bPqEk0h3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0.7613279746674015,
			"x": -274.23481785972734,
			"y": -319.47214917329734,
			"strokeColor": "#1971c2",
			"backgroundColor": "#a5d8ff",
			"width": 184.7196357194545,
			"height": 117.5343476886998,
			"seed": 95194493,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		},
		{
			"type": "diamond",
			"version": 333,
			"versionNonce": 900358909,
			"isDeleted": false,
			"id": "hN9xpGg8Q81mM3WGhAQs0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 6.082391036428366,
			"x": -182.44436521369335,
			"y": -413.49844533543154,
			"strokeColor": "#f08c00",
			"backgroundColor": "#ffec99",
			"width": 192.6518883221233,
			"height": 90.43069001296813,
			"seed": 1002043165,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695715457798,
			"link": null,
			"locked": false
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 469.6947869178263,
		"scrollY": 604.6038929896628,
		"zoom": {
			"value": 1.6
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%