---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Rust ^gGGdEpnp

C++ ^p7S5af9d

broadphase time (lower is better) ^WKcvMEme

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "line",
			"version": 265,
			"versionNonce": 284613586,
			"isDeleted": false,
			"id": "f8_KkP0vAG_XoSTqet5bg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -538.7172571914584,
			"y": 176.59010577165802,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 683.7737156240881,
			"height": 0,
			"seed": 195462862,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695915477523,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					683.7737156240881,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 127,
			"versionNonce": 53297230,
			"isDeleted": false,
			"id": "gGGdEpnp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.874820892634,
			"y": -132.23524621690868,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 65.91227722167969,
			"height": 35,
			"seed": 2055122254,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915486946,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Rust",
			"rawText": "Rust",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Rust",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "text",
			"version": 78,
			"versionNonce": 1947688850,
			"isDeleted": false,
			"id": "p7S5af9d",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.2467498469856,
			"y": -101.7614428283194,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 53.0322265625,
			"height": 35,
			"seed": 548624910,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915483378,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "C++",
			"rawText": "C++",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C++",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "freedraw",
			"version": 3,
			"versionNonce": 1046380110,
			"isDeleted": false,
			"id": "2LyPPfOTbOcLXoVoFUA0c",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -541.4328832849994,
			"y": 174.88171980769835,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1376104978,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915790421,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 5,
			"versionNonce": 12569998,
			"isDeleted": false,
			"id": "PlTflF72aXSLv4B6e9_0x",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -426.8214220358359,
			"y": 170.14237052806777,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.28766605614634955,
			"height": 0,
			"seed": 1788695378,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915791568,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.28766605614634955,
					0
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1752695310,
			"isDeleted": false,
			"id": "FEf_0-JLbu7H1txg7F_8_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -236.6415159663897,
			"y": 158.68877308689432,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1015349266,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923796513,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1183604558,
			"isDeleted": false,
			"id": "J2QQoFsXTbndYl_JiWlOR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -46.15354199284354,
			"y": 124.19740893647051,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 913793362,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923790333,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 653856658,
			"isDeleted": false,
			"id": "lq6MlXwhC7JDBQ0_THP23",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 143.38166568126775,
			"y": -194.8426089929614,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1253984914,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923787799,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 3,
			"versionNonce": 484667218,
			"isDeleted": false,
			"id": "bJokuEPZPTHhHbN8JD0wy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -541.57773640547,
			"y": 175.17142604864003,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 24191566,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915804498,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 3,
			"versionNonce": 2025658514,
			"isDeleted": false,
			"id": "APensffEJABX5tugSQubS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -428.92281237506046,
			"y": 171.24203013276897,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1217486094,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915806038,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 6,
			"versionNonce": 2135928658,
			"isDeleted": false,
			"id": "4lmNgtYZT-m_lIS5go2O_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -237.3412993511992,
			"y": 162.20605167410181,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.21013903392247357,
			"seed": 1083346894,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923798368,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.21013903392247357
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 119678610,
			"isDeleted": false,
			"id": "CG7fwnl-b81j5M2nuKdrx",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -46.224948460680935,
			"y": 142.50194692047629,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 951643918,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923806937,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1117712206,
			"isDeleted": false,
			"id": "hKFW4CqF8DcP-3ZbFtljT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 144.12837331637127,
			"y": -123.28312729556907,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1246454222,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923809867,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 196,
			"versionNonce": 358673870,
			"isDeleted": false,
			"id": "WRwmUi5ELuO_MohjH82jG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 144.55885230819308,
			"y": -124.56436334705768,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 686.4752593896934,
			"height": 299.6643829278601,
			"seed": 387723534,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923782900,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-191.4162580554368,
					267.9497102839266
				],
				[
					-382.71622557753784,
					287.50488154749064
				],
				[
					-570.1541232820148,
					297.2712461628981
				],
				[
					-686.4752593896934,
					299.6643829278601
				]
			]
		},
		{
			"type": "line",
			"version": 197,
			"versionNonce": 821988306,
			"isDeleted": false,
			"id": "RWc3bcaIJeKio7y6A3Qcs",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 143.4510319643104,
			"y": -194.47741591459123,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 685.2735905452239,
			"height": 368.1064622579362,
			"seed": 1853720594,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923785272,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-190.23907142851175,
					318.6483024487221
				],
				[
					-379.8273239073024,
					353.3130823067519
				],
				[
					-570.2398110434201,
					364.5300183116632
				],
				[
					-685.2735905452239,
					368.1064622579362
				]
			]
		},
		{
			"type": "text",
			"version": 152,
			"versionNonce": 2127944270,
			"isDeleted": false,
			"id": "WKcvMEme",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -374.03870341405445,
			"y": 179.93224515565515,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 330.319580078125,
			"height": 25,
			"seed": 1462708434,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923918350,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "broadphase time (lower is better)",
			"rawText": "broadphase time (lower is better)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "broadphase time (lower is better)",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 28,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 565.2442407559442,
		"scrollY": 455.2798084157734,
		"zoom": {
			"value": 1.05
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%