---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "line",
			"version": 109,
			"versionNonce": 1202659800,
			"isDeleted": false,
			"id": "nHi3EUpZwOFQokthLnWBn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -250.98828125,
			"y": 53.88671875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 482.85546875,
			"height": 0,
			"seed": 315596200,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312111,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					482.85546875,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 15,
			"versionNonce": 665749416,
			"isDeleted": false,
			"id": "xUfJudJh08ln3Up5dSVLJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -228.6328125,
			"y": 57.4609375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 31.4375,
			"height": 24.05078125,
			"seed": 1941974440,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312111,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-31.4375,
					24.05078125
				]
			]
		},
		{
			"type": "line",
			"version": 13,
			"versionNonce": 1444751064,
			"isDeleted": false,
			"id": "MRBk-GMmths48hVQPinDd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -198.96875,
			"y": 57.890625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 26.828125,
			"height": 18.171875,
			"seed": 1650646952,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312111,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-26.828125,
					18.171875
				]
			]
		},
		{
			"type": "line",
			"version": 17,
			"versionNonce": 47141544,
			"isDeleted": false,
			"id": "AB0Ki8Lt0Z0xxoz4H5giv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -159.13671875,
			"y": 60.3046875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 34.15234375,
			"height": 22.671875,
			"seed": 1023287512,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312111,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-34.15234375,
					22.671875
				]
			]
		},
		{
			"type": "line",
			"version": 15,
			"versionNonce": 1384287192,
			"isDeleted": false,
			"id": "nhEwmJi9-Q9_Su61XOGJP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -132.62890625,
			"y": 63.65234375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 27.3203125,
			"height": 19.97265625,
			"seed": 1186411176,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-27.3203125,
					19.97265625
				]
			]
		},
		{
			"type": "line",
			"version": 12,
			"versionNonce": 1973337512,
			"isDeleted": false,
			"id": "oGbzi2ofSXQcqtuxzNISr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -94.4765625,
			"y": 62.33203125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 23,
			"height": 21.93359375,
			"seed": 2036901848,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-23,
					21.93359375
				]
			]
		},
		{
			"type": "line",
			"version": 13,
			"versionNonce": 1190351064,
			"isDeleted": false,
			"id": "0sPutVGpFzCrmhDLxsxnn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -60.29296875,
			"y": 58.29296875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 27.41015625,
			"height": 19.69921875,
			"seed": 1020839080,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-27.41015625,
					19.69921875
				]
			]
		},
		{
			"type": "line",
			"version": 14,
			"versionNonce": 1363724456,
			"isDeleted": false,
			"id": "kGTvUxocqCsYaQZXtE2cs",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -27.75,
			"y": 64,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 24.84765625,
			"height": 19.15625,
			"seed": 1399540696,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-24.84765625,
					19.15625
				]
			]
		},
		{
			"type": "line",
			"version": 22,
			"versionNonce": 1526953432,
			"isDeleted": false,
			"id": "xPRTPOXuanent-bSeVsAD",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 8.91015625,
			"y": 54.64453125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 22.91796875,
			"height": 27.61328125,
			"seed": 1640027816,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-22.91796875,
					27.61328125
				]
			]
		},
		{
			"type": "line",
			"version": 15,
			"versionNonce": 1635349416,
			"isDeleted": false,
			"id": "dRI9u2wsjPcd7ri0n8vQs",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 52.41796875,
			"y": 56.01953125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 44.34375,
			"height": 26.07421875,
			"seed": 1280607960,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-44.34375,
					26.07421875
				]
			]
		},
		{
			"type": "line",
			"version": 14,
			"versionNonce": 600242904,
			"isDeleted": false,
			"id": "X-Mav2IaYSMD2tPBA09XC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 62.4765625,
			"y": 63.4375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13.83203125,
			"height": 19.25390625,
			"seed": 1488205480,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-13.83203125,
					19.25390625
				]
			]
		},
		{
			"type": "line",
			"version": 15,
			"versionNonce": 475840168,
			"isDeleted": false,
			"id": "DuzkI-YsuyFFt2Nz_A3i5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 112.75390625,
			"y": 54.56640625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 38.33984375,
			"height": 25.45703125,
			"seed": 1729724120,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-38.33984375,
					25.45703125
				]
			]
		},
		{
			"type": "line",
			"version": 12,
			"versionNonce": 1943134168,
			"isDeleted": false,
			"id": "Mp6wbBzc0FOpYcYCb3qFn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 136.21875,
			"y": 65.48828125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 36.9140625,
			"height": 27.3359375,
			"seed": 1448001192,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-36.9140625,
					27.3359375
				]
			]
		},
		{
			"type": "line",
			"version": 9,
			"versionNonce": 1386129832,
			"isDeleted": false,
			"id": "Sno31FVagHzCtzfoWa066",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 169.390625,
			"y": 62.5703125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 21.7265625,
			"height": 22.90234375,
			"seed": 840027352,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312112,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-21.7265625,
					22.90234375
				]
			]
		},
		{
			"type": "line",
			"version": 13,
			"versionNonce": 725099736,
			"isDeleted": false,
			"id": "bGAjGAAzItX9ku5r9jn8r",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 207.13671875,
			"y": 57.67578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 27.29296875,
			"height": 25.4453125,
			"seed": 900230824,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-27.29296875,
					25.4453125
				]
			]
		},
		{
			"type": "line",
			"version": 18,
			"versionNonce": 1600861352,
			"isDeleted": false,
			"id": "fQDUpLn1cV6BasYvV4tX-",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 246.59765625,
			"y": 60.09765625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 33.17578125,
			"height": 27.45703125,
			"seed": 1961345496,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-33.17578125,
					27.45703125
				]
			]
		},
		{
			"type": "ellipse",
			"version": 162,
			"versionNonce": 1428801678,
			"isDeleted": false,
			"id": "oL5H6mGnVlPdnmHMdzJpO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -79.9140625,
			"y": -20.56640625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 71.015625,
			"height": 74.84375,
			"seed": 1492872360,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695908798320,
			"link": null,
			"locked": false
		},
		{
			"type": "line",
			"version": 159,
			"versionNonce": 1483628456,
			"isDeleted": false,
			"id": "o09-i5GcR1DhgTFCJENcG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -95.2109375,
			"y": 16.8515625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.76953125,
			"height": 23.4609375,
			"seed": 1636036056,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-37.08984375,
					-23.40625
				],
				[
					-66.76953125,
					0.0546875
				]
			]
		},
		{
			"type": "line",
			"version": 130,
			"versionNonce": 1944506072,
			"isDeleted": false,
			"id": "sIjmcKxpGDpK31kAohVJc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -162.375,
			"y": 17.01171875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 86.859375,
			"height": 37.55859375,
			"seed": 905852328,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-46.05078125,
					-37.55859375
				],
				[
					-86.859375,
					-0.15234375
				]
			]
		},
		{
			"type": "line",
			"version": 44,
			"versionNonce": 1037314728,
			"isDeleted": false,
			"id": "wBMbuB9GP7lLWG21Ltd_e",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -94.55859375,
			"y": 15.8203125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 50.71875,
			"height": 0.8125,
			"seed": 370178264,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					50.71875,
					0.8125
				]
			]
		},
		{
			"type": "line",
			"version": 68,
			"versionNonce": 1490498520,
			"isDeleted": false,
			"id": "i8xir8xlgIyUFOLOCil7F",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -253.89453125,
			"y": 14.859375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 71.13671875,
			"height": 59.140625,
			"seed": 1290994600,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-46.96484375,
					-54.11328125
				],
				[
					-71.13671875,
					-59.140625
				]
			]
		},
		{
			"type": "arrow",
			"version": 477,
			"versionNonce": 689531982,
			"isDeleted": false,
			"id": "Q2TsHLMEO5CXfmMNL7HzQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -94.67499985727957,
			"y": -40.791285345508456,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 100.8064658857597,
			"height": 35.66978989635235,
			"seed": 2012407976,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695908804437,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					31.52447739635236
				],
				[
					53.02203438416924,
					-4.145312499999989
				],
				[
					100.8064658857597,
					31.246817570589506
				]
			]
		},
		{
			"type": "ellipse",
			"version": 229,
			"versionNonce": 1819474136,
			"isDeleted": false,
			"id": "jGUGgdFlR9tkFZAO9G3hz",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 5.778400391459845,
			"x": 188.86183579130775,
			"y": -142.7991069225321,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 17.241492028911136,
			"height": 139.81377936467626,
			"seed": 255716056,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "eGIPYyHVJ3TIUtcLygRjA",
					"type": "arrow"
				}
			],
			"updated": 1695671312113,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 95,
			"versionNonce": 49782952,
			"isDeleted": false,
			"id": "8fJhhcOzlJw6h1Fw3GMtX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 149.81656618076335,
			"y": -151.767217240194,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 17.42578125,
			"height": 21.09375,
			"seed": 1147078872,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 160,
			"versionNonce": 38673880,
			"isDeleted": false,
			"id": "UKSX6CoznWTp_vG2-raNP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 6.079811632081086,
			"x": 12.199378680763346,
			"y": -140.747685990194,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 139.4921875,
			"height": 22.59375,
			"seed": 261081560,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "eGIPYyHVJ3TIUtcLygRjA",
					"type": "arrow"
				}
			],
			"updated": 1695671312113,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 120,
			"versionNonce": 663571368,
			"isDeleted": false,
			"id": "LytLamMAgo2Q5TrE8Xk5i",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -22.244345747782106,
			"y": -124.82548561878201,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 38.20153692854545,
			"height": 21.34765625,
			"seed": 2015714264,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312113,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 117,
			"versionNonce": 1534894808,
			"isDeleted": false,
			"id": "EA6jdlFHTQkUyR8nLfdUc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -49.976849404307245,
			"y": -124.22406664564377,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 29.73046875,
			"height": 6.1015625,
			"seed": 1342672344,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312114,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 229,
			"versionNonce": 573937320,
			"isDeleted": false,
			"id": "tuSFiNamNSBTjhjcxqT5e",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -56.730730710926764,
			"y": -116.70885567765825,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 35.22228757103631,
			"height": 6.016900953311562,
			"seed": 1937751208,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312114,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 159,
			"versionNonce": 1989314520,
			"isDeleted": false,
			"id": "5YRyOjQMn4WBjFpCMx8wP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -49.270930276302124,
			"y": -110.88158544521477,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 29.73046875,
			"height": 6.1015625,
			"seed": 1895258792,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671312114,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 196,
			"versionNonce": 690125992,
			"isDeleted": false,
			"id": "GDUcdTz9iM75mozGq_7iq",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 5.55107132078436,
			"x": -24.319567634157806,
			"y": -99.35378801909935,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19.075830451268462,
			"height": 5.454121929763671,
			"seed": 1282252760,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671377864,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 371,
			"versionNonce": 1934651352,
			"isDeleted": false,
			"id": "eGIPYyHVJ3TIUtcLygRjA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 197.15648631986477,
			"y": -116.7431995017646,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 76.64821949437169,
			"height": 58.13960368753516,
			"seed": 1706882008,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671342657,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "jGUGgdFlR9tkFZAO9G3hz",
				"focus": 1.1555673109489315,
				"gap": 13.685811296543054
			},
			"endBinding": {
				"elementId": "UKSX6CoznWTp_vG2-raNP",
				"focus": -1.3000775829658398,
				"gap": 11.708962324101371
			},
			"lastCommittedPoint": null,
			"startArrowhead": "arrow",
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-21.964338378366108,
					-58.13960368753516
				],
				[
					-76.64821949437169,
					-41.7102785805173
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "dashed",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": "arrow",
		"currentItemEndArrowhead": null,
		"scrollX": 346.0668451137049,
		"scrollY": 415.74789211699454,
		"zoom": {
			"value": 1.25
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%