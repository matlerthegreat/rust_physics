---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "ellipse",
			"version": 77,
			"versionNonce": 50012617,
			"isDeleted": false,
			"id": "nDXXi8itWUOgKtfhiPePC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -429.0687040441176,
			"y": -145.93060661764707,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73.6484375,
			"height": 82.734375,
			"seed": 572377511,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695111809277,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 59,
			"versionNonce": 614858921,
			"isDeleted": false,
			"id": "mTZMS_24w7yVwlTvS1OQY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -376.8851102941176,
			"y": -184.63763786764707,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 99.2109375,
			"height": 96.57421875,
			"seed": 1946744201,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695111809277,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 49,
			"versionNonce": 1330774921,
			"isDeleted": false,
			"id": "zQETY3sy6wWLBHsoZ-BVL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -354.8382352941176,
			"y": -232.07513786764707,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 90.24609375,
			"height": 77.3515625,
			"seed": 1997145031,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695111809277,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 119,
			"versionNonce": 513341400,
			"isDeleted": false,
			"id": "bQ8k0HaWDTJ6yQtXbaFcH",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -188.94791666666669,
			"y": -130.76813616071422,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73.6484375,
			"height": 82.734375,
			"seed": 1374990535,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671562641,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 55,
			"versionNonce": 743748359,
			"isDeleted": false,
			"id": "jHJtOwYMuaFUzQI7-Htlw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -121.80078125,
			"y": -167.833984375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 99.2109375,
			"height": 96.57421875,
			"seed": 97583079,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695111733896,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 49,
			"versionNonce": 579289256,
			"isDeleted": false,
			"id": "tsODrFswFipq4jNWFzecA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -118.05394345238096,
			"y": -222.29045758928572,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 90.24609375,
			"height": 77.3515625,
			"seed": 328624903,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671579740,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 184,
			"versionNonce": 867080616,
			"isDeleted": false,
			"id": "OMFDzJAJcK_thS7-hmYK7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 74.70515471813712,
			"y": -132.28417148109241,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73.6484375,
			"height": 82.734375,
			"seed": 539132647,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671570964,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 183,
			"versionNonce": 1766802856,
			"isDeleted": false,
			"id": "4mPdSUJGiSitjeu_TFqY4",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 139.9304807860642,
			"y": -157.06705182072824,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 99.2109375,
			"height": 96.57421875,
			"seed": 247434759,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671596291,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 113,
			"versionNonce": 2054615256,
			"isDeleted": false,
			"id": "lgWvY5T9S97sh6VTYgQtA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 142.61444655987395,
			"y": -233.6767988445377,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 90.24609375,
			"height": 77.3515625,
			"seed": 936260903,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671599177,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 75,
			"versionNonce": 529601240,
			"isDeleted": false,
			"id": "DeJ3cQCRSBiTfIE1zBWqa",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -238.81485523897055,
			"y": -144.4111328125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 65.4097861702258,
			"height": 3.0955086240885805,
			"seed": 1563129161,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695671562640,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					65.4097861702258,
					-3.0955086240885805
				]
			]
		},
		{
			"type": "arrow",
			"version": 39,
			"versionNonce": 108085449,
			"isDeleted": false,
			"id": "iBC1w6U9DBByKGQil0Cwu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 3.9287109375,
			"y": -144.2043313419117,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 58.32720588235293,
			"height": 2.2748161764706083,
			"seed": 2126887303,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695111820726,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					58.32720588235293,
					-2.2748161764706083
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 459.6399821647409,
		"scrollY": 591.2198633359593,
		"zoom": {
			"value": 1.05
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%