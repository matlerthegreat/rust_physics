---
theme: solarized
highlightTheme: monokai
transition: none
showNotes: false

---

Talk is starting soon...

---

## Hello, world!

notes:
Hi, I'm Mateusz Resterny and I love learning all sorts of stuff! I went to drum lessons, I try to play ukulele. I'm getting better at dark souls and elden ring. I manage a home server and network and by the way, it's on Arch Linux.

Oh, also I like programming. Like a lot!
I started with functional programming with F#, but then transitioned to something more exciting. Game development at PixelAnt.

---

## Hello, world!

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
I got to see a lot of C++ code in the biggest and most wildly used engines and frameworks. And it was fine.

---

![[fine.jpg|700]]

<grid drag="40 10" drop="bottomleft">
###### Image: KC Green [[https://gunshowcomic.com/648]]
</grid>

notes:
I mean really it is fine. We're making games and having fun with it! But I still was looking back to that pure, clean functional style. And I think I've found it.

---

![[rust-logo-blk.svg|700]]

<grid drag="40 10" drop="bottomleft">
###### Image: Rust logo [[https://www.rust-lang.org/]]
</grid>

notes:
How many of you have ever tried writing in Rust?
And how many of you do gamedev in another language? Great, I will be making some comparisons to C++ but they should apply to most other languages.
Finally, how many of you actually like writing your code? I really do like it as well.I write C++ daily.  There are better and worse parts. But in my opinion,

---
# What `C++`
# can't ==borrow==
# from `Rust`

notes:
there are just some things that C++ can't borrow from Rust.

---

### `Rust` as a low-level language

- native execution
- no garbage collector
- manual memory management

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Let's start with what makes Rust similar to C++. Unlike C# or Java, Rust compiles to native code. It doesn't run in a VM, and has no garbage collector. All memory management is manual and fully controlled by the programmer.

---

### `Rust` as a high-level language

- functional style
- algebraic data types
- memory safety

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
On the other hand, Rust code feels much more like a high level language. Functions are the main building blocks of the program. The type system is rich and well designed. And Rust claims to be fully type safe and also memory safe. This means that all references must always be valid throughout the execution. No null pointers, no hanging references.

---

## Game physics engine

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
I decided to test Rust's claims on a real life example. Something more than just "hello world". And more gamedev oriented. So I started programming a game physics engine (how hard can that be?). And through the magic of coding two of them, I have a one to one comparison between Rust and C++. By the way: through this talk the physics engine will act just as an...

---

## Game physics engine

The problem generator

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
example. I do not expect you to have any knowledge of physics nor should you expect to learn exactly how to write a physics engine. However, I think that a basic overview will be helpful.


---

### Position integration

![[integration.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
When you think about a physics engine you probably picture the most obvious stuff, like the movement simulation. The engine is responsible for gathering all forces, it calculates the velocities and tries to estimate positions of bodies. We call this position integration.

---

### Collision detection

![[collision.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
There's also collision detection. It's usually split into two parts. A broad phase which is coarse and fast. It's job is to quickly discard those objects that definitely don't collide.
And there's the narrow phase that precisely calculates the exact collision but can be a lot slower.

---

### Constraint resolution

![[constraints.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The final stage is constraint resolution. Constraints specify how the bodies are supposed to be moving. Collisions are constraints as well as ropes or joints in bones. They all limit and dictate how bodies move.

---
### Position Based Dynamics

![[PBD.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
I chose the simplest but also very fitting to games solution which is Position Based Dynamics. The algorithm is to fix broken constraints one by one in a loop. The theory is that this approach eventually converges on a correct solution.
Let's see how a basic simulation loop could look like in code:

---

### `C++` Simulation

``` C++ []
while(running) {
	integratePositions(bodies);
	detectCollisions(bodies);
	resolveConstraints(bodies);
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Starting with traditional C++ implementation that should be self-explanatory. I am obviously skipping rendering steps, player input, etc. Just focusing on the physics engine.

---

### `Rust` Simulation

``` Rust []
while running {
	integrate_positions(bodies);
	detect_collisions(bodies);
	resolve_constraints(bodies);
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Let's now see how to do it in Rust. The syntax didn't change much. 
And here we have to make our first choice:

---

## How to pass arguments

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
That is, how to pass arguments to functions?

---

### `C++` By value

``` C++ []
void integratePositions(Bodies bodies);

Bodies bodies;
integratePositions(bodies); // implicit copy
detectCollisions(bodies); // positions NOT integrated!
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
We can send bodies by value, but that would just create an implicit copy. The next phase, collision detection will not get the updated positions. That's clearly not what we want.

---

### `C++` By pointer

``` C++ []
void integratePositions(Bodies* bodies);

Bodies bodies;
integratePositions(nullptr); // What if?
detectCollisions(&bodies);
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
We could use pointers. That allows for in place updating of bodies. A bit better, but we now we run the risk of the pointer being null. We don't want to clutter the code with unnecessary nullptr checks....

---

### `C++` By reference

``` C++ []
void integratePositions(Bodies& bodies);

Bodies bodies;
integratePositions(nullptr); // ERROR: Good!
detectCollisions(bodies);
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Simplest and safest solution as references cannot be null in C++.

---

### `Rust` Consume

``` Rust []
fn integrate_positions(bodies: Bodies) { /*...*/ }

integrate_positions(bodies);
detect_collisions(bodies);
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
In Rust parameters are CONSUMED by the function by default. The function is free to do anything with the consumed parameter

---

### `Rust` Consume

``` Rust []
fn integrate_positions(bodies: Bodies) { /*...*/ }

integrate_positions(bodies);
detect_collisions(bodies); // ERROR
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
 but the value cannot be used after the call. It's gone. But that's not helpful.

---

### `Rust` Borrow

``` Rust []
fn integrate_positions(bodies: &Bodies) { /*...*/ }

integrate_positions(&bodies); // borrow
detect_collisions(&bodies);
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
So, we have to borrow the value.  This is similar to a reference (they even share the same syntax) but Rust makes it clear on the caller side that the borrow occured;

---

### `Rust` Borrow

``` Rust []
fn integrate_positions(bodies: &mut Bodies) { /*...*/ }

integrate_positions(&bodies); // ERROR
detect_collisions(&bodies);
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
There is one more thing however. Everything in Rust is constant by default. To allow for mutations we have to explicitly state that.

---

### `Rust` Mutable borrow

``` Rust []
fn integrate_positions(bodies: &mut Bodies) { /*...*/ }

integrate_positions(&mut bodies);
detect_collisions(&bodies);
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
And the `mut` specifier has to be added everywhere, especially on the caller's side. So just by glancing we can already tell that detect_collisions will not mutate the collection of bodies.

---

### `Rust` is explicit but not verbose

- borrows
- mutations
- clones (expensive copies)
- error handling

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
So as you will hopefully see, Rust is explicit where it matters but not verbose. Everything that it deems unsafe has to be visible, that includes borrows and mutations. Clones are also explicit as they may involve expensive copying. There are no move semantics or copy constructors like in c++. You saw that arguments are consumed by functions. Errors in rust are reported by the return value. Exceptions are usually considered to be bugs (which is very fitting as we usually don't throw exceptions in games).

---

## Collision resolution

1. Find all pairs of bodies that collide.
2. Resolve each collision

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
As another example let's focus on collision resolution. The algorithm is as follows: find all pairs of bodies that collide - so we're only dealing with 2 bodies at a time. If there is a chain of collisions, we break it apart into individual pair-wise collisions.
2. Run some constraint resolution code for each collision found.

---

### `C++` Indexes

```C++ []
for (int i = 0; i < bodies.size(); ++i) {
	for (int j = i+1; j < bodies.size(); ++j) {
		if (bodies[i].collides(bodies[j])) {
			resolve(bodies[i], bodies[j]);
		}
	}
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
Again, let's start with simple C++ code. We first loop over each index in bodies. In the nested loop we should start with the next index, so we don't have to deal with duplicates or  a body colliding with itself. Inside, we check for collision and resolve it.
This code has similar vibe to the raw pointer example - we can do better.

---

### `C++` Iterators

```C++ []
for (auto a = bodies.begin(); a != bodies.end(); ++a) {
	for (auto b = std::next(a); b != bodies.end(); ++b) {
		if (a->collides(b)) {
			resolve(*a, *b);
		}
	}
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
C++ has iterators. They nicely abstract the raw indexes away and are usually the preferred option.

---

### Think like a machine

C++ reflects (almost) 1:1 how the CPU runs instructions

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
But the structure of code didn't change much. C++ reflects almost one-to-one how the CPU runs instructions. You have to think like a machine when writing code. This has a lot of value, especially in performance focused application, like games.

---

### `Rust` iterators

``` Rust []
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
Now, let's compare it to Rust collision resolution. Let's break down the code line-by-line

---

### `Rust` iterators

``` Rust [1,2]
bodies
	.iter() // All bodies
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
`bodies` is the collection that we want to iterate on. So we first create an iterator. Notice, that we don't have to deal with beginning or ending. The iterator has all required information.

---

### `Rust` iterators

```rust [3]
bodies
	.iter()
	.into_pairs() // All pairs of bodies
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
Next, we turn it into an iterator on all pairs of objects. This is called a iterator adapter. It takes one iterator and transforms it into another iterator
 
---

### `Rust` iterators

```rust [4]
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides) // All colliding pairs
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
we then use another adaptor: filter. It takes a function as a parameter. `Body::collides`checs for collision in a given pair. The result is yet another iterator that visits only the colliding pairs

---

### `Rust` iterators

```rust [5]
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
finally, we run the resolution code for each colliding pair. It needs mutable access to `bodies`  to actually update the collection.

---

### `Rust` iterators

```rust []
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Let's read the code again:

---

### Think like a human

Rust code reads (almost) like English

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Rust code reads almost like English. It's more natural to think in terms of the general flow and abstract implementation away. We've build up layer on top of layer of computation, in the direction of the data flow.
Unfortunately there is a problem...

---

### The code doesn't compile

```rust []
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

```bash
error[E0500]: closure requires unique access to bodies
but it is already borrowed
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The code doesn't compile. Let's read the compiler error: closure requires unique access to bodies but it is already borrowed.
OK, what does that mean?

---

### The borrow checker

Each variable can be borrowed ==either==:
- multiple times immutably
- one time mutably

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
It's the borrow checker enforcing it's rules, that is: each variable can be borrowed either multiple times immutably, or mutably but only one time.

---

### Borrow

```rust [1-4]
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
`bodies` are borrowed by all the iterators. Which is fine, they are borrowed immutably

---

### Mutable Borrow

```rust [5]
bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
But later `bodies` are borrowed again to resolve the collision and this time they are borrowed mutably. How do we solve that?

---

### Intermediate value

```rust [1,6-7]
let collisions =
	bodies
	.iter()
	.into_pairs()
	.filter(Body::collides);
	
collisions
	.iter()
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
We could introduce an intermediate value. Let's first gather all colliding pairs and then resolve them all at once. This code still doesn't compile.

---

### Iterators are lazy

Iterate only when requested

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

note:
That's because iterators in Rust are lazy. They iterate only on as many items as it's needed. What's more, iterators will do nothing unless requested. That's what's happening to us with the example. We just declared the intent of iteration, but didn't request any action. The iterator will hold the borrow and do nothing.

---

### Consume the iterator

```rust [6]
let collisions =
	bodies
	.iter()
	.into_pairs()
	.filter(Body::collides)
	.collect::<Vec<Collision>>();
	
collisions
	.into_iter()
	.for_each(|collision| resolve(collision, &mut bodies));
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
So, let's consume the iterator and store the colliding pairs in a vector. This frees the original borrow so it is safe to borrow mutably

---

### Lifetimes

![[Lifetimes.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Rust borrow checker operates on lifetimes. The borrows' lifetime will be extended until it's last use. The compiler will ensure that the borrow is always valid.
You also saw that the borrow on bodies was preserved through multiple iterators. The lifetime can span multiple function calls.

---

### Lifetimes

![[Lifetimes2.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
And because iterators are needed until the very last collision is resolved, without collecting them first, the lifetimes will overlap. And the borrow checker won't allow this.

---

## Multiple types of bodies

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Now, the physics engine would be exceptionally boring if it only supported a single type of physical body. Games are made up of a wide range of elements. There are characters running, cars driving and buildings collapsing. The physics engine has to be able to deal with them all.

---

### `C++` inheritance

``` C++ []
struct Body {
	virtual bool collides(Body* other) = 0;
};

struct Sphere: Body { /*...*/ };
struct Cube: Body { /*...*/ };
struct Car: Body { /*...*/ };

std::vector<Body*> bodies;
bodies.push_back(new Sphere());
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
In C++ we could use inheritance. Nice object oriented design. So here we a common interface, providing collision checks. Next, we have a sphere, a cube and a car, all inheriting the Body class. We can even store them all in a single container. 

---

### `C++` inheritance

``` C++ [2,10]
struct Body {
	virtual bool collides(Body* other) = 0; // Vtables!
};

struct Sphere: Body { /*...*/ };
struct Cube: Body { /*...*/ };
struct Car: Body { /*...*/ };

std::vector<Body*> bodies;
bodies.push_back(new Sphere()); // Memory fragmentation!
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
But that introduces virtual call tables. And storing bodies this way on the heap would lead to memory fragmentation. Both of which aren't particularly performance friendly.

---

### `C++` union

``` C++ []
struct Body {
	BodyKind kind;
	union {
		Sphere sphere;
		Cube cube;
		Car car;
	};
};
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
OK, so we could use unions instead. Now, the body class  has a kind, and storage for all variants. This approach doesn't suffer from performance issues.

---

### `C++` union

``` C++ []
Body body;
body.kind = BodyKind::Cube;
body.sphere.radius = 5.0f; // Ooops!
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
But it's much more error prone. I can just set a radius of a cube. What does that even mean?

---

### The `C++` compiler trusts you

You have to know\
(& remember)\
how to use the code

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The key takeaway from this is that the C++ compiler trusts you. It doesn't get in the way however it won't offer much help either. You have to know what you are doing. If you don't (or forgotten how to), the compiler often won't stop you from making mistakes.

---

### `Rust` enums

``` Rust []
struct SphereBody { /*...*/ }
struct CubeBody { /*...*/ }
struct CarBody { /*...*/ }
enum Body {
	Sphere(SphereBody),
	Cube(CubeBody),
	Car(CarBody),
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Rust tackles the problem polymorphism with algebraic types system. The enum in Rust is supercharged. It represents the logical "OR" of types. A `Body` can be either a `Sphere` or a `Cube` or a `Car`. The crucial part is that, this information is not stored as a variable's value but rather as it's type. This way, the compiler check for errors.  

---

### `Rust` match

``` Rust []
match body {
	Sphere(sphere) => sphere.radius,
	Cube(cube) => cube.radius,
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Handling of enums is done through a match statement. It's similar to C++ switch but again, supercharged.

---

### `Rust` match

``` Rust [2]
match body {
	Sphere(sphere) => sphere.radius, // Access to inner SphereBody
	Cube(cube) => cube.radius
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
It allows to deconstruct the inner structure of enum. Here we can access the radius of the sphere

---

### `Rust` match

``` Rust [3]
match body {
	Sphere(sphere) => sphere.radius,
	Cube(cube) => cube.radius, // ERROR: cube has no radius
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The compiler guards against mistakes, the cube has no radius

---

### `Rust` match

``` Rust [4]
match body {
	Sphere(sphere) => sphere.radius,
	Cube(cube) => cube.radius,
} // ERROR: missing Car
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
And also checks for missing cases

---

### `Rust` match

``` Rust []
match (a, b) {
	(Sphere(sphere), Cube(cube)) | (Cube(cube), Sphere(sphere))
		=> sphere_cube(sphere, cube),
		
	(Cube(cube), Car(car)) | (Car(car), Cube(cube))
		=> cube_car(cube, car)),
	/*...*/
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
It is even possible to match against more complex patterns. For example, all permutations of pairs of bodies in the narrow phase collision detection.

---

## Collision

![[Collision0.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Speaking of collisions, how do we define one? That's a surprisingly hard problem. We could try calculate the exact volume of intersection. But that can get very complex and isn't necessary.

---

## Collision

![[Collision1.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Easiest solution would be to just find the deepest penetration.

---

## Collision

![[Collision2.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
What we usually do in physics engines is to deconstruct this penetration into a collision normal vector and a depth scalar. With these values we can be certain that after moving bodies in the opposite direction along the vector by the depth distance, they will no longer collide.

---

### `C++` Collision resolution

``` C++[]
auto collision = a.collides(b);
if (collision.depth > 0.0) {
	Vector d = collision.normal * collision.depth;
	a->position += d / 2.0;
	b->position -= d / 2.0;
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Great, here is how it looks like in my C++ implementation. Here I calculate the collision between a and b, and if the depth is positive I just move bodies in the opposite direction. What if I forget to check for depth? Is the normal meaningful if there wasn't a collision? If not, what should it be? A zero vector as a normal?

---

### `Rust` Collision resolution

``` Rust[]
match a.collides(&b) {
	Some((normal, depth)) => {
		let d = collision.normal * collision.depth;
		a.position += d / 2.0;
		b.position -= d / 2.0;
	}
	None => ()
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
In Rust, situation is made clear. The normal and depth exist only in the inner structure of one of the enum variants. If there's no collision, the function returns a different type. This way, the compiler can enforce all static checks on our code.

---

### Encoding state in types

![[Option.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Rust heavily uses the idea of encoding state in variable type rather that it's value. We just saw it used for calculations that may or may not return a value.

---

### Encoding errors in types

![[Result.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
It is also the backbone of error handling. A function can either succeed with a solution or report an error.

---

### You can trust the Rust compiler

1. Borrows are always valid
2. Mutations are exclusive
3. Iterators are zero-cost
4. Types are statically checked

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The very particular architecture of Rust allows you to trust the compiler. It makes sure that borrows are always valid and mutations are exclusive. It promises to optimize iterators away so they are zero cost abstractions. And types which can also represent state are statically checked. The Rust compiler does all the boring bookkeeping, so the programmer can rest assured that the code is correct.

---

## Demo

Break to show a video of a physics simulation made with Bevy

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Both C++ and Rust implementations of the engine are compiled into static libraries. They share the same interface so it's trivial to switch between them in the runtime.

---

<video data-autoplay controls><source src="demo.mp4" type="video/mp4"></video>

---

## Getting deeper

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Let's get a bit deeper into the more juicy slices. I may not be explaining everything fully here, I just want you to get a taste for more advanced Rust features.

---

### Separating axis theorem

![[SAT1.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Let's start with a bit of theory: The separating axis theorem states (in simplified terms) that if you can put a piece of paper between two objects, they don't collide. Obviously.

---

### Separating axis theorem

![[SAT2.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
How computers actually compute that is by casting a shadow and checking whether the first one ends before the other begins.

---

### Sweep and prune

![[SAP1.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Now, what we can do is pick a couple of axes, say the primary axes, sort the shadows' ends on each axis and we have a nice broad phase algorithm. If the shadows don't overlap, the objects certainly don't collide either.

---

### Sweep and prune

``` Rust []
for axis in axes {
	let (min, max) = cast_shadow(body, axis);
	
	shadows[axis].push(Shadow::Start(min));
	shadows[axis].push(Shadow::Stop(max));
	
	shadows[axis].sort();
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
In rust code it would look something like this:
For each axis cast a shadow of a body, push the starting and stopping point into the array and sort them.

---

### Shadow casting

``` Rust []
pub fn cast_shadow(cube: &Cube, axis: Vector) -> (f32, f32) {
	cube
		.vertices
		.map(|v| v.project(axis))
		.min_max()
		.unwrap()
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The shadow casting function looks like this: for each vertex of the body and project it on a line. In return we get some numbers back, and all we have to do is to find the minimum and maximum values between them.
Ok, but the min_max function isn't defined in the standard library. But it would be really convenient to have it, as it would be used in multiple algorithms.

---

### Extension traits

``` Rust []
pub trait MinMaxExt: Iterator {
	fn min_max(mut self) -> Option<(Self::Item, Self::Item)>
	where
		Self::Item: Ord + Copy
	{	
		self.next().map(|first| {
			self.fold((first, first), |(prev_min, prev_max), v| {
				(min(prev_min, v), max(prev_max, v))
			})
		})
	}	
}

impl<I: Iterator> MinMaxExt for I {}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Well, in Rust you can extend existing types with new functionality. 

---

### Extension traits

``` Rust [1,3,4]
pub trait MinMaxExt: Iterator {
	fn min_max(mut self) -> Option<(Self::Item, Self::Item)>
	where
		Self::Item: Ord + Copy
	{	
		self.next().map(|first| {
			self.fold((first, first), |(prev_min, prev_max), v| {
				(min(prev_min, v), max(prev_max, v))
			})
		})
	}	
}

impl<I: Iterator> MinMaxExt for I {}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Here, we define a trait (which is like an interface) for Iterators that iterate on items that can be ordered and copied.

---

### Extension traits

``` Rust [2]
pub trait MinMaxExt: Iterator {
	fn min_max(mut self) -> Option<(Self::Item, Self::Item)>
	where
		Self::Item: Ord + Copy
	{	
		self.next().map(|first| {
			self.fold((first, first), |(prev_min, prev_max), v| {
				(min(prev_min, v), max(prev_max, v))
			})
		})
	}	
}

impl<I: Iterator> MinMaxExt for I {}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The trait has a method that consumes the original iterator and outputs a pair of items. It is wrapped in an Option type, because the iterator can be empty, we don't know that. In that case there's no item to return.

---

### Extension traits

``` Rust [6-10]
pub trait MinMaxExt: Iterator {
	fn min_max(mut self) -> Option<(Self::Item, Self::Item)>
	where
		Self::Item: Ord + Copy
	{	
		self.next().map(|first| {
			self.fold((first, first), |(prev_min, prev_max), v| {
				(min(prev_min, v), max(prev_max, v))
			})
		})
	}	
}

impl<I: Iterator> MinMaxExt for I {}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
We compute the values using standard min and max functions albeit in a nice functional matter.

---

### Extension traits

``` Rust [14]
pub trait MinMaxExt: Iterator {
	fn min_max(mut self) -> Option<(Self::Item, Self::Item)>
	where
		Self::Item: Ord + Copy
	{	
		self.next().map(|first| {
			self.fold((first, first), |(prev_min, prev_max), v| {
				(min(prev_min, v), max(prev_max, v))
			})
		})
	}	
}

impl<I: Iterator> MinMaxExt for I {}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
And here's an amazing part. We tell the compiler to implement our new trait for all iterators. No matter where they come from. They can be standard library iterators, our own custom ones or even they can come from a third party library.
Even better, Rust allows one library to define a trait, other library to define a type and they can work together, without knowing of each other's existence.
No ugly hooks nor callbacks like in C++ are necessary.

---

## Persistence

![[SAP2.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Now, the objects usually move only a little bit between frames. It would be wasteful to recalculate our broad phase structure each time from scratch. If we keep the sorted shadows and update them, only small portion of them will need to be moved around.

---

### The borrow checker

![[BorrowTimeline.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
However, we may run into a big problem. If you remember, the borrow checker ensures exclusive mutable borrows on our collection of bodies. Right now, access is nicely distributed into position integration, collision detection and constraint resolution across multiple frames.

---

### Persistent SAP

![[BorrowTimelineError.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
But for our improved Sweep and Prune, we want to store results for a long time.

---

### The `Proxy`

``` Rust []
let proxy = bodies.insert(body);
sap.insert(proxy);

// ...

let mut body = proxy.upgrade_mut(&mut bodies)?;
body.positon += displacement;
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Instead of using borrows, we can introduce a proxy system.

---

### The `Proxy`

``` Rust [1-3]
let proxy = bodies.insert(body);
sap.insert(proxy);
// store the proxy without borrowing bodies
// ...

let mut body = proxy.upgrade_mut(&mut bodies)?;
body.positon += displacement;
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The proxy acts as an index, a key to the value stored. We can safely store proxies for extended amount of time without holding a borrow on the entire collection. You may think that this approach works against the borrow checker. In my opinion, however, it woks with the borrow checker.

---

### The `Proxy`

``` Rust [5-7]
let proxy = bodies.insert(body);
sap.insert(proxy);

// ...
// Borrow again to access the value
let mut body = proxy.upgrade_mut(&mut bodies)?;
body.positon += displacement;
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
To access the entry you still have to borrow the whole collection. Here, we only limit how long we have to borrow it for. The validity of the proxy has to be checked at runtime. The question mark at the end of line 6 indicates that the method can fail, for example the body has been destroyed and can no longer be accessed.

---

### The `Arena<T>`

``` Rust []
// Credit: Catherine West
pub struct Arena<T> {
	entries: Vec<Entry<T>>,
	first_free: Option<usize>,
}

pub enum Entry<T> {
	Taken { generation: usize, value: T },
	Free { generation: usize, next_free: Option<usize> },
}

pub struct Proxy {
	generation: usize,
	index: usize,
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
To implement the proxy system, I borrowed the idea of a Generational Arena from  Catherine West who did a great talk about the ECS in Rust.

---

### The `Arena<T>`

``` Rust [3]
// Credit: Catherine West
pub struct Arena<T> {
	entries: Vec<Entry<T>>,
	first_free: Option<usize>,
}

pub enum Entry<T> {
	Taken { generation: usize, value: T },
	Free { generation: usize, next_free: Option<usize> },
}

pub struct Proxy {
	generation: usize,
	index: usize,
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The structure is composed of a dynamic array of entries.

---

### The `Arena<T>`

``` Rust [8]
// Credit: Catherine West
pub struct Arena<T> {
	entries: Vec<Entry<T>>,
	first_free: Option<usize>,
}

pub enum Entry<T> {
	Taken { generation: usize, value: T },
	Free { generation: usize, next_free: Option<usize> },
}

pub struct Proxy {
	generation: usize,
	index: usize,
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Each entry can be taken and contain a value.

---

### The `Arena<T>`

``` Rust [4,9]
// Credit: Catherine West
pub struct Arena<T> {
	entries: Vec<Entry<T>>,
	first_free: Option<usize>, // List head
}

pub enum Entry<T> {
	Taken { generation: usize, value: T },
	Free { generation: usize, next_free: Option<usize> },
}

pub struct Proxy {
	generation: usize,
	index: usize,
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Or be free and form a linked list of items to be reused.

---

### The `Arena<T>`

``` Rust [13]
// Credit: Catherine West
pub struct Arena<T> {
	entries: Vec<Entry<T>>,
	first_free: Option<usize>,
}

pub enum Entry<T> {
	Taken { generation: usize, value: T },
	Free { generation: usize, next_free: Option<usize> },
}

pub struct Proxy {
	generation: usize, // detect stale proxies
	index: usize,
}
```

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
To prevent access to reused entries, a generation counter is held in the proxy. Each time a new value is inserted, the generation count is incremented. If the generation counts don't match we have detected an stale Proxy.

---

### Composition

![[Composition.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
In Rust it's natural to build algorithms from smaller pieces. Thanks to traits it's trivial to write generic functions that accept wide spectrum of parameters. Iterators allow to extend their reach to entire collections. In C++ it's much more difficult because you either have to accept the unnecessary runtime cost of polymorphism or deal with template programming, which on bigger scale hell.
The min_max is generic and works for all iterators. Arena works for all types. They don't even know about the sweep and prune algorithm. But all the pieces fit together really nicely.

---

## Benchmarks

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Now, you may be thinking, what's the price of having all this luxury...

---

### Brute force

![[compareBruteForce.excalidraw.svg|1000]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
From my tests it turns out that rust is faster. At least using a brute force approach for the broad phase. I've found that the optimizer

---

### Hash grid

![[compareHashGrid.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

---

### Sweep and prune

![[compareSAP.excalidraw.svg]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

---

## Summary

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
The talk is nearing it's end and there's so much to rust that I had to skip.

---

### Batteries included

![[Pasted image 20230921091557.png]]

- dependency management `Cargo.toml`
- in-code documentation `cargo doc`
- unit tests `cargo tests`
- formatting `cargo fmt`
- linting `clippy`

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Rust comes preloded with a whole ecosystem in the form of the Cargo toolchain. It has proper dependency management, dcocumentation, unit testing, formatting and linting! All build in and ready to be used.

---

### Rust is everywhere

- Firefox
- Linux!
- JetBrains RustRover

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
What's more Rust is gaining popularity fast. It is already everywhere, Firefox is probably the most well known project that incorporated Rust. It is also the only language besides C and assembler to be accepted into the Linux kernel. So Rust is now running the clouds (in small parts at least). Finally JetBrains recently announced a Rust exclusive IDE.

---

### arewegameyet.rs ?
![[Screen Shot 2023-10-04 at 22.05.29.png]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
But are we game yet? Dot rs - it's a website that exists and tracks statistics. It turns out we are in a peculiar spot as there are over 40 Rust based game engines.

---

### arewegameyet.rs ?

![[Screen Shot 2023-10-04 at 22.05.48.png]]

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
And 12 games released... I don't know what's up with that. Maybe we should just 

---

### Start using Rust

- C(++)/Rust integration
- Helper tools
- Game jam

<grid drag="30 20" drop="bottomright">
![[PixelAnt_Horizontal_Black.svg]]
</grid>

notes:
Start using Rust! If writing your next AAA game is out of question maybe just a small module thanks to the Rust-C compatibility? Godot has native bindings now that Rust can seamlessly interface with. Or just smaller tools could be rewritten in Rust. Or you could try it at the next game jam! Rust is perfectly fine for fast iterations and prototyping. Here's a proof:

---

### Win a game jam!

![[winner.jpg|700]]

notes:
That's my award for winning a game jam at PixelAnt. And the best part is that the game didn't crash once. Rapid iteration and prototyping is much easier when you don't have to deal with runtime errors. And thanks to strict Rust compiler they can be mostly eliminated.

---

# What `C++` 
# can ==borrow== 
# from `Rust`

notes:
In the end, the practices I've shown you aren't revolutionary. Of course you can implement them all in your favorite language of choice. But what are good practices in other languages, Rust makes mandatory.

---

# What `C++` 
# can't ==borrow== 
# from `Rust`

notes:
And that's really what C++ can't borrow from Rust.

---

# Thank you!