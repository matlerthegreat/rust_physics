---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
iter ^bzYCRKU6

into_pairs ^bAGaCW6U

filter ^mqbZ6aEV

collect ^tOMRlhZy

resolve ^hD29Xcxy

&bodies ^PYs5bUNo

&mut bodies ^fB3yeWtF

bodies ^6rzONMaJ

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "rectangle",
			"version": 179,
			"versionNonce": 1003733545,
			"isDeleted": false,
			"id": "uGqZEZWl1-XwKL9gMm4dP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -239.3359375,
			"y": -223.3124745852123,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 590.0008932331165,
			"height": 23.143968534697954,
			"seed": 1144192903,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1695128457809,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 265,
			"versionNonce": 645796007,
			"isDeleted": false,
			"id": "FYL1TTSeQe_w8E-ZXDs_0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -222.08436314323896,
			"y": -190.10246096800864,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73,
			"height": 35,
			"seed": 797769415,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "bzYCRKU6"
				}
			],
			"updated": 1695128138376,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 5,
			"versionNonce": 894885319,
			"isDeleted": false,
			"id": "bzYCRKU6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -203.19434086540693,
			"y": -185.10246096800864,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 35.21995544433594,
			"height": 25,
			"seed": 1789626601,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128074937,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "iter",
			"rawText": "iter",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "FYL1TTSeQe_w8E-ZXDs_0",
			"originalText": "iter",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 479,
			"versionNonce": 1906383431,
			"isDeleted": false,
			"id": "nlSLgx9YizFm1PegLosVu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -161.25145493759752,
			"y": -130.47584301371455,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 121,
			"height": 35,
			"seed": 1615163369,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "SZLZ9T1cdWZ6m8cjUITDm",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "bAGaCW6U"
				},
				{
					"id": "VMSaQ_F0OlF82Ht1BvZNn",
					"type": "arrow"
				}
			],
			"updated": 1695128283650,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 349,
			"versionNonce": 389298535,
			"isDeleted": false,
			"id": "bAGaCW6U",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -150.58139573349595,
			"y": -125.47584301371455,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 99.65988159179688,
			"height": 25,
			"seed": 695047017,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128283650,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "into_pairs",
			"rawText": "into_pairs",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "nlSLgx9YizFm1PegLosVu",
			"originalText": "into_pairs",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 498,
			"versionNonce": 755575879,
			"isDeleted": false,
			"id": "ApYy-bNGYSjwnGJokUj5C",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -52.693322142666574,
			"y": -73.85587708050758,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 112,
			"height": 35,
			"seed": 164006889,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "SZLZ9T1cdWZ6m8cjUITDm",
					"type": "arrow"
				},
				{
					"id": "OLCiexoeHVOiwQG3GZyVy",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "mqbZ6aEV"
				}
			],
			"updated": 1695128341929,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 413,
			"versionNonce": 264899431,
			"isDeleted": false,
			"id": "mqbZ6aEV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -21.793290099209543,
			"y": -68.85587708050758,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 50.19993591308594,
			"height": 25,
			"seed": 1817185321,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128341929,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "filter",
			"rawText": "filter",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ApYy-bNGYSjwnGJokUj5C",
			"originalText": "filter",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "diamond",
			"version": 590,
			"versionNonce": 832912233,
			"isDeleted": false,
			"id": "z6qaKMcMX6ZhbNrd7P8Fv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 4.558925759554256,
			"y": -34.62762743622035,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 151,
			"height": 70,
			"seed": 1853168937,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "tOMRlhZy"
				}
			],
			"updated": 1695128338251,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 517,
			"versionNonce": 685432905,
			"isDeleted": false,
			"id": "tOMRlhZy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 48.348957192659725,
			"y": -12.12762743622035,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 63.91993713378906,
			"height": 25,
			"seed": 451282121,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128333684,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "collect",
			"rawText": "collect",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "z6qaKMcMX6ZhbNrd7P8Fv",
			"originalText": "collect",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 296,
			"versionNonce": 1934906729,
			"isDeleted": false,
			"id": "SIseSqjz7Q8RbcQeBs8FB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 151.23202895501373,
			"y": -5.614378734220594,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 2.595400702206348,
			"height": 214.64407562170314,
			"seed": 1984686025,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695128377873,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-2.595400702206348,
					-214.64407562170314
				]
			]
		},
		{
			"type": "line",
			"version": 57,
			"versionNonce": 1182978377,
			"isDeleted": false,
			"id": "mdcWtaFaNip-kxQYnEj82",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -222.17322657534052,
			"y": -224.74006717712848,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 2.0638515753405215,
			"height": 49.75569217712848,
			"seed": 258940873,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695128256050,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					2.0638515753405215,
					49.75569217712848
				]
			]
		},
		{
			"type": "line",
			"version": 33,
			"versionNonce": 1427382195,
			"isDeleted": false,
			"id": "3wBteKrhk7bNt0Bq7r98p",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -219.19985414386065,
			"y": -202.63093710778185,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 19.292361216034436,
			"height": 20.47168690810628,
			"seed": 599609417,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					19.292361216034436,
					-20.47168690810628
				]
			]
		},
		{
			"type": "line",
			"version": 37,
			"versionNonce": 1300572413,
			"isDeleted": false,
			"id": "leyjLoHM-qiapRblX12sB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -198.71201871860768,
			"y": -202.07113432334134,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 18.940932172218652,
			"height": 19.49958754334753,
			"seed": 1528920071,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					18.940932172218652,
					-19.49958754334753
				]
			]
		},
		{
			"type": "line",
			"version": 28,
			"versionNonce": 393982291,
			"isDeleted": false,
			"id": "48SH8CYZIg07KycO3Asjk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -168.38172961417212,
			"y": -200.44561331091916,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 15.583616943417297,
			"height": 19.915937244245555,
			"seed": 835877673,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					15.583616943417297,
					-19.915937244245555
				]
			]
		},
		{
			"type": "line",
			"version": 31,
			"versionNonce": 1951465821,
			"isDeleted": false,
			"id": "8VcIW8fkD5lm7u1fkt7Kk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -140.5545368960342,
			"y": -201.7167036686589,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 14.018493476430763,
			"height": 19.22478647639585,
			"seed": 825155719,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					14.018493476430763,
					-19.22478647639585
				]
			]
		},
		{
			"type": "line",
			"version": 16,
			"versionNonce": 1983909619,
			"isDeleted": false,
			"id": "PSqWgbhBWzAgGtcU6kix7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -123.98023234494518,
			"y": -202.46015558244386,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 16.441786944394806,
			"height": 17.61787372982468,
			"seed": 129055401,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					16.441786944394806,
					-17.61787372982468
				]
			]
		},
		{
			"type": "line",
			"version": 29,
			"versionNonce": 415419837,
			"isDeleted": false,
			"id": "ooqloCfTcxD3vRIm92aJW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -104.37774194159384,
			"y": -201.3080175309097,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 16.960167133394535,
			"height": 22.56536875335695,
			"seed": 1537213575,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					16.960167133394535,
					-22.56536875335695
				]
			]
		},
		{
			"type": "arrow",
			"version": 1389,
			"versionNonce": 190694611,
			"isDeleted": false,
			"id": "VMSaQ_F0OlF82Ht1BvZNn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.18224737483894,
			"y": -157.25844109552787,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 4.910896196037811,
			"height": 20.602323870405485,
			"seed": 1801217545,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712172367,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "nlSLgx9YizFm1PegLosVu",
				"gap": 6.180274211407834,
				"focus": -0.632046257626877
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					4.910896196037811,
					20.602323870405485
				]
			]
		},
		{
			"type": "arrow",
			"version": 1835,
			"versionNonce": 905636883,
			"isDeleted": false,
			"id": "SZLZ9T1cdWZ6m8cjUITDm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -50.3039085917485,
			"y": -93.38046180899347,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5.783134497827447,
			"height": 13.394267513911558,
			"seed": 136827879,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712172374,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "nlSLgx9YizFm1PegLosVu",
				"gap": 2.095381204721079,
				"focus": -0.616989832117844
			},
			"endBinding": {
				"elementId": "ApYy-bNGYSjwnGJokUj5C",
				"gap": 6.130317214574333,
				"focus": -0.5919957161725654
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					5.783134497827447,
					13.394267513911558
				]
			]
		},
		{
			"type": "arrow",
			"version": 1676,
			"versionNonce": 1586098611,
			"isDeleted": false,
			"id": "OLCiexoeHVOiwQG3GZyVy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 9.284687533174838,
			"y": -37.13451386909189,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 22.924057621149863,
			"height": 27.513938846207896,
			"seed": 75717705,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712172377,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "ApYy-bNGYSjwnGJokUj5C",
				"gap": 1.7213632114156994,
				"focus": 0.14220387752306127
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					22.924057621149863,
					27.513938846207896
				]
			]
		},
		{
			"type": "line",
			"version": 8,
			"versionNonce": 1676130451,
			"isDeleted": false,
			"id": "g3po1TPvyuAjejsNalgFt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 19.490035178292146,
			"y": -203.10925466419172,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 8.477757920051943,
			"height": 17.72697252441145,
			"seed": 2063949127,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					8.477757920051943,
					-17.72697252441145
				]
			]
		},
		{
			"type": "line",
			"version": 23,
			"versionNonce": 2046561821,
			"isDeleted": false,
			"id": "n_WpsmqbMmRQ7y7o9iHT3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 35.326938826149956,
			"y": -203.0651714250884,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 13.296606994530634,
			"height": 15.83139324296991,
			"seed": 1088711657,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					13.296606994530634,
					-15.83139324296991
				]
			]
		},
		{
			"type": "rectangle",
			"version": 194,
			"versionNonce": 1151430089,
			"isDeleted": false,
			"id": "VPgUtNhwXScbMgq8hiDm_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 187.44607089428064,
			"y": 40.318211212173935,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 146,
			"height": 35,
			"seed": 114802761,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "hD29Xcxy"
				}
			],
			"updated": 1695129063545,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 113,
			"versionNonce": 1940049065,
			"isDeleted": false,
			"id": "hD29Xcxy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 226.37610934642908,
			"y": 45.318211212173935,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 68.13992309570312,
			"height": 25,
			"seed": 658960489,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695129063545,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "resolve",
			"rawText": "resolve",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "VPgUtNhwXScbMgq8hiDm_",
			"originalText": "resolve",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 83,
			"versionNonce": 1222583623,
			"isDeleted": false,
			"id": "harOCBlSYkeQmae0IIFmA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 187.85514661763216,
			"y": 54.04750521518213,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 3.1411312802000566,
			"height": 278.4379435564886,
			"seed": 1134148329,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695129065390,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-3.1411312802000566,
					-278.4379435564886
				]
			]
		},
		{
			"type": "line",
			"version": 108,
			"versionNonce": 1876012167,
			"isDeleted": false,
			"id": "jWmnoHNi7u6w7b-FFVVxV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 336.9183334049892,
			"y": 50.50185269798578,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5.442949485364693,
			"height": 275.2890401912219,
			"seed": 1595956391,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695129068723,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-5.442949485364693,
					-275.2890401912219
				]
			]
		},
		{
			"type": "line",
			"version": 9,
			"versionNonce": 1042886195,
			"isDeleted": false,
			"id": "29FeuKHAS3pAubUgkST7U",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 54.618866338728196,
			"y": -205.56138483931272,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 15.059936558662287,
			"height": 16.181303953352284,
			"seed": 1230238441,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					15.059936558662287,
					-16.181303953352284
				]
			]
		},
		{
			"type": "line",
			"version": 10,
			"versionNonce": 1995326077,
			"isDeleted": false,
			"id": "R0UUYveY2Xk57JowTDr0K",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 80.61144619500703,
			"y": -203.7732584531854,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 12.662910432420745,
			"height": 15.850679660077589,
			"seed": 1926581767,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					12.662910432420745,
					-15.850679660077589
				]
			]
		},
		{
			"type": "line",
			"version": 15,
			"versionNonce": 1743450067,
			"isDeleted": false,
			"id": "YBhEycFBDI9VKpD-ujnVk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 103.00848686192353,
			"y": -200.2218025029264,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 21.179241186688046,
			"height": 20.67779434188813,
			"seed": 1780252905,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					21.179241186688046,
					-20.67779434188813
				]
			]
		},
		{
			"type": "line",
			"version": 12,
			"versionNonce": 341502685,
			"isDeleted": false,
			"id": "W9AKw8d4N62mh9FFm3Nwu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 128.83575457156496,
			"y": -202.09534016481635,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 12.764852922847126,
			"height": 15.56413860590618,
			"seed": 158658247,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					12.764852922847126,
					-15.56413860590618
				]
			]
		},
		{
			"type": "text",
			"version": 52,
			"versionNonce": 1067041671,
			"isDeleted": false,
			"id": "PYs5bUNo",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -77.03021683836681,
			"y": -222.39567177188255,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 73.17991638183594,
			"height": 25,
			"seed": 105698793,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128506502,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&bodies",
			"rawText": "&bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&bodies",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 57,
			"versionNonce": 1845575,
			"isDeleted": false,
			"id": "fB3yeWtF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 200.8484808492371,
			"y": -221.92453215396623,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 118.49986267089844,
			"height": 25,
			"seed": 177830281,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128496216,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut bodies",
			"rawText": "&mut bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut bodies",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 25,
			"versionNonce": 1526907401,
			"isDeleted": false,
			"id": "6rzONMaJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -308.9609281091387,
			"y": -224.81792814167778,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 58.7999267578125,
			"height": 25,
			"seed": 787065287,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128480293,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "bodies",
			"rawText": "bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "bodies",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 328.69344073346565,
		"scrollY": 501.3292002399064,
		"zoom": {
			"value": 1.1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%