---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Rust ^gGGdEpnp

C++ ^p7S5af9d

broadphase time (lower is better) ^6xxuvRHK

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "line",
			"version": 265,
			"versionNonce": 284613586,
			"isDeleted": false,
			"id": "f8_KkP0vAG_XoSTqet5bg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -538.7172571914584,
			"y": 176.59010577165802,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 683.7737156240881,
			"height": 0,
			"seed": 195462862,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695915477523,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					683.7737156240881,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 127,
			"versionNonce": 53297230,
			"isDeleted": false,
			"id": "gGGdEpnp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.874820892634,
			"y": -132.23524621690868,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 65.91227722167969,
			"height": 35,
			"seed": 2055122254,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915486946,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Rust",
			"rawText": "Rust",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Rust",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "text",
			"version": 78,
			"versionNonce": 1947688850,
			"isDeleted": false,
			"id": "p7S5af9d",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.2467498469856,
			"y": -101.7614428283194,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 53.0322265625,
			"height": 35,
			"seed": 548624910,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915483378,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "C++",
			"rawText": "C++",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C++",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 2091055886,
			"isDeleted": false,
			"id": "5IysNpStefL3MVv79sj5o",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -537.6694552503087,
			"y": 153.0049330054169,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 368380946,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923763640,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 717821266,
			"isDeleted": false,
			"id": "Gm8FFd3aCrNq4xS1TtXwL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -423.4075309082035,
			"y": 117.97615011068001,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 2094198098,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923761214,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1670314322,
			"isDeleted": false,
			"id": "fBG2yns3GyrTFxz86ADnD",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -235.157119723993,
			"y": 3.8026303738379283,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1270175378,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923758451,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1926032210,
			"isDeleted": false,
			"id": "Kb0aHzpOKOupS9ArDgU0F",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -46.03910985557195,
			"y": -74.49712291563574,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1065961426,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923756017,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1275932622,
			"isDeleted": false,
			"id": "WWsGE5Aag6iVAarR8ywNS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 144.1335875128491,
			"y": -164.82401436300415,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1079755026,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923753687,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 142,
			"versionNonce": 131144398,
			"isDeleted": false,
			"id": "AWEytbb9DF_pWj8P2hVfy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -535.2455243292561,
			"y": 152.8618409001537,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 679.3893914473683,
			"height": 318.67804276315786,
			"seed": 895074898,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923731569,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					111.84827302631578,
					-35.02467105263156
				],
				[
					299.75123355263156,
					-150.0575657894737
				],
				[
					487.3499177631579,
					-228.22162828947367
				],
				[
					679.3893914473683,
					-318.67804276315786
				]
			]
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1974675474,
			"isDeleted": false,
			"id": "knQlrAFWj-FeLNrOJq2Yt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -536.8183039345192,
			"y": 165.12047576857475,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1158193294,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923739027,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1681561426,
			"isDeleted": false,
			"id": "uyuxb_kbdFREqsCyRaq98",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -424.3697019608351,
			"y": 137.5197356369958,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 827753294,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923741295,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 1659722638,
			"isDeleted": false,
			"id": "PNVoop_8ZckQDTtMrWiS1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -234.8137809082035,
			"y": 43.32565668962741,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 1652973070,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923745418,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 279501970,
			"isDeleted": false,
			"id": "mYi1zyDnXjqs_vyqNLc3B",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -46.168632881887675,
			"y": -20.44490252089892,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 121319630,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923748675,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 4,
			"versionNonce": 494250642,
			"isDeleted": false,
			"id": "VDD6LNYQIXmtQGxKST0lL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 142.9329296181122,
			"y": -107.1369255472147,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 217578382,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923751231,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 122,
			"versionNonce": 1491550030,
			"isDeleted": false,
			"id": "LC6WY4Yi0-JD7Tr6dWW3m",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -537.4947019608351,
			"y": 163.5805909001537,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 679.6587171052631,
			"height": 270.8264802631579,
			"seed": 203406926,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923733660,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					113.51151315789474,
					-26.4453125
				],
				[
					302.57401315789474,
					-120.48519736842104
				],
				[
					491.9140625,
					-184.9424342105263
				],
				[
					679.6587171052631,
					-270.8264802631579
				]
			]
		},
		{
			"id": "6xxuvRHK",
			"type": "text",
			"x": -337.58289796900374,
			"y": 182.17549081623298,
			"width": 330.319580078125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 975034446,
			"version": 100,
			"versionNonce": 1845272014,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695923898546,
			"link": null,
			"locked": false,
			"text": "broadphase time (lower is better)",
			"rawText": "broadphase time (lower is better)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "broadphase time (lower is better)",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 659.8882094807904,
		"scrollY": 554.7537370185753,
		"zoom": {
			"value": 0.792865539029611
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%