---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
m*g ^A4PciLyt

V ^zBlomCLz

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "ellipse",
			"version": 115,
			"versionNonce": 1672460947,
			"isDeleted": false,
			"id": "m1eSqrJBaHdxYEEdTVG_4",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -221.0859375,
			"y": -184.6171875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 219.75,
			"height": 222.046875,
			"seed": 1864514675,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695709540221,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 227,
			"versionNonce": 232078259,
			"isDeleted": false,
			"id": "lDV4fOde_WgXdE72iWK3N",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -81.359375,
			"y": -148.890625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 219.75,
			"height": 222.046875,
			"seed": 682782973,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "MWSXEQnyFnml1XYGdWvKi",
					"type": "arrow"
				},
				{
					"id": "31aAIuDEA4vvbgjIlu-Oj",
					"type": "arrow"
				}
			],
			"updated": 1695709624665,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 87,
			"versionNonce": 125852861,
			"isDeleted": false,
			"id": "i-V246GcEtsfo9eOBvKVr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -120.09765625,
			"y": -77.6796875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 144.0078125,
			"height": 49.6484375,
			"seed": 1363910781,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695709551758,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					76.859375,
					11.62109375
				],
				[
					144.0078125,
					49.6484375
				]
			]
		},
		{
			"type": "arrow",
			"version": 50,
			"versionNonce": 1601554493,
			"isDeleted": false,
			"id": "MWSXEQnyFnml1XYGdWvKi",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 25.6015625,
			"y": 75.08984375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 1.01953125,
			"height": 72.3359375,
			"seed": 1121262899,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695709572196,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "lDV4fOde_WgXdE72iWK3N",
				"focus": 0.012030648244467996,
				"gap": 1.971951493822914
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-1.01953125,
					72.3359375
				]
			]
		},
		{
			"type": "text",
			"version": 26,
			"versionNonce": 740951261,
			"isDeleted": false,
			"id": "A4PciLyt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 39.15234375,
			"y": 85.875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.4000244140625,
			"height": 45,
			"seed": 359381149,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695709605971,
			"link": null,
			"locked": false,
			"fontSize": 36,
			"fontFamily": 1,
			"text": "m*g",
			"rawText": "m*g",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "m*g",
			"lineHeight": 1.25,
			"baseline": 32
		},
		{
			"type": "arrow",
			"version": 105,
			"versionNonce": 1869051411,
			"isDeleted": false,
			"id": "31aAIuDEA4vvbgjIlu-Oj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 140.24609375,
			"y": -39.08984375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 87.94140625,
			"height": 1.6015625,
			"seed": 1393176989,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695709624665,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "lDV4fOde_WgXdE72iWK3N",
				"focus": 0.00731389388654269,
				"gap": 1.8620228309851257
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					87.94140625,
					-1.6015625
				]
			]
		},
		{
			"type": "text",
			"version": 6,
			"versionNonce": 888613693,
			"isDeleted": false,
			"id": "zBlomCLz",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 158.6953125,
			"y": -83.64453125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 18.61199951171875,
			"height": 45,
			"seed": 1371098451,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695709634724,
			"link": null,
			"locked": false,
			"fontSize": 36,
			"fontFamily": 1,
			"text": "V",
			"rawText": "V",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "V",
			"lineHeight": 1.25,
			"baseline": 32
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 36,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 235.9963702336792,
		"scrollY": 311.4093878639862,
		"zoom": {
			"value": 1.6
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%