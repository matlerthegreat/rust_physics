---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
iter ^bzYCRKU6

into_pairs ^bAGaCW6U

filter ^mqbZ6aEV

resolve ^hD29Xcxy

&bodies ^PYs5bUNo

&mut bodies ^fB3yeWtF

bodies ^6rzONMaJ

for_each ^n19SLl4c

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "rectangle",
			"version": 179,
			"versionNonce": 1003733545,
			"isDeleted": false,
			"id": "uGqZEZWl1-XwKL9gMm4dP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -239.3359375,
			"y": -223.3124745852123,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 590.0008932331165,
			"height": 23.143968534697954,
			"seed": 1144192903,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1695128457809,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 265,
			"versionNonce": 645796007,
			"isDeleted": false,
			"id": "FYL1TTSeQe_w8E-ZXDs_0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -222.08436314323896,
			"y": -190.10246096800864,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73,
			"height": 35,
			"seed": 797769415,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "bzYCRKU6"
				}
			],
			"updated": 1695128138376,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 5,
			"versionNonce": 894885319,
			"isDeleted": false,
			"id": "bzYCRKU6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -203.19434086540693,
			"y": -185.10246096800864,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 35.21995544433594,
			"height": 25,
			"seed": 1789626601,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128074937,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "iter",
			"rawText": "iter",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "FYL1TTSeQe_w8E-ZXDs_0",
			"originalText": "iter",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 479,
			"versionNonce": 1906383431,
			"isDeleted": false,
			"id": "nlSLgx9YizFm1PegLosVu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -161.25145493759752,
			"y": -130.47584301371455,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 121,
			"height": 35,
			"seed": 1615163369,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "SZLZ9T1cdWZ6m8cjUITDm",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "bAGaCW6U"
				},
				{
					"id": "VMSaQ_F0OlF82Ht1BvZNn",
					"type": "arrow"
				}
			],
			"updated": 1695128283650,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 349,
			"versionNonce": 389298535,
			"isDeleted": false,
			"id": "bAGaCW6U",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -150.58139573349595,
			"y": -125.47584301371455,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 99.65988159179688,
			"height": 25,
			"seed": 695047017,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128283650,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "into_pairs",
			"rawText": "into_pairs",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "nlSLgx9YizFm1PegLosVu",
			"originalText": "into_pairs",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 498,
			"versionNonce": 755575879,
			"isDeleted": false,
			"id": "ApYy-bNGYSjwnGJokUj5C",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -52.693322142666574,
			"y": -73.85587708050758,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 112,
			"height": 35,
			"seed": 164006889,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "SZLZ9T1cdWZ6m8cjUITDm",
					"type": "arrow"
				},
				{
					"id": "OLCiexoeHVOiwQG3GZyVy",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "mqbZ6aEV"
				}
			],
			"updated": 1695128341929,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 413,
			"versionNonce": 264899431,
			"isDeleted": false,
			"id": "mqbZ6aEV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -21.793290099209543,
			"y": -68.85587708050758,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 50.19993591308594,
			"height": 25,
			"seed": 1817185321,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128341929,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "filter",
			"rawText": "filter",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ApYy-bNGYSjwnGJokUj5C",
			"originalText": "filter",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 57,
			"versionNonce": 1182978377,
			"isDeleted": false,
			"id": "mdcWtaFaNip-kxQYnEj82",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -222.17322657534052,
			"y": -224.74006717712848,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 2.0638515753405215,
			"height": 49.75569217712848,
			"seed": 258940873,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695128256050,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					2.0638515753405215,
					49.75569217712848
				]
			]
		},
		{
			"type": "line",
			"version": 33,
			"versionNonce": 1427382195,
			"isDeleted": false,
			"id": "3wBteKrhk7bNt0Bq7r98p",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -219.19985414386065,
			"y": -202.63093710778185,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 19.292361216034436,
			"height": 20.47168690810628,
			"seed": 599609417,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					19.292361216034436,
					-20.47168690810628
				]
			]
		},
		{
			"type": "line",
			"version": 37,
			"versionNonce": 1300572413,
			"isDeleted": false,
			"id": "leyjLoHM-qiapRblX12sB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -198.71201871860768,
			"y": -202.07113432334134,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 18.940932172218652,
			"height": 19.49958754334753,
			"seed": 1528920071,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					18.940932172218652,
					-19.49958754334753
				]
			]
		},
		{
			"type": "line",
			"version": 28,
			"versionNonce": 393982291,
			"isDeleted": false,
			"id": "48SH8CYZIg07KycO3Asjk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -168.38172961417212,
			"y": -200.44561331091916,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 15.583616943417297,
			"height": 19.915937244245555,
			"seed": 835877673,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					15.583616943417297,
					-19.915937244245555
				]
			]
		},
		{
			"type": "line",
			"version": 31,
			"versionNonce": 1951465821,
			"isDeleted": false,
			"id": "8VcIW8fkD5lm7u1fkt7Kk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -140.5545368960342,
			"y": -201.7167036686589,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 14.018493476430763,
			"height": 19.22478647639585,
			"seed": 825155719,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					14.018493476430763,
					-19.22478647639585
				]
			]
		},
		{
			"type": "line",
			"version": 16,
			"versionNonce": 1983909619,
			"isDeleted": false,
			"id": "PSqWgbhBWzAgGtcU6kix7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -123.98023234494518,
			"y": -202.46015558244386,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 16.441786944394806,
			"height": 17.61787372982468,
			"seed": 129055401,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					16.441786944394806,
					-17.61787372982468
				]
			]
		},
		{
			"type": "line",
			"version": 29,
			"versionNonce": 415419837,
			"isDeleted": false,
			"id": "ooqloCfTcxD3vRIm92aJW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -104.37774194159384,
			"y": -201.3080175309097,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 16.960167133394535,
			"height": 22.56536875335695,
			"seed": 1537213575,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					16.960167133394535,
					-22.56536875335695
				]
			]
		},
		{
			"type": "arrow",
			"version": 1391,
			"versionNonce": 33722642,
			"isDeleted": false,
			"id": "VMSaQ_F0OlF82Ht1BvZNn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.18224737483894,
			"y": -157.25844109552787,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 4.910896196037811,
			"height": 20.602323870405485,
			"seed": 1801217545,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695903642713,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "nlSLgx9YizFm1PegLosVu",
				"gap": 6.180274211407834,
				"focus": -0.632046257626877
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					4.910896196037811,
					20.602323870405485
				]
			]
		},
		{
			"type": "arrow",
			"version": 1843,
			"versionNonce": 186315922,
			"isDeleted": false,
			"id": "SZLZ9T1cdWZ6m8cjUITDm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -50.30233613509969,
			"y": -93.38046180899347,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5.7826900085816035,
			"height": 13.394267513911558,
			"seed": 136827879,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695903642718,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "nlSLgx9YizFm1PegLosVu",
				"gap": 2.095381204721079,
				"focus": -0.616989832117844
			},
			"endBinding": {
				"elementId": "ApYy-bNGYSjwnGJokUj5C",
				"gap": 6.130317214574333,
				"focus": -0.5919957161725654
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					5.7826900085816035,
					13.394267513911558
				]
			]
		},
		{
			"type": "arrow",
			"version": 1947,
			"versionNonce": 2044772366,
			"isDeleted": false,
			"id": "OLCiexoeHVOiwQG3GZyVy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 25.79661787707461,
			"y": -37.13451386909188,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 36.21852236839018,
			"height": 29.044478618935084,
			"seed": 75717705,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695903666790,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "ApYy-bNGYSjwnGJokUj5C",
				"focus": 0.019006551052147925,
				"gap": 1.7213632114156994
			},
			"endBinding": {
				"elementId": "a6seGzh8fXoEI4UVgyDyr",
				"focus": -0.7442595692929016,
				"gap": 6.243123566524048
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					36.21852236839018,
					29.044478618935084
				]
			]
		},
		{
			"type": "line",
			"version": 8,
			"versionNonce": 1676130451,
			"isDeleted": false,
			"id": "g3po1TPvyuAjejsNalgFt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 19.490035178292146,
			"y": -203.10925466419172,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 8.477757920051943,
			"height": 17.72697252441145,
			"seed": 2063949127,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					8.477757920051943,
					-17.72697252441145
				]
			]
		},
		{
			"type": "line",
			"version": 23,
			"versionNonce": 2046561821,
			"isDeleted": false,
			"id": "n_WpsmqbMmRQ7y7o9iHT3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 35.326938826149956,
			"y": -203.0651714250884,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 13.296606994530634,
			"height": 15.83139324296991,
			"seed": 1088711657,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					13.296606994530634,
					-15.83139324296991
				]
			]
		},
		{
			"type": "rectangle",
			"version": 243,
			"versionNonce": 69856082,
			"isDeleted": false,
			"id": "VPgUtNhwXScbMgq8hiDm_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 186.55118453064426,
			"y": -73.64130583328063,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 146,
			"height": 35,
			"seed": 114802761,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "hD29Xcxy"
				}
			],
			"updated": 1695903670468,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 162,
			"versionNonce": 1513402642,
			"isDeleted": false,
			"id": "hD29Xcxy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 225.4812229827927,
			"y": -68.64130583328063,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 68.13992309570312,
			"height": 25,
			"seed": 658960489,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695903670468,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "resolve",
			"rawText": "resolve",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "VPgUtNhwXScbMgq8hiDm_",
			"originalText": "resolve",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 127,
			"versionNonce": 424020754,
			"isDeleted": false,
			"id": "harOCBlSYkeQmae0IIFmA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 189.19747616308666,
			"y": -46.7159891029997,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 4.483460825654561,
			"height": 177.6744492383068,
			"seed": 1134148329,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695903678596,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-4.483460825654561,
					-177.6744492383068
				]
			]
		},
		{
			"type": "line",
			"version": 140,
			"versionNonce": 236261586,
			"isDeleted": false,
			"id": "jWmnoHNi7u6w7b-FFVVxV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 337.47231067771645,
			"y": 24.173727697985782,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5.996926758091945,
			"height": 248.9609151912219,
			"seed": 1595956391,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695903681911,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-5.996926758091945,
					-248.9609151912219
				]
			]
		},
		{
			"type": "line",
			"version": 9,
			"versionNonce": 1042886195,
			"isDeleted": false,
			"id": "29FeuKHAS3pAubUgkST7U",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 54.618866338728196,
			"y": -205.56138483931272,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 15.059936558662287,
			"height": 16.181303953352284,
			"seed": 1230238441,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					15.059936558662287,
					-16.181303953352284
				]
			]
		},
		{
			"type": "line",
			"version": 10,
			"versionNonce": 1995326077,
			"isDeleted": false,
			"id": "R0UUYveY2Xk57JowTDr0K",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 80.61144619500703,
			"y": -203.7732584531854,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 12.662910432420745,
			"height": 15.850679660077589,
			"seed": 1926581767,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					12.662910432420745,
					-15.850679660077589
				]
			]
		},
		{
			"type": "line",
			"version": 15,
			"versionNonce": 1743450067,
			"isDeleted": false,
			"id": "YBhEycFBDI9VKpD-ujnVk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 103.00848686192353,
			"y": -200.2218025029264,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 21.179241186688046,
			"height": 20.67779434188813,
			"seed": 1780252905,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					21.179241186688046,
					-20.67779434188813
				]
			]
		},
		{
			"type": "line",
			"version": 12,
			"versionNonce": 341502685,
			"isDeleted": false,
			"id": "W9AKw8d4N62mh9FFm3Nwu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 128.83575457156496,
			"y": -202.09534016481635,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 12.764852922847126,
			"height": 15.56413860590618,
			"seed": 158658247,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695712182348,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					12.764852922847126,
					-15.56413860590618
				]
			]
		},
		{
			"type": "text",
			"version": 52,
			"versionNonce": 1067041671,
			"isDeleted": false,
			"id": "PYs5bUNo",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -77.03021683836681,
			"y": -222.39567177188255,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 73.17991638183594,
			"height": 25,
			"seed": 105698793,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128506502,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&bodies",
			"rawText": "&bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&bodies",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 25,
			"versionNonce": 1526907401,
			"isDeleted": false,
			"id": "6rzONMaJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -308.9609281091387,
			"y": -224.81792814167778,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 58.7999267578125,
			"height": 25,
			"seed": 787065287,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695128480293,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "bodies",
			"rawText": "bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "bodies",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 754,
			"versionNonce": 549441810,
			"isDeleted": false,
			"id": "a6seGzh8fXoEI4UVgyDyr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 68.25826381198885,
			"y": -6.126785467179218,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 268,
			"height": 35,
			"seed": 439403406,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "n19SLl4c"
				},
				{
					"id": "OLCiexoeHVOiwQG3GZyVy",
					"type": "arrow"
				}
			],
			"updated": 1695903663862,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 676,
			"versionNonce": 1049091794,
			"isDeleted": false,
			"id": "n19SLl4c",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 157.1983120297623,
			"y": -1.126785467179218,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 90.11990356445312,
			"height": 25,
			"seed": 1185751502,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695903663862,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "for_each",
			"rawText": "for_each",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "a6seGzh8fXoEI4UVgyDyr",
			"originalText": "for_each",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"id": "u_MmfaXTerwLF-mikr7Hy",
			"type": "line",
			"x": 154.8449543778269,
			"y": -200.63521529160747,
			"width": 14.682169741244849,
			"height": 19.29933014158175,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1594169298,
			"version": 15,
			"versionNonce": 611968910,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903697262,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					14.682169741244849,
					-19.29933014158175
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "ieS5hVs7fHOnY0_Vw4mSr",
			"type": "line",
			"x": 181.1600997809033,
			"y": -197.81954811105516,
			"width": 15.94821914292686,
			"height": 22.33684787605199,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1949418958,
			"version": 15,
			"versionNonce": 1058228434,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903698560,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					15.94821914292686,
					-22.33684787605199
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "wtAt-x8woTbGYDb6wp_UI",
			"type": "line",
			"x": 207.8155272366057,
			"y": -201.99634350211142,
			"width": 20.029935725161096,
			"height": 18.10000271100293,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 2065063570,
			"version": 17,
			"versionNonce": 86280526,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903700628,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					20.029935725161096,
					-18.10000271100293
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "UqsRZhzrQtWOcx6ikzV3D",
			"type": "line",
			"x": 233.59022467374672,
			"y": -202.6068495377051,
			"width": 22.01991851331212,
			"height": 18.957380039678213,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 849489806,
			"version": 17,
			"versionNonce": 1554880146,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903701762,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					22.01991851331212,
					-18.957380039678213
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "04DkrnpE30FUht0NhbQrF",
			"type": "line",
			"x": 263.96039787061613,
			"y": -203.20567922835573,
			"width": 18.94236759618002,
			"height": 18.883985871464773,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 329315410,
			"version": 14,
			"versionNonce": 580660430,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903702924,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					18.94236759618002,
					-18.883985871464773
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "9HM5EScJ6n2c7TKsNxlOv",
			"type": "line",
			"x": 280.0687497441905,
			"y": -201.8829161512361,
			"width": 20.512001966381263,
			"height": 18.283488131536558,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 408511246,
			"version": 15,
			"versionNonce": 940976018,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903703529,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					20.512001966381263,
					-18.283488131536558
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "GnmjAL2CAiHlJohiTe1yI",
			"type": "line",
			"x": 304.1920783965289,
			"y": -206.05804349301476,
			"width": 14.745555613792817,
			"height": 18.074981971839236,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1976526162,
			"version": 16,
			"versionNonce": 1120972878,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903704509,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					14.745555613792817,
					-18.074981971839236
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"type": "text",
			"version": 62,
			"versionNonce": 416149714,
			"isDeleted": false,
			"id": "fB3yeWtF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 200.8484808492371,
			"y": -222.73932399927835,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 118.49986267089844,
			"height": 25,
			"seed": 177830281,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695903711578,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut bodies",
			"rawText": "&mut bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut bodies",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"id": "sUyiqFTX4nolnce7-rjzT",
			"type": "line",
			"x": 322.22702718570633,
			"y": -202.5234470738262,
			"width": 13.215954426253404,
			"height": 19.53952923755307,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1446082190,
			"version": 15,
			"versionNonce": 1296022546,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695903705356,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					13.215954426253404,
					-19.53952923755307
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"type": "line",
			"version": 337,
			"versionNonce": 79951954,
			"isDeleted": true,
			"id": "SIseSqjz7Q8RbcQeBs8FB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 223.58998350046824,
			"y": -33.57602646149334,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 2.595400702206348,
			"height": 214.64407562170314,
			"seed": 1984686025,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695903674397,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-2.595400702206348,
					-214.64407562170314
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 371.19660120627003,
		"scrollY": 649.0487091652244,
		"zoom": {
			"value": 0.976635198212286
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%