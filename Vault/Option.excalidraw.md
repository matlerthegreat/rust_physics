---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Option<T> ^h9z5ZU11

Some(T) ^QOR4TFjv

None ^0a6Khrx6

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"id": "ueDhWfnq8GiNgMfcXbB6S",
			"type": "diamond",
			"x": -123.888671875,
			"y": -327.1796875,
			"width": 217,
			"height": 70,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 211743959,
			"version": 291,
			"versionNonce": 151460985,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "h9z5ZU11"
				},
				{
					"id": "_Hn-EkBWu7F6mBY5Kn1OV",
					"type": "arrow"
				},
				{
					"id": "qHydKLwjYAkkMiV9393de",
					"type": "arrow"
				}
			],
			"updated": 1695842842283,
			"link": null,
			"locked": false
		},
		{
			"id": "h9z5ZU11",
			"type": "text",
			"x": -64.3186264038086,
			"y": -304.6796875,
			"width": 98.35990905761719,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1956514711,
			"version": 275,
			"versionNonce": 1827481303,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695842745747,
			"link": null,
			"locked": false,
			"text": "Option<T>",
			"rawText": "Option<T>",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "ueDhWfnq8GiNgMfcXbB6S",
			"originalText": "Option<T>",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 725,
			"versionNonce": 302971383,
			"isDeleted": false,
			"id": "B2y3NNsKTId8BFLlVUkRK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -213.8359375,
			"y": -190.7109375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 120,
			"seed": 251444089,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "QOR4TFjv"
				},
				{
					"id": "_Hn-EkBWu7F6mBY5Kn1OV",
					"type": "arrow"
				}
			],
			"updated": 1695842839988,
			"link": null,
			"locked": false
		},
		{
			"id": "QOR4TFjv",
			"type": "text",
			"x": -191.22917176377098,
			"y": -142.65657514042363,
			"width": 74.93365478515625,
			"height": 24.03846153846154,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1154521815,
			"version": 1461,
			"versionNonce": 1412122423,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695842830868,
			"link": null,
			"locked": false,
			"text": "Some(T)",
			"rawText": "Some(T)",
			"fontSize": 19.230769230769234,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "B2y3NNsKTId8BFLlVUkRK",
			"originalText": "Some(T)",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 752,
			"versionNonce": 105058873,
			"isDeleted": false,
			"id": "onkgSYd2PhXNrScu_q24F",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 56.51953125,
			"y": -189.11328125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 120,
			"seed": 2065932633,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "0a6Khrx6"
				},
				{
					"id": "qHydKLwjYAkkMiV9393de",
					"type": "arrow"
				}
			],
			"updated": 1695842842284,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 1493,
			"versionNonce": 1871145049,
			"isDeleted": false,
			"id": "0a6Khrx6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 95.33725737441262,
			"y": -141.05891889042363,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 42.51173400878906,
			"height": 24.03846153846154,
			"seed": 1085804089,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695842832066,
			"link": null,
			"locked": false,
			"fontSize": 19.230769230769234,
			"fontFamily": 1,
			"text": "None",
			"rawText": "None",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "onkgSYd2PhXNrScu_q24F",
			"originalText": "None",
			"lineHeight": 1.25,
			"baseline": 17
		},
		{
			"id": "_Hn-EkBWu7F6mBY5Kn1OV",
			"type": "arrow",
			"x": -63.79296875,
			"y": -259.9609375,
			"width": 52.23828125,
			"height": 70.1875,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1011475993,
			"version": 48,
			"versionNonce": 1215081687,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695842839988,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-52.23828125,
					70.1875
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "ueDhWfnq8GiNgMfcXbB6S",
				"focus": 0.22511452130977344,
				"gap": 12.213314107684958
			},
			"endBinding": {
				"elementId": "B2y3NNsKTId8BFLlVUkRK",
				"focus": -0.08227405125501078,
				"gap": 10.125411251718546
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "qHydKLwjYAkkMiV9393de",
			"type": "arrow",
			"x": 40.34765625,
			"y": -268.42578125,
			"width": 44.71875,
			"height": 73.1640625,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1210753815,
			"version": 37,
			"versionNonce": 918496601,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1695842842284,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					44.71875,
					73.1640625
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "ueDhWfnq8GiNgMfcXbB6S",
				"focus": -0.37988623540993643,
				"gap": 6.408203339043958
			},
			"endBinding": {
				"elementId": "onkgSYd2PhXNrScu_q24F",
				"focus": 0.12766786085106657,
				"gap": 13.245579088140417
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 417.0859375,
		"scrollY": 574.03515625,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%