---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "ellipse",
			"version": 266,
			"versionNonce": 550543479,
			"isDeleted": false,
			"id": "fg2SEl76TfxvEES40v779",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -144.68359375,
			"y": -90.0390625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 253.453125,
			"height": 147.5546875,
			"seed": 713591033,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846281989,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 184,
			"versionNonce": 217282967,
			"isDeleted": false,
			"id": "edfvuPtN-uaBTPwe6jynC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 10.703125,
			"y": -157.50390625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 266.29296875,
			"height": 300.05078125,
			"seed": 584608633,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846281989,
			"link": null,
			"locked": false
		},
		{
			"type": "line",
			"version": 19,
			"versionNonce": 1646391289,
			"isDeleted": false,
			"id": "t9b3v3Kk5AemAOFCSY1xQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 23.948069852941188,
			"y": -67.80939797794116,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 25.275735294117624,
			"height": 12.931985294117652,
			"seed": 1836400599,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846292171,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					25.275735294117624,
					-12.931985294117652
				]
			]
		},
		{
			"type": "line",
			"version": 25,
			"versionNonce": 1725207319,
			"isDeleted": false,
			"id": "YzBUvR1rGUsNkP1dbXEGO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 17.737132352941188,
			"y": -56.17566636029409,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 35.70312499999997,
			"height": 16.539522058823536,
			"seed": 1546881241,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846293172,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					35.70312499999997,
					-16.539522058823536
				]
			]
		},
		{
			"type": "line",
			"version": 30,
			"versionNonce": 1784654649,
			"isDeleted": false,
			"id": "fD8w4f-DT-xugmFzW0yxe",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 16.257352941176464,
			"y": -39.700482536764696,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 57.58042279411765,
			"height": 26.360294117647044,
			"seed": 1595938871,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846294671,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					57.58042279411765,
					-26.360294117647044
				]
			]
		},
		{
			"type": "line",
			"version": 36,
			"versionNonce": 1711071287,
			"isDeleted": false,
			"id": "Zlk8JgdhgIGHW2Nn1pXaI",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 9.690257352941188,
			"y": -13.275850183823536,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 79.73345588235293,
			"height": 41.21093749999997,
			"seed": 1670896665,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846296324,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					79.73345588235293,
					-41.21093749999997
				]
			]
		},
		{
			"type": "line",
			"version": 33,
			"versionNonce": 1767603897,
			"isDeleted": false,
			"id": "vmd_uhdAw562geuqi2ADx",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 12.544117647058812,
			"y": -1.3640854779411598,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 83.65579044117646,
			"height": 44.36351102941177,
			"seed": 704758103,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846297904,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					83.65579044117646,
					-44.36351102941177
				]
			]
		},
		{
			"type": "line",
			"version": 27,
			"versionNonce": 2093459095,
			"isDeleted": false,
			"id": "mJGe9sHhBXrjDHsTKwlEk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 16.259650735294116,
			"y": 10.586741727941217,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 85.79963235294116,
			"height": 45.60661764705884,
			"seed": 1254965145,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846299308,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					85.79963235294116,
					-45.60661764705884
				]
			]
		},
		{
			"type": "line",
			"version": 60,
			"versionNonce": 183731705,
			"isDeleted": false,
			"id": "ELKEx-JPjWaTRcDRE4WON",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 15.411764705882348,
			"y": 30.15935202205884,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 93.7454044117647,
			"height": 51.64751838235293,
			"seed": 2086976439,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846301399,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					93.7454044117647,
					-51.64751838235293
				]
			]
		},
		{
			"type": "line",
			"version": 55,
			"versionNonce": 969788119,
			"isDeleted": false,
			"id": "pqJKOHMkcL45CG6-tGxGC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 19.543198529411768,
			"y": 47.829388786764696,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 84.93795955882351,
			"height": 46.792279411764696,
			"seed": 1650613977,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695846303387,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					84.93795955882351,
					-46.792279411764696
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#e03131",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": "arrow",
		"currentItemEndArrowhead": "arrow",
		"scrollX": 160.02022058823528,
		"scrollY": 283.22529871323525,
		"zoom": {
			"value": 1.7000000000000002
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%