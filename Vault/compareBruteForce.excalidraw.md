---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Rust ^gGGdEpnp

C++ ^p7S5af9d

broadphase time (lower is better) ^m3CH3wLA

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "line",
			"version": 358,
			"versionNonce": 314143118,
			"isDeleted": false,
			"id": "zmoxM1r-Soke-3S_GMvxl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -538.0316490507741,
			"y": 172.59289667432265,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 691.9766749823971,
			"height": 329.2589641975111,
			"seed": 944572882,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923655338,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					114.6971774530349,
					-14.592078262931594
				],
				[
					307.7987773580857,
					-72.7929914832364
				],
				[
					500.398177768872,
					-173.1527270366288
				],
				[
					691.9766749823971,
					-329.2589641975111
				]
			]
		},
		{
			"type": "freedraw",
			"version": 84,
			"versionNonce": 1866024526,
			"isDeleted": false,
			"id": "Fu_lHo_dTuN9OoFWR0hVE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -536.753751746119,
			"y": 169.60327714977956,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 2.1903442731077583,
			"height": 0.2334166663484135,
			"seed": 231902798,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923678123,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.2145547135122188,
					0
				],
				[
					-0.5139882147874257,
					0
				],
				[
					-1.6009082469754417,
					0
				],
				[
					-2.1903442731077583,
					0
				],
				[
					-1.8343249133237431,
					0
				],
				[
					-1.574973061825506,
					0
				],
				[
					-1.1953762609963594,
					0
				],
				[
					-0.8134217160626327,
					0
				],
				[
					-0.5187037029964745,
					0
				],
				[
					-0.5187037029964745,
					-0.2334166663484135
				],
				[
					-0.306506733588836,
					-0.2334166663484135
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 74,
			"versionNonce": 866545422,
			"isDeleted": false,
			"id": "28M0vhRQGPOHx0nYDjjJH",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -421.43177210538386,
			"y": 157.19211218353752,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.5399233999371935,
			"seed": 874570766,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923671445,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.5399233999371935
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 72,
			"versionNonce": 938923666,
			"isDeleted": false,
			"id": "jD0zq_fgJdT3CugYcCweV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -229.99709728223513,
			"y": 100.10641192467517,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 730504014,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923713977,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 72,
			"versionNonce": 1050333778,
			"isDeleted": false,
			"id": "FbCReKIVHaBvCp8Dg3pUl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -38.06022296482172,
			"y": -1.4604886102363537,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 118584846,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923692303,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 72,
			"versionNonce": 1929564434,
			"isDeleted": false,
			"id": "GkI_kIAT8gvRMqjyeN63N",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 154.8197489944033,
			"y": -159.33503384951564,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 1098729678,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923687636,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 70,
			"versionNonce": 1199205010,
			"isDeleted": false,
			"id": "M4nb16Bzj1iSRD03xFNCm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -537.288325281886,
			"y": 174.2807910920596,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 1807108494,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915477523,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 71,
			"versionNonce": 1541957266,
			"isDeleted": false,
			"id": "BKp-sPHipCS8-mRQZC0o5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -421.2885466226425,
			"y": 161.9678790547175,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 546884686,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923673752,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 71,
			"versionNonce": 1910177550,
			"isDeleted": false,
			"id": "2NYR4KW4QYzWujTcWlvbt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -229.42797824884718,
			"y": 115.50347664366751,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 1327580942,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923711974,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 71,
			"versionNonce": 316459918,
			"isDeleted": false,
			"id": "MguO1BU_3IeWq95gBuIoW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -36.476364395868586,
			"y": 36.78697477408707,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0.00019707732077568687,
			"height": 0.00019707732077568687,
			"seed": 745716174,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923705408,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.00019707732077568687,
					0.00019707732077568687
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 73,
			"versionNonce": 1733603150,
			"isDeleted": false,
			"id": "SnV7-Gmo0-ArASyHaj7NR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 155.10812971699977,
			"y": -71.45180426197788,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.34012348946200877,
			"seed": 1016079502,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923690240,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.34012348946200877
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 329,
			"versionNonce": 871116690,
			"isDeleted": false,
			"id": "c1DBIXlzla03K-H99SQ71",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 153.97111419362,
			"y": -74.3518969500496,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 691.1441821513214,
			"height": 244.61062955763006,
			"seed": 333548494,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923707038,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-190.7032650719896,
					108.28957670007554
				],
				[
					-384.53610796584695,
					185.58374397736006
				],
				[
					-575.0582683226688,
					234.8243491562914
				],
				[
					-691.1441821513214,
					244.61062955763006
				]
			]
		},
		{
			"type": "line",
			"version": 265,
			"versionNonce": 284613586,
			"isDeleted": false,
			"id": "f8_KkP0vAG_XoSTqet5bg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -538.7172571914584,
			"y": 176.59010577165802,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 683.7737156240881,
			"height": 0,
			"seed": 195462862,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695915477523,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					683.7737156240881,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 127,
			"versionNonce": 53297230,
			"isDeleted": false,
			"id": "gGGdEpnp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.874820892634,
			"y": -132.23524621690868,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 65.91227722167969,
			"height": 35,
			"seed": 2055122254,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915486946,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Rust",
			"rawText": "Rust",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Rust",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "text",
			"version": 78,
			"versionNonce": 1947688850,
			"isDeleted": false,
			"id": "p7S5af9d",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -523.2467498469856,
			"y": -101.7614428283194,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 53.0322265625,
			"height": 35,
			"seed": 548624910,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695915483378,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "C++",
			"rawText": "C++",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C++",
			"lineHeight": 1.25,
			"baseline": 25
		},
		{
			"type": "text",
			"version": 135,
			"versionNonce": 759428114,
			"isDeleted": false,
			"id": "m3CH3wLA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -361.43150845727257,
			"y": 181.13097320356837,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 330.319580078125,
			"height": 25,
			"seed": 1008352526,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695923908911,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "broadphase time (lower is better)",
			"rawText": "broadphase time (lower is better)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "broadphase time (lower is better)",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 28,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 558.1913612753528,
		"scrollY": 437.52602084405055,
		"zoom": {
			"value": 1.05
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%