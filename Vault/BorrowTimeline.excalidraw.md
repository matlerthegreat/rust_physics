---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Frame 0 ^4vyN1b8A

Frame 1 ^GJ6CDDOc

Frame 2 ^27ZgfkIW

... ^9euy4qle

position integration ^oAWhfASS

collision detection ^krb3ybaR

constraint resolution ^nQTww62e

bodies ^dXaIrHjb

&mut ^gJNzV9B4

&mut ^lDqSn1MK

&mut ^4FO2yZAI

&mut ^YfZfrSwL

&mut ^OYLHGqRg

&mut ^HMWlUVLG

& ^AikYSoUd

& ^Bfoy9HDT

& ^YftQa705

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "line",
			"version": 176,
			"versionNonce": 730426674,
			"isDeleted": false,
			"id": "h-BulzdvyXON8jA03drPJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -121.60357436850376,
			"y": -9.90401581236716,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 10.836732631148152,
			"height": 62.706149211443574,
			"seed": 1926890951,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-10.836732631148152,
					62.706149211443574
				]
			]
		},
		{
			"type": "line",
			"version": 191,
			"versionNonce": 1039447790,
			"isDeleted": false,
			"id": "JDzswJE_sW7Fkp38TM7oR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 54.50640929294224,
			"y": -8.100254121912256,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 8.923892187364117,
			"height": 58.48876614602841,
			"seed": 1062506697,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-8.923892187364117,
					58.48876614602841
				]
			]
		},
		{
			"type": "line",
			"version": 215,
			"versionNonce": 1376319218,
			"isDeleted": false,
			"id": "T4YZUfRqOgfb28saS_ao0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 244.75932154776183,
			"y": -6.950414211913994,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 10.13479421195052,
			"height": 61.5908710899713,
			"seed": 716390311,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-10.13479421195052,
					61.5908710899713
				]
			]
		},
		{
			"type": "text",
			"version": 124,
			"versionNonce": 1750837550,
			"isDeleted": false,
			"id": "4vyN1b8A",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -253.9140625,
			"y": 34.24609375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 80.77992248535156,
			"height": 25,
			"seed": 797769801,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Frame 0",
			"rawText": "Frame 0",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Frame 0",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 125,
			"versionNonce": 460409010,
			"isDeleted": false,
			"id": "GJ6CDDOc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -86.88671875,
			"y": 35.1015625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 72.43992614746094,
			"height": 25,
			"seed": 517267433,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Frame 1",
			"rawText": "Frame 1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Frame 1",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 122,
			"versionNonce": 95048558,
			"isDeleted": false,
			"id": "27ZgfkIW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 95.37109375,
			"y": 32.8203125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 81.25991821289062,
			"height": 25,
			"seed": 1419043273,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Frame 2",
			"rawText": "Frame 2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Frame 2",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 115,
			"versionNonce": 1063871090,
			"isDeleted": false,
			"id": "9euy4qle",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 247.9609375,
			"y": 32.1171875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 16.439987182617188,
			"height": 25,
			"seed": 925747113,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443354,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "...",
			"rawText": "...",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "...",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 289,
			"versionNonce": 1344910766,
			"isDeleted": false,
			"id": "oAWhfASS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -219.56464019395384,
			"y": -138.63589420606013,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 186.29977416992188,
			"height": 25,
			"seed": 1147296424,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "mc8vy0ETYD8PORvBPZuWX",
					"type": "arrow"
				}
			],
			"updated": 1696497443355,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "position integration",
			"rawText": "position integration",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "position integration",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 215,
			"versionNonce": 1756906546,
			"isDeleted": false,
			"id": "krb3ybaR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -106.51404495585851,
			"y": -99.72592396796485,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 172.3798065185547,
			"height": 25,
			"seed": 1808851624,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "uoo2VQURaZ3AKXVkgbV-g",
					"type": "arrow"
				}
			],
			"updated": 1696497443355,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "collision detection",
			"rawText": "collision detection",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "collision detection",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 367,
			"versionNonce": 1359138798,
			"isDeleted": false,
			"id": "nQTww62e",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -25.008696310245483,
			"y": -61.77577515844115,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 203.77976989746094,
			"height": 25,
			"seed": 1415050200,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "SKjLxPB8H5dg18KFitSZT",
					"type": "arrow"
				}
			],
			"updated": 1696497443355,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "constraint resolution",
			"rawText": "constraint resolution",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "constraint resolution",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "arrow",
			"version": 233,
			"versionNonce": 1363471858,
			"isDeleted": false,
			"id": "mc8vy0ETYD8PORvBPZuWX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -227.57803305109672,
			"y": -119.05628111082206,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 20.066964285714306,
			"height": 97.06473214285711,
			"seed": 748066984,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443355,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "oAWhfASS",
				"focus": 0.6513859611195535,
				"gap": 8.01339285714289
			},
			"endBinding": {
				"elementId": "AveuvtWvg6-Aavav-vDch",
				"focus": -0.6278144390821084,
				"gap": 13.538312064429931
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-20.066964285714306,
					7.54464285714289
				],
				[
					-19.438244047619037,
					97.06473214285711
				]
			]
		},
		{
			"type": "arrow",
			"version": 95,
			"versionNonce": 71213614,
			"isDeleted": false,
			"id": "uoo2VQURaZ3AKXVkgbV-g",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -117.45526519395384,
			"y": -82.96997158701254,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 83.11011904761907,
			"height": 63.27380952380952,
			"seed": 2144479704,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443355,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "krb3ybaR",
				"focus": -0.06264056203148242,
				"gap": 10.941220238095326
			},
			"endBinding": {
				"elementId": "AveuvtWvg6-Aavav-vDch",
				"focus": -0.5028301780653349,
				"gap": 11.242925159668005
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-60.956101190476204,
					2.064732142857167
				],
				[
					-83.11011904761907,
					63.27380952380952
				]
			]
		},
		{
			"type": "arrow",
			"version": 141,
			"versionNonce": 1578524594,
			"isDeleted": false,
			"id": "SKjLxPB8H5dg18KFitSZT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -29.62551178643595,
			"y": -47.95462919222699,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 107.15639031227975,
			"height": 26.137931414738304,
			"seed": 1747935144,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443355,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "nQTww62e",
				"focus": 0.27458104358708946,
				"gap": 4.616815476190482
			},
			"endBinding": {
				"elementId": "AveuvtWvg6-Aavav-vDch",
				"focus": -0.3943908909218461,
				"gap": 13.36346087395367
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-70.30371174085126,
					4.255490938547766
				],
				[
					-107.15639031227975,
					26.137931414738304
				]
			]
		},
		{
			"type": "rectangle",
			"version": 140,
			"versionNonce": 2093362286,
			"isDeleted": false,
			"id": "AveuvtWvg6-Aavav-vDch",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -365.37598457738056,
			"y": -8.453236903535014,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 637.3254693999272,
			"height": 25.69486654238574,
			"seed": 1446353021,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "mc8vy0ETYD8PORvBPZuWX",
					"type": "arrow"
				},
				{
					"id": "uoo2VQURaZ3AKXVkgbV-g",
					"type": "arrow"
				},
				{
					"id": "SKjLxPB8H5dg18KFitSZT",
					"type": "arrow"
				}
			],
			"updated": 1696497443355,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 48,
			"versionNonce": 995334514,
			"isDeleted": false,
			"id": "dXaIrHjb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -358.0280176430572,
			"y": -6.289570495404178,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 58.7999267578125,
			"height": 25,
			"seed": 42053149,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "bodies",
			"rawText": "bodies",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "bodies",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 77,
			"versionNonce": 140668590,
			"isDeleted": false,
			"id": "fTjHZ3yjIE-b4GfTIue-w",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -276.60293607480037,
			"y": -10.235954117964866,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 9.192140278343231,
			"height": 62.693698645839504,
			"seed": 1617869693,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-9.192140278343231,
					62.693698645839504
				]
			]
		},
		{
			"type": "text",
			"version": 24,
			"versionNonce": 2017696562,
			"isDeleted": false,
			"id": "gJNzV9B4",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -279.36381472400666,
			"y": -7.714681878671001,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 49.699951171875,
			"height": 25,
			"seed": 731675250,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut",
			"rawText": "&mut",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 54,
			"versionNonce": 1404856558,
			"isDeleted": false,
			"id": "lDqSn1MK",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -175.9585819766109,
			"y": -8.632149827389014,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 49.699951171875,
			"height": 25,
			"seed": 590893042,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut",
			"rawText": "&mut",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 54,
			"versionNonce": 592274674,
			"isDeleted": false,
			"id": "4FO2yZAI",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -120.96459159199549,
			"y": -8.441845340209454,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 49.699951171875,
			"height": 25,
			"seed": 1328031790,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut",
			"rawText": "&mut",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 63,
			"versionNonce": 240982830,
			"isDeleted": false,
			"id": "YfZfrSwL",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 1.104719305440426,
			"y": -7.600499186363379,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 49.699951171875,
			"height": 25,
			"seed": 968028594,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut",
			"rawText": "&mut",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 61,
			"versionNonce": 1710749362,
			"isDeleted": false,
			"id": "OYLHGqRg",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 57.05824494646606,
			"y": -6.248335724824869,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 49.699951171875,
			"height": 25,
			"seed": 1533540014,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut",
			"rawText": "&mut",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 71,
			"versionNonce": 246252910,
			"isDeleted": false,
			"id": "HMWlUVLG",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 187.25055263877368,
			"y": -8.890563288927524,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 49.699951171875,
			"height": 25,
			"seed": 2137470834,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&mut",
			"rawText": "&mut",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&mut",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 24,
			"versionNonce": 552226930,
			"isDeleted": false,
			"id": "AikYSoUd",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -207.74121857016053,
			"y": -7.957069699183876,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 14.379989624023438,
			"height": 25,
			"seed": 367630062,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443356,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&",
			"rawText": "&",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 62,
			"versionNonce": 1979099054,
			"isDeleted": false,
			"id": "Bfoy9HDT",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -44.4083768437107,
			"y": -8.820451109440285,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 14.379989624023438,
			"height": 25,
			"seed": 668691374,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497443357,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&",
			"rawText": "&",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 65,
			"versionNonce": 1232268210,
			"isDeleted": false,
			"id": "YftQa705",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 140.7098122588534,
			"y": -8.866524827388957,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 14.379989624023438,
			"height": 25,
			"seed": 723964978,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1696497483620,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "&",
			"rawText": "&",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "&",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 32,
			"versionNonce": 944509938,
			"isDeleted": false,
			"id": "4b5gcwu6rgYVX7JvQzTEo",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -216.06854228810926,
			"y": -8.359713929953102,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 3.473557692307679,
			"height": 24.078525641025635,
			"seed": 966811246,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497448955,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-3.473557692307679,
					24.078525641025635
				]
			]
		},
		{
			"type": "line",
			"version": 20,
			"versionNonce": 1933199086,
			"isDeleted": false,
			"id": "I7_BpIlH5AZwElXjb73T5",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -179.41990446759644,
			"y": -7.786797263286417,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 6.356169871794862,
			"height": 21.742788461538453,
			"seed": 1699268018,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497450341,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-6.356169871794862,
					21.742788461538453
				]
			]
		},
		{
			"type": "line",
			"version": 20,
			"versionNonce": 264376370,
			"isDeleted": false,
			"id": "y1NHopqO2NeXRxe9ounLI",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -54.0132538265708,
			"y": -7.8068293145685175,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 7.465945512820497,
			"height": 23.59775641025641,
			"seed": 2121218350,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497452057,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-7.465945512820497,
					23.59775641025641
				]
			]
		},
		{
			"type": "line",
			"version": 19,
			"versionNonce": 349996142,
			"isDeleted": false,
			"id": "qrnlKMhColJBdgAh-YnLV",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -13.28609036503235,
			"y": -6.296412647901832,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 4.671474358974365,
			"height": 20.470753205128233,
			"seed": 221592050,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497453133,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-4.671474358974365,
					20.470753205128233
				]
			]
		},
		{
			"type": "line",
			"version": 25,
			"versionNonce": 1230670706,
			"isDeleted": false,
			"id": "775UyHrb3qqWJttbV07Fr",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 127.4751275836856,
			"y": -8.762358160722329,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 4.74358974358978,
			"height": 24.84375,
			"seed": 2079180462,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497454844,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-4.74358974358978,
					24.84375
				]
			]
		},
		{
			"type": "line",
			"version": 18,
			"versionNonce": 217922286,
			"isDeleted": false,
			"id": "SWwnLtb2005yyTpu_QaN8",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 176.7639897631728,
			"y": -7.636556878671058,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5.570913461538453,
			"height": 23.046875,
			"seed": 1285311794,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1696497455957,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-5.570913461538453,
					23.046875
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 0.5,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 383.3799165440836,
		"scrollY": 323.36991206515125,
		"zoom": {
			"value": 1.6500000000000001
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%