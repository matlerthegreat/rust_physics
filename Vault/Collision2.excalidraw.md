---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
 ^i2fY21mk

normal ^eUWRg06I

depth ^phze2ucs

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "ellipse",
			"version": 361,
			"versionNonce": 1401586041,
			"isDeleted": false,
			"id": "fg2SEl76TfxvEES40v779",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -144.68359375,
			"y": -90.0390625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 253.453125,
			"height": 147.5546875,
			"seed": 713591033,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695845756468,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 226,
			"versionNonce": 1516449433,
			"isDeleted": false,
			"id": "edfvuPtN-uaBTPwe6jynC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 10.349609375,
			"y": -157.978515625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 267,
			"height": 301,
			"seed": 584608633,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "i2fY21mk"
				}
			],
			"updated": 1695845989806,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 45,
			"versionNonce": 93186649,
			"isDeleted": false,
			"id": "i2fY21mk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 138.84961700439453,
			"y": -20.062486686098197,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 9.999984741210938,
			"height": 25,
			"seed": 1265656153,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695845756469,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "edfvuPtN-uaBTPwe6jynC",
			"originalText": "",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "arrow",
			"version": 402,
			"versionNonce": 1788788887,
			"isDeleted": false,
			"id": "62Gf4nygLcl8M8saKdgAb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 11.466074671544789,
			"y": -23.05689157304471,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 215.4093848872787,
			"height": 26.428559771574093,
			"seed": 1436878617,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695845992373,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					215.4093848872787,
					26.428559771574093
				]
			]
		},
		{
			"type": "freedraw",
			"version": 52,
			"versionNonce": 860599511,
			"isDeleted": false,
			"id": "-9LnZ-7_QJUT5zDGXM8Ez",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 18.674632352941188,
			"y": -38.691750919117624,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 12.465533088235304,
			"height": 37.53676470588232,
			"seed": 1645547255,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695845756469,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.234375,
					0
				],
				[
					-0.5629595588235361,
					0
				],
				[
					-1.0064338235294201,
					0.574448529411768
				],
				[
					-1.6061580882353041,
					0.574448529411768
				],
				[
					-2.805606617647072,
					1.1626838235294201
				],
				[
					-3.3938419117647243,
					1.7509191176470438
				],
				[
					-4.5703125,
					2.339154411764696
				],
				[
					-6.022518382352956,
					3.527113970588232
				],
				[
					-6.62224264705884,
					3.527113970588232
				],
				[
					-7.210477941176492,
					4.115349264705884
				],
				[
					-7.210477941176492,
					4.692095588235304
				],
				[
					-7.789522058823536,
					5.280330882352928
				],
				[
					-8.97518382352942,
					5.880055147058812
				],
				[
					-9.563419117647072,
					6.468290441176464
				],
				[
					-10.151654411764724,
					7.644761029411768
				],
				[
					-10.739889705882376,
					8.23299632352942
				],
				[
					-11.2890625,
					9.416360294117652
				],
				[
					-11.877297794117652,
					10.592830882352928
				],
				[
					-11.877297794117652,
					11.769301470588232
				],
				[
					-12.465533088235304,
					12.357536764705856
				],
				[
					-12.465533088235304,
					13.53400735294116
				],
				[
					-12.465533088235304,
					14.12224264705884
				],
				[
					-12.465533088235304,
					15.298713235294088
				],
				[
					-12.465533088235304,
					15.886948529411768
				],
				[
					-12.465533088235304,
					16.456801470588232
				],
				[
					-12.465533088235304,
					17.6171875
				],
				[
					-12.465533088235304,
					18.793658088235304
				],
				[
					-11.877297794117652,
					20.25505514705884
				],
				[
					-11.2890625,
					22.771139705882376
				],
				[
					-11.2890625,
					24.099264705882376
				],
				[
					-10.588235294117652,
					24.680606617647072
				],
				[
					-10.588235294117652,
					25.395220588235304
				],
				[
					-9.2578125,
					27.17830882352939
				],
				[
					-8.062959558823536,
					30.091911764705856
				],
				[
					-7.474724264705884,
					32.1875
				],
				[
					-6.88648897058826,
					33.363970588235304
				],
				[
					-6.298253676470608,
					33.363970588235304
				],
				[
					-6.298253676470608,
					33.947610294117624
				],
				[
					-5.72380514705884,
					35.07123161764707
				],
				[
					-5.124080882352956,
					35.659466911764696
				],
				[
					-4.524356617647072,
					37.08869485294116
				],
				[
					-3.93612132352942,
					37.08869485294116
				],
				[
					-3.93612132352942,
					37.53676470588232
				],
				[
					-3.93612132352942,
					37.53676470588232
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 46,
			"versionNonce": 1075308569,
			"isDeleted": false,
			"id": "RXW-OV9EbxJYcvK23qN-K",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 102.19944852941174,
			"y": -26.095243566176464,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 10.209099264705856,
			"height": 27.79181985294116,
			"seed": 388966071,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695845756469,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.22518382352944855,
					0
				],
				[
					0.22518382352944855,
					0.5284926470588402
				],
				[
					0.8134191176470722,
					0.5284926470588402
				],
				[
					1.4016544117646959,
					1.7049632352941444
				],
				[
					1.9898897058823763,
					1.7049632352941444
				],
				[
					2.578125,
					2.8814338235294485
				],
				[
					3.754595588235304,
					3.469669117647072
				],
				[
					4.35661764705884,
					5.245863970588232
				],
				[
					5.089613970588232,
					6.569393382352928
				],
				[
					5.808823529411768,
					7.157628676470608
				],
				[
					6.392463235294144,
					7.867647058823536
				],
				[
					6.392463235294144,
					8.455882352941217
				],
				[
					6.9921875,
					9.632352941176464
				],
				[
					6.9921875,
					11.40854779411768
				],
				[
					7.5804227941176805,
					12.605698529411768
				],
				[
					7.5804227941176805,
					13.782169117647072
				],
				[
					8.168658088235304,
					14.368106617647072
				],
				[
					8.168658088235304,
					15.443474264705912
				],
				[
					8.168658088235304,
					16.031709558823536
				],
				[
					8.168658088235304,
					17.20818014705884
				],
				[
					7.5804227941176805,
					18.384650735294144
				],
				[
					7.5804227941176805,
					19.56112132352945
				],
				[
					6.9921875,
					20.737591911764696
				],
				[
					6.403952205882376,
					21.9140625
				],
				[
					5.818014705882376,
					21.9140625
				],
				[
					5.818014705882376,
					22.497702205882376
				],
				[
					5.241268382352985,
					23.074448529411768
				],
				[
					4.0647977941176805,
					23.66268382352945
				],
				[
					3.4765625,
					24.250919117647072
				],
				[
					2.159926470588232,
					25.71231617647061
				],
				[
					1.4361213235294485,
					26.30974264705884
				],
				[
					0.8731617647059124,
					26.30974264705884
				],
				[
					-0.04136029411762365,
					26.847426470588232
				],
				[
					-0.7559742647058556,
					27.435661764705912
				],
				[
					-1.4797794117646959,
					27.435661764705912
				],
				[
					-2.0404411764705515,
					27.435661764705912
				],
				[
					-2.0404411764705515,
					27.79181985294116
				],
				[
					-2.0404411764705515,
					27.79181985294116
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 136,
			"versionNonce": 283924887,
			"isDeleted": false,
			"id": "eUWRg06I",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0.12089686744331463,
			"x": 115.80698529411765,
			"y": -34.53044577205878,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 60.279937744140625,
			"height": 25,
			"seed": 636832951,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695845758059,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "normal",
			"rawText": "normal",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "normal",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 84,
			"versionNonce": 170732057,
			"isDeleted": false,
			"id": "phze2ucs",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0.14796599575544,
			"x": 34.791360294117624,
			"y": -46.32502297794113,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 53.35993957519531,
			"height": 25,
			"seed": 917638391,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695845778263,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "depth",
			"rawText": "depth",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "depth",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#e03131",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 159.84346277573528,
		"scrollY": 283.22529871323525,
		"zoom": {
			"value": 1.7000000000000002
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%