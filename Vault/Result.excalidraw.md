---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Result<T,E> ^h9z5ZU11

Ok(T) ^QOR4TFjv

Err(E) ^0a6Khrx6

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "diamond",
			"version": 457,
			"versionNonce": 650738297,
			"isDeleted": false,
			"id": "ueDhWfnq8GiNgMfcXbB6S",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.87051309121622,
			"y": -320.699535472973,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 260,
			"height": 70,
			"seed": 211743959,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "h9z5ZU11"
				},
				{
					"id": "_Hn-EkBWu7F6mBY5Kn1OV",
					"type": "arrow"
				},
				{
					"id": "qHydKLwjYAkkMiV9393de",
					"type": "arrow"
				}
			],
			"updated": 1695842896900,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 432,
			"versionNonce": 1753596761,
			"isDeleted": false,
			"id": "h9z5ZU11",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -82.76045144570841,
			"y": -298.199535472973,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 119.77987670898438,
			"height": 25,
			"seed": 1956514711,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695842896900,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Result<T,E>",
			"rawText": "Result<T,E>",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ueDhWfnq8GiNgMfcXbB6S",
			"originalText": "Result<T,E>",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 725,
			"versionNonce": 302971383,
			"isDeleted": false,
			"id": "B2y3NNsKTId8BFLlVUkRK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -213.8359375,
			"y": -190.7109375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 120,
			"seed": 251444089,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "QOR4TFjv"
				},
				{
					"id": "_Hn-EkBWu7F6mBY5Kn1OV",
					"type": "arrow"
				}
			],
			"updated": 1695842839988,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 1463,
			"versionNonce": 423362199,
			"isDeleted": false,
			"id": "QOR4TFjv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -180.38021851670067,
			"y": -142.65657514042363,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 53.235748291015625,
			"height": 24.03846153846154,
			"seed": 1154521815,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695842903631,
			"link": null,
			"locked": false,
			"fontSize": 19.230769230769234,
			"fontFamily": 1,
			"text": "Ok(T)",
			"rawText": "Ok(T)",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "B2y3NNsKTId8BFLlVUkRK",
			"originalText": "Ok(T)",
			"lineHeight": 1.25,
			"baseline": 17
		},
		{
			"type": "ellipse",
			"version": 753,
			"versionNonce": 186680535,
			"isDeleted": false,
			"id": "onkgSYd2PhXNrScu_q24F",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 56.51953125,
			"y": -189.56725084459464,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 120,
			"seed": 2065932633,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "0a6Khrx6"
				},
				{
					"id": "qHydKLwjYAkkMiV9393de",
					"type": "arrow"
				}
			],
			"updated": 1695842905254,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 1502,
			"versionNonce": 378566359,
			"isDeleted": false,
			"id": "0a6Khrx6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 88.13985442031105,
			"y": -141.51288848501827,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 56.90653991699219,
			"height": 24.03846153846154,
			"seed": 1085804089,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1695842911055,
			"link": null,
			"locked": false,
			"fontSize": 19.230769230769234,
			"fontFamily": 1,
			"text": "Err(E)",
			"rawText": "Err(E)",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "onkgSYd2PhXNrScu_q24F",
			"originalText": "Err(E)",
			"lineHeight": 1.25,
			"baseline": 17
		},
		{
			"type": "arrow",
			"version": 448,
			"versionNonce": 1887752697,
			"isDeleted": false,
			"id": "_Hn-EkBWu7F6mBY5Kn1OV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -75.41739671162294,
			"y": -252.19856004170092,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 42.55188776161599,
			"height": 61.22854435525184,
			"seed": 1011475993,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695842916898,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "ueDhWfnq8GiNgMfcXbB6S",
				"gap": 12.213314107684958,
				"focus": 0.22511452130977344
			},
			"endBinding": {
				"elementId": "B2y3NNsKTId8BFLlVUkRK",
				"gap": 10.125411251718546,
				"focus": -0.08227405125501078
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-42.55188776161599,
					61.22854435525184
				]
			]
		},
		{
			"type": "arrow",
			"version": 439,
			"versionNonce": 1861314489,
			"isDeleted": false,
			"id": "qHydKLwjYAkkMiV9393de",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 41.87443318457785,
			"y": -261.49447656676733,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 42.08320416468375,
			"height": 66.31748618943192,
			"seed": 1210753815,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695842916905,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "ueDhWfnq8GiNgMfcXbB6S",
				"gap": 6.408203339043958,
				"focus": -0.37988623540993643
			},
			"endBinding": {
				"elementId": "onkgSYd2PhXNrScu_q24F",
				"gap": 13.245579088140417,
				"focus": 0.12766786085106657
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					42.08320416468375,
					66.31748618943192
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 226.49604096283784,
		"scrollY": 451.53542018581084,
		"zoom": {
			"value": 1.85
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%