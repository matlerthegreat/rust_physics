---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.19",
	"elements": [
		{
			"type": "ellipse",
			"version": 265,
			"versionNonce": 922850679,
			"isDeleted": false,
			"id": "fg2SEl76TfxvEES40v779",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -144.68359375,
			"y": -90.0390625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 253.453125,
			"height": 147.5546875,
			"seed": 713591033,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "KU9Ac6nL40n1OhJLadvoL",
					"type": "arrow"
				}
			],
			"updated": 1695845399538,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 183,
			"versionNonce": 859407321,
			"isDeleted": false,
			"id": "edfvuPtN-uaBTPwe6jynC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 10.703125,
			"y": -157.50390625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 266.29296875,
			"height": 300.05078125,
			"seed": 584608633,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "KU9Ac6nL40n1OhJLadvoL",
					"type": "arrow"
				}
			],
			"updated": 1695845403909,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 95,
			"versionNonce": 271776119,
			"isDeleted": false,
			"id": "KU9Ac6nL40n1OhJLadvoL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 9.7578125,
			"y": -25.9296875,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 101.30859375,
			"height": 14.0234375,
			"seed": 284651001,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1695845413722,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "edfvuPtN-uaBTPwe6jynC",
				"focus": 0.0007291561318968423,
				"gap": 1.9445322985928897
			},
			"endBinding": {
				"elementId": "fg2SEl76TfxvEES40v779",
				"focus": 0.1780773642377917,
				"gap": 2.5057139441304344
			},
			"lastCommittedPoint": null,
			"startArrowhead": "arrow",
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					101.30859375,
					14.0234375
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#e03131",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": "arrow",
		"currentItemEndArrowhead": "arrow",
		"scrollX": 160.02022058823528,
		"scrollY": 283.22529871323525,
		"zoom": {
			"value": 1.7000000000000002
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%